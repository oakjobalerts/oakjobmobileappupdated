package com.oakjobalerts;

import retrofit.RestAdapter;
import retrofit.RestAdapter.LogLevel;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentManager.OnBackStackChangedListener;
import android.support.v4.widget.SlidingPaneLayout;
import android.support.v4.widget.SlidingPaneLayout.PanelSlideListener;
import android.text.TextUtils;
import android.util.Log;
import android.view.DragEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.oakjobalerts.Model.MyInter;
import com.oakjobalerts.fragments.AboutAppFragment;
import com.oakjobalerts.fragments.LoginFragment;
import com.oakjobalerts.fragments.MyAlertsFragment;
import com.oakjobalerts.fragments.RecentSearchFragment;
import com.oakjobalerts.fragments.SaveJobsFragment;
import com.oakjobalerts.fragments.SearchFragment;
import com.oakjobalerts.fragments.SettingsFragment;
import com.oakjobalerts.fragments.SignUpFragment;
import com.oakjobalerts.oakinterface.OakFragmentInterface;
import com.oakjobalerts.preferences.Preferences;
import com.oakjobalerts.utilities.Utility;

public class MainActivity extends FragmentActivity implements OakFragmentInterface, OnClickListener, MyInter {
	public static int search_flag = 0;
	View layoutLogin = null;
	View layoutSearch = null;
	View layoutSettings = null;
	View layoutRecentSearch = null;
	View layoutSaveJobs = null;
	View layoutMyAlerts = null;
	View layoutAbout = null;

	SlidingPaneLayout slidingPanel = null;
	ImageButton imMenu = null;
	ImageButton imBack = null;
	TextView txtMenu = null;
	ImageView imLogout = null;
	ImageView imEdit = null;
	ImageView imrefresh = null;
	ImageView imDelete = null;
	Boolean slidingEnabled = true;
	LinearLayout menuLayout = null;
	TextView m_login = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		// getSupportFragmentManager().addOnBackStackChangedListener(MainActivity.this);

		slidingPanel = (SlidingPaneLayout) findViewById(R.id.sliding_panel);
		imMenu = (ImageButton) findViewById(R.id.im_menu);
		imBack = (ImageButton) findViewById(R.id.im_back);
		txtMenu = (TextView) findViewById(R.id.txt_menu);
		imLogout = (ImageView) findViewById(R.id.im_logout);
		imEdit = (ImageView) findViewById(R.id.im_edit);
		imDelete = (ImageView) findViewById(R.id.im_delete);
		menuLayout = (LinearLayout) findViewById(R.id.menu_layout);
		imrefresh=(ImageView) findViewById(R.id.im_refresh);
		m_login = (TextView) findViewById(R.id.m_login);
		layoutLogin = findViewById(R.id.ll_login);
		layoutSearch = findViewById(R.id.ll_search);
		layoutSettings = findViewById(R.id.ll_settings);
		layoutRecentSearch = findViewById(R.id.ll_recent_search);
		layoutSaveJobs = findViewById(R.id.ll_save_jobs);
		layoutMyAlerts = findViewById(R.id.ll_alerts);
		layoutAbout = findViewById(R.id.ll_about);

		imMenu.setOnClickListener(this);

		// m_login.setOnClickListener(this);
		layoutLogin.setOnClickListener(this);
		layoutSearch.setOnClickListener(this);
		layoutSettings.setOnClickListener(this);
		layoutRecentSearch.setOnClickListener(this);

		layoutSaveJobs.setOnClickListener(this);
		layoutMyAlerts.setOnClickListener(this);
		layoutAbout.setOnClickListener(this);

		slidingPanel.setPanelSlideListener(new PanelSlideListener() {

			@Override
			public void onPanelSlide(View arg0, float arg1) {
				loginLayout();

			}

			@Override
			public void onPanelOpened(View arg0) {
				hideKeyboard();
				loginLayout();

			}

			@Override
			public void onPanelClosed(View arg0) {
				// TODO Auto-generated method stub

			}

		});

		Utility.replaceFragment(MainActivity.this, SearchFragment.getInstance(), Utility.TAG_HOME, true);

	}

	@Override
	public void onBackPressed() {

		FragmentManager fa = getSupportFragmentManager();
		int i = fa.getBackStackEntryCount();

		if (i > 0) {
			Log.e("back=", fa.getBackStackEntryAt(i - 1).getName());
			Log.e("frag=", SearchFragment.getInstance().getClass().getName());
			if (fa.getBackStackEntryAt(i - 1).getName().equals(SearchFragment.getInstance().getClass().getName()))
				finish();

		} 
		if (slidingPanel.isOpen()) {

			slidingPanel.closePane();

		} else if (i > 1) {

			fa.popBackStackImmediate(SearchFragment.getInstance().getClass().getName(), 0);

		} else {
			finish();
			// super.onBackPressed();

		}

	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();

		slidingPanel.closePane();
	}

	@Override
	protected void onResume() {
		// Load initial fragment
		super.onResume();
		loginLayout();
		if (search_flag == 1) {
			Utility.replaceFragment(MainActivity.this, SearchFragment.getInstance(), Utility.TAG_HOME, true);
			search_flag = 0;
		} else if (search_flag == 2) {
			Utility.replaceFragment(MainActivity.this, LoginFragment.getInstance(), Utility.TAG_HOME, true);
			search_flag = 0;
		} else if (search_flag == 3) {
			Utility.replaceFragment(MainActivity.this, SignUpFragment.getInstance(), Utility.TAG_SIGNUP, true);
			search_flag = 0;
		}

		hideKeyboard();

	}

	public void hideKeyboard() {
		View view = MainActivity.this.getCurrentFocus();
		if (view != null) {
			InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
		}
	}

	// Overrided Methods of OakFragmentInterface

	@Override
	public void initiateNewActivity(Class className, String tag) {

		startActivity(new Intent(MainActivity.this, className));

	}

	@Override
	public void backBtnSimulate(Fragment fragment, String tag) {
		// TODO Auto-generated method stub
		// Utility.backBtn(MainActivity.this);
	}

	@Override
	public void showBackBtn() {
		// TODO Auto-generated method stub
		imrefresh.setVisibility(View.GONE);
		imMenu.setVisibility(View.GONE);
		imLogout.setVisibility(View.GONE);
		imDelete.setVisibility(View.GONE);
		imBack.setVisibility(View.VISIBLE);
		imEdit.setVisibility(View.VISIBLE);
	}

	@Override
	public void showEditBtn() {
		// TODO Auto-generated method stub
		imrefresh.setVisibility(View.GONE);
		imMenu.setVisibility(View.VISIBLE);
		imEdit.setVisibility(View.VISIBLE);
		imLogout.setVisibility(View.GONE);
		imBack.setVisibility(View.GONE);
		imDelete.setVisibility(View.GONE);
	}

	@Override
	public void showDeleteBtn() {
		// TODO Auto-generated method stub
		imrefresh.setVisibility(View.GONE);
		imMenu.setVisibility(View.VISIBLE);
		imDelete.setVisibility(View.VISIBLE);
		imLogout.setVisibility(View.GONE);
		imBack.setVisibility(View.GONE);
		imEdit.setVisibility(View.GONE);

	}

	@Override
	public void showMenuBtn() {
		// TODO Auto-generated method stub
		imrefresh.setVisibility(View.GONE);
		imMenu.setVisibility(View.VISIBLE);
		imLogout.setVisibility(View.GONE);
		imBack.setVisibility(View.GONE);
		imEdit.setVisibility(View.GONE);
		imDelete.setVisibility(View.GONE);
	}

	@Override
	public void showLogoutBtn() {
		// TODO Auto-generated method stub
		imrefresh.setVisibility(View.GONE);
		imMenu.setVisibility(View.VISIBLE);
		imLogout.setVisibility(View.VISIBLE);
		imBack.setVisibility(View.GONE);
		imEdit.setVisibility(View.GONE);
		imDelete.setVisibility(View.GONE);
	}

	@Override
	public void showNoBtn() {
		// TODO Auto-generated method stub
		imrefresh.setVisibility(View.GONE);
		imMenu.setVisibility(View.VISIBLE);
		imLogout.setVisibility(View.GONE);
		imBack.setVisibility(View.GONE);
		imEdit.setVisibility(View.GONE);
		imDelete.setVisibility(View.GONE);
	}

	@Override
	public void showRefreshBtn() {
		// TODO Auto-generated method stub
		imrefresh.setVisibility(View.VISIBLE);
		imMenu.setVisibility(View.VISIBLE);
		imLogout.setVisibility(View.GONE);
		imBack.setVisibility(View.GONE);
		imEdit.setVisibility(View.GONE);
		imDelete.setVisibility(View.GONE);
	}
	
	@Override
	public void oakFragmentListner(Fragment fragment, String tag, boolean addToStack) {
		// TODO Auto-generated method stub
		Utility.replaceFragment(MainActivity.this, fragment, tag, addToStack);
	}

	@Override
	public void initiateSearchResultActivity(String what, String where, Class className, String tag) {

		Intent i = new Intent(MainActivity.this, className);
		i.putExtra("what", what);
		i.putExtra("where", where);
		startActivity(i);

	}
	
	public static boolean isLogin(Context con) {
		if (Preferences.getPreferenceInstance().islogin(con)) {
			return true;
		}
		return false;
	}

	public void loginLayout() {
		if (Preferences.getPreferenceInstance().islogin(this)) {
			String userName = Preferences.getPreferenceInstance().getUserName(this);
			m_login.setText(userName);
			layoutLogin.setClickable(false);
		} else {
			m_login.setText(Utility.TAG_LOGIN);
			layoutLogin.setClickable(true);
		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.im_menu:
			// loginLayout();
			slidingPanel.openPane();
			hideKeyboard();
			break;

		case R.id.m_login:
			// loginLayout();
			slidingPanel.openPane();
			hideKeyboard();
			break;

		case R.id.ll_login:

			// loginLayout();
			slidingPanel.closePane();
			Utility.replaceFragment(MainActivity.this, LoginFragment.getInstance(), Utility.TAG_LOGIN, false);
			hideKeyboard();
			break;

		case R.id.ll_search:
			slidingPanel.closePane();
			Utility.replaceFragment(MainActivity.this, SearchFragment.getInstance(), Utility.TAG_HOME, true);

			break;

		case R.id.ll_settings:
//			slidingPanel.closePane();
//			Utility.replaceFragment(MainActivity.this, SettingsFragment.getInstance(), Utility.TAG_SETTINGS, false);
		
			if (isLogin(MainActivity.this)) {
				slidingPanel.closePane();
				Utility.replaceFragment(MainActivity.this, SettingsFragment.getInstance(), Utility.TAG_SETTINGS, false);
			} else {
				String msg = "Please login to change settings";
				Utility.loginAlert(msg, this);
				// Toast.makeText(MainActivity.this,
				// "Please login to change settings ",
				// Toast.LENGTH_LONG).show();
				slidingPanel.closePane();
				// Utility.replaceFragment(MainActivity.this,
				// LoginFragment.getInstance(), Utility.TAG_LOGIN, false);

			}

			break;

		case R.id.ll_recent_search:
			slidingPanel.closePane();
			Utility.replaceFragment(MainActivity.this, RecentSearchFragment.getInstance(), Utility.TAG_RECENT_SEARCH,
					false);

			break;

		case R.id.ll_save_jobs:
			slidingPanel.closePane();
			Utility.replaceFragment(MainActivity.this, SaveJobsFragment.getInstance(), Utility.TAG_SAVE_JOBS, false);

			break;

		case R.id.ll_alerts:
			slidingPanel.closePane();
			Utility.replaceFragment(MainActivity.this, MyAlertsFragment.getInstance(), Utility.TAG_MY_ALERTS, false);

			break;

		case R.id.ll_about:

			String url = "http://oakjobalerts.com/about-us.php";
			Utility.replaceFragment(MainActivity.this, AboutAppFragment.getInstance(url), Utility.TAG_ABOUT, false);

			// Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
			// startActivity(i);
			slidingPanel.closePane();

			break;

		default:
			break;
		}

	}

	@Override
	public void loadLoginPage() {

		Utility.replaceFragment(MainActivity.this, LoginFragment.getInstance(), Utility.TAG_LOGIN, false);

	}
	
	

}
