package com.oakjobalerts;

import retrofit.Callback;

import com.oakjobalerts.modeldata.*;

import retrofit.http.Body;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;
import retrofit.http.Query;

public interface AwsCloudSearch {
	
	@GET("/OakJobMobileApi/SearcApi")
	//void javaAwsCloudSearchAPI(@Query("keyword") String keyword,@Query("sort") String sort,@Query("start") String start,@Query("upperLatitude") String upperLatitude,@Query("upperLongitude") String upperLongitude,@Query("lowerLatitude") String lowerLatitude,@Query("lowerLongitude") String lowerLongitude,@Query("offset")  String offset,Callback<Job> callback);
	void  SearcApi(@Query("keyword") String keyword, @Query("zipcode") String zipcode, @Query("start") String start, @Query("offset") String offset,@Query("sort") String sort,Callback<Job> callback);

	@POST("/login")
	void javaAwsCloudSearchAPILogin(@Body  UserRegister login,Callback<SignInResponse> callback);
	
//	@GET("/loginget")
//	void javaAwsCloudSearchAPILogin(@Query("emailId") String email_id, @Query("pass") String pass,Callback<SignInResponse> callback);
//	
	
	@POST("/register")
	void javaAwsCloudSearchAPIRegister(@Body UserRegister user,Callback<SignInResponse> callback);
	
	@POST("/update")
	void javaAwsCloudSearchAPIUpdate(@Body UserRegister user,Callback<SignInResponse> callback);
	
//	@POST("/savejob")
//	void javaAwsCloudSearchAPISaveJob(@Body Job_ job,Callback<SaveJobResponse> callback);
	
	@GET("/savejobget")
	void javaAwsCloudSearchAPISaveJob(@Query("id") String id,@Query("title") String title,@Query("source") String source,@Query("location") String location,@Query("joburl") String joburl,@Query("state") String state,@Query("zipcode") String zipcode,@Query("employer") String employer,@Query("sourcename") String sourcename,@Query("postingdate") String postingdate,@Query("city") String city,@Query("description") String description,@Query("email") String email,Callback<SaveResponse> callback);
	
	
	@GET("/getsavedjobget")
	void javaAwsCloudSearchAPIgetsavedjobs(@Query("emailId") String email,Callback<SaveJobResponse> callback);
	
	@GET("/deletejob")
	void javaAwsCloudSearchAPIDeleteJob(@Query("employer") String employer,@Query("title") String title,@Query("email") String email, Callback<DeleteResponse> callback);
	
	@POST("/savemyalert")
	void javaAwsCloudSearchAPISaveAlert(@Body JobAlerts alert,Callback<SaveAlertResponse> callback);
	
	@GET("/getmyalert")
	void javaAwsCloudSearchAPIgetAlert(@Query("email") String email,@Query("device") String deviceToken, Callback<JobAlertsResponse> callBack);
	

	@GET("/deletemyalertget")
	void javaAwsCloudSearchAPIdeleteAlert(@Query("alert_id") int alert_id, Callback<DeleteResponse> callBack);
	
	@GET("/redirecturl")
	void javaAwsCloudSearchAPIredirecturl(@Query("url") String url, Callback<String> callBack);
	
	@GET("/getuser")
	void javaAwsgetUserDetail(@Query("email") String email,@Query("password") String password, Callback<SignInResponse> callBack);
	
	@GET("/logout")
	void javaAwslogout(@Query("email") String email,@Query("device") String device, Callback<DeleteResponse> callBack);
	
	
//	@POST("/getsavedjob")
//	void javaAwsCloudSearchAPIgetsavedjobs(@Body String email,Callback<SaveJobResponse> callback);
	
	
}
//http://s2.oakjobalerts.com:8080/OakJobMobileApi/SearcApi?
//	keyword=sales&
//	start=0&
//	offset=1&
//	sort=relevance&
//	zipcode=01844