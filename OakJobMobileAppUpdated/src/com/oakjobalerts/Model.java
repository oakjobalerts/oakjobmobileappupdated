package com.oakjobalerts;

public class Model {
	
	MyInter mInter;
	static Model mMode  =null;
	
	private Model(){
		
	}
	
	public static Model getInstance() {
		if(mMode == null){
			mMode = new Model();
		}
		
		return mMode;
	}

	
	
	public interface MyInter{
		public void loadLoginPage();
	}
	
	public void setLister(MyInter myInter){
		this.mInter =  myInter;
	}
	
	public void loadLoginPage(){
		mInter.loadLoginPage();
	}
	

}
