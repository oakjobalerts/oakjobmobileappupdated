package com.oakjobalerts;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Locale;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class BrowseActivity extends Activity {
	
	RelativeLayout topLayout;
	ListView listView;
	CountriesListAdapter countriesListAdapter;
	Context context;
	ImageView imLogout,imEdit,imSearch;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.searchlist_act);
		topLayout= (RelativeLayout)findViewById(R.id.top_ll);
		listView=(ListView)findViewById(R.id.lst_search);
		imLogout=(ImageView)findViewById(R.id.im_logout);
		imEdit=(ImageView)findViewById(R.id.im_edit);
		imSearch=(ImageView)findViewById(R.id.im_search);
		topLayout.setVisibility(View.GONE);
		imEdit.setVisibility(View.GONE);
		imSearch.setVisibility(View.GONE);
		imLogout.setVisibility(View.VISIBLE);
		context=BrowseActivity.this;
		
		Locale[] locale = Locale.getAvailableLocales();
		ArrayList<String> countries = new ArrayList<String>();
		String country;
		for( Locale loc : locale ){
		    country = loc.getDisplayCountry();
		    if( country.length() > 0 && !countries.contains(country) ){
		        countries.add( country );
		    }
		}
		Collections.sort(countries, String.CASE_INSENSITIVE_ORDER);

		countriesListAdapter = new CountriesListAdapter(countries, context);

		listView.setAdapter(countriesListAdapter);
		
		
	}
		
		public class CountriesListAdapter extends BaseAdapter{

			ArrayList<String> countriesList = null;
			Context context;


			public CountriesListAdapter(ArrayList<String> countriesList, Context context) {
				// TODO Auto-generated constructor stub
				this.countriesList = countriesList;
				this.context = context;
			}

			@Override
			public int getCount() {
				// TODO Auto-generated method stub
				return countriesList.size();
			}

			@Override
			public Object getItem(int position) {
				// TODO Auto-generated method stub
				return countriesList.get(position);
			}

			@Override
			public long getItemId(int position) {
				// TODO Auto-generated method stub
				return 0;
			}

			@Override
			public View getView(int position, View convertView, ViewGroup parent) {
				// TODO Auto-generated method stub
				MyViewHolder mViewHolder;

				if (convertView == null) {
					convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.recentsearchlistdetail, parent, false);
					mViewHolder = new MyViewHolder(convertView);
					convertView.setTag(mViewHolder);
				} else {
					mViewHolder = (MyViewHolder) convertView.getTag();
				}


				mViewHolder.tvTitle.setText(countriesList.get(position));
				mViewHolder.tvLocation.setVisibility(View.GONE);


				mViewHolder.nextBtn.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
//						mOakFragmentInterface.oakFragmentListner(new JobDetailFragment(), Utility.TAG_DETAIL,true);
//						startActivity(new Intent(BrowseActivity.this, JobDetailActivity.class));
					
					}
				});
				
				convertView.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
//						mOakFragmentInterface.oakFragmentListner(new JobDetailFragment(), Utility.TAG_DETAIL,true);
//						startActivity(new Intent(BrowseActivity.this, JobDetailActivity.class));
					
					}
				});

				return convertView;
			}

		}


		private class MyViewHolder {
			TextView tvTitle, tvLocation;
			ImageView nextBtn; 

			public MyViewHolder(View item) {
				tvTitle = (TextView) item.findViewById(R.id.tv_title);
				tvLocation = (TextView) item.findViewById(R.id.tv_location);

				nextBtn = (ImageView)item.findViewById(R.id.next_btn);
			}
		}

		
		
		
	

}
