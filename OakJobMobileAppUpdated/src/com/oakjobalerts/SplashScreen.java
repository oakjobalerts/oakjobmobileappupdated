package com.oakjobalerts;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.util.Log;
import android.view.Window;
import android.widget.Toast;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.oakjobalerts.db.ServerSideDb;
import com.oakjobalerts.fragments.LoginFragment;
import com.oakjobalerts.preferences.Preferences;
import com.oakjobalerts.utilities.Utility;

public class SplashScreen  extends Activity{
	private com.google.android.gms.analytics.Tracker ta;
	Handler handler = new Handler();
	Runnable runnable=new Thread();
	GoogleCloudMessaging mGoogleCloudMessaging;
	public static final String PROJECT_NO = "810783849550";
	
@Override
protected void onCreate(Bundle savedInstanceState) {
	// TODO Auto-generated method stub
	super.onCreate(savedInstanceState);
	requestWindowFeature(Window.FEATURE_NO_TITLE);
	setContentView(R.layout.splashscreen);
	
	if (Preferences.getPreferenceInstance().getDevicedata(SplashScreen.this).get(1).equalsIgnoreCase(""))
		new RegisterGCMAsyc().execute();
	else
		getDataFromServer();
	
	try{
		ta = Utility.initialiseTracker(SplashScreen.this, "Splash View",Utility.TRACKING_ID);
	}
	catch(Exception e)
	{
		e.printStackTrace();
	}
	//go to next screen
	runnable=new Runnable() {
		public void run() {
			Intent intent = new Intent(SplashScreen.this, MainActivity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			startActivity(intent);
			overridePendingTransition(0, 0);
			finish();
		}

	};
	handler.postDelayed(runnable, 2000);
}
@Override
public void onBackPressed() {
	// TODO Auto-generated method stub
	super.onBackPressed();
	handler.removeCallbacks(runnable);
}

public void getDataFromServer()
{
	String email=Preferences.getPreferenceInstance().getEmail(this);
	if(!email.equals(""))
	{
		ServerSideDb.getSeverDbInstance().getSaveJobsFromServer(this, email);
	}
	ServerSideDb.getSeverDbInstance().getAlertFromServer(this);
}

class RegisterGCMAsyc extends AsyncTask<Void, Void, String> {

	String DEVICE_TOKEN = "";

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		mGoogleCloudMessaging = GoogleCloudMessaging.getInstance(SplashScreen.this);

	}

	@Override
	protected String doInBackground(Void... params) {
		try {

			DEVICE_TOKEN = mGoogleCloudMessaging.register(PROJECT_NO);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return DEVICE_TOKEN;
	}

	@Override
	protected void onPostExecute(String result) {
		super.onPostExecute(result);

		if (result != null && !DEVICE_TOKEN.equalsIgnoreCase("")) {
			System.out.println(result);
			String DEVICE_ID = Settings.Secure.getString(SplashScreen.this.getApplicationContext()
					.getContentResolver(), Settings.Secure.ANDROID_ID);
			Preferences.getPreferenceInstance().setDevicedata(SplashScreen.this, DEVICE_ID, DEVICE_TOKEN);
			if(Utility.isNetworkAvailable(SplashScreen.this))
				getDataFromServer();
			Log.e("deviceId=", DEVICE_ID);
			Log.e("deviceToken=", "token=" + DEVICE_TOKEN);

		}
	}
}


}


