package com.oakjobalerts;

import com.oakjobalerts.oakinterface.OakFragmentInterface;
import com.oakjobalerts.utilities.Utility;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

public class ApplyJob extends Activity {
	
	private com.google.android.gms.analytics.Tracker ta;
	WebView applyJobPage=null;
	ProgressBar bar;
	ImageView imEdit = null;
	ImageView imSearch=null;
	ImageView imBack = null;
	private TextView txt_menu;
	OakFragmentInterface mOakFragmentInterface;
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		try{ ta = Utility.initialiseTracker(this, "Apply Job View",Utility.TRACKING_ID); }
		catch(Exception e)	{	e.printStackTrace(); }
		
		setContentView(R.layout.apply_job_page);
		
		txt_menu=(TextView)findViewById(R.id.txt_menu);
		txt_menu.setText(Utility.TAG_HOME);
		imEdit = (ImageView)findViewById(R.id.im_edit);
		imSearch= (ImageView)findViewById(R.id.im_search);
		imBack		= (ImageView)findViewById(R.id.im_back);
		
		applyJobPage=(WebView)findViewById(R.id.applyJobPage);
		imEdit.setVisibility(View.GONE);
		imSearch.setVisibility(View.GONE);
		
		imBack.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				ApplyJob.this.finish();
				
			}
		});
		Intent intent=getIntent();
		String url=intent.getStringExtra("url");
		boolean flag=false;
		try{ flag=intent.getBooleanExtra("notifiaction",false);	}
		catch(Exception e){	e.printStackTrace(); }
		try{
			if(flag)ta = Utility.initialiseTracker(this, "BroadCast View",Utility.TRACKING_ID);
			        ta = Utility.initialiseTracker(this, "ApplyJob View",Utility.TRACKING_ID);	
			}
		catch(Exception e)	{	e.printStackTrace(); }
		
		bar = (ProgressBar)findViewById(R.id.progress_Bar);
		openWebView(url);
		
		
//		applyJobPage.setWebViewClient(new WebViewClient() {
//		    public boolean shouldOverrideUrlLoading(WebView view, String url){
//		      
//		        view.loadUrl(url);
//		        return false; // then it is not handled by default action
//		   }
//		});
//		//applyJobPage.loadUrl(url);
//	}
	

	}
	private void openWebView(String url) {
		
		
//		applyJobPage.setWebViewClient(new Callback());
//		//add this below line
//		applyJobPage.setWebChromeClient(new WebChromeClient() {
//		   public void onProgressChanged(WebView view, int progress) {
//			   bar.setProgress(progress);
//		   }
//		 });
//		
//		
		
		applyJobPage.setWebViewClient(new Callback());
		applyJobPage.getSettings().setJavaScriptEnabled(true);
		applyJobPage.getSettings().setUseWideViewPort(true);
		applyJobPage.getSettings().setBuiltInZoomControls(true);	
		applyJobPage.setInitialScale(1);
		applyJobPage.loadUrl(url);
		
		
		
		

	}

	private class Callback extends WebViewClient {

		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {
			if(url.equalsIgnoreCase("http://oakjobalerts.com/about-us.php") || url.equalsIgnoreCase("http://oakjobalerts.com/termsandservice.php") || url.equalsIgnoreCase("http://oakjobalerts.com/privacy.php") || url.equalsIgnoreCase("https://jobalerts.freshdesk.com/support/tickets/new"))
			{
				
			}
			else if(url.equalsIgnoreCase("http://oakjobalerts.com/sign_up.php")){
				finish();
				MainActivity.search_flag=3;
				
			}
			else
			view.loadUrl(url);
			return true;
		}

		@Override
		public void onPageStarted(WebView view, String url, Bitmap favicon) {
			// TODO Auto-generated method st
			super.onPageStarted(view, url, favicon);
			bar.setProgress(0);
			bar.setVisibility(View.VISIBLE);
		}

		@Override
		public void onPageFinished(WebView view, String url) {
			// TODO Auto-generated method stub
			super.onPageFinished(view, url);
			bar.setProgress(100);
			bar.setVisibility(View.GONE);
		}

	}

}
