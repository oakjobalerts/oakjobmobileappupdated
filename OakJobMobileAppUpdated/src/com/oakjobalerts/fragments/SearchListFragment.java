package com.oakjobalerts.fragments;
/*package com.aws.oakjob.fragments;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.aws.oakjob.Job;
import com.aws.oakjob.JobDetailActivity;
import com.aws.oakjob.OakFragmentInterface;
import com.aws.oakjob.R;
import com.aws.oakjob.Utility;

public class SearchListFragment extends Fragment{


	ListView lstJobs;
	JobsListAdapter jobsListAdapter = null;
	Context context;
	OakFragmentInterface mOakFragmentInterface;
	ImageView imMenu = null;
	TextView txtMenu = null;
	ImageView imLogout = null;
	
	
	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		
		mOakFragmentInterface = (OakFragmentInterface) activity;
		
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub

		View view = inflater.inflate(R.layout.searchlist ,container, false);
		context = getActivity();
		try {
			imMenu		= (ImageView)getActivity().findViewById(R.id.im_menu);
			txtMenu     = (TextView)getActivity().findViewById(R.id.txt_menu);
			imLogout = (ImageView)getActivity().findViewById(R.id.im_logout);
			
//			imMenu.setImageResource(R.drawable.back_btn);
//			imLogout.setImageResource(R.drawable.search_top);
//			imLogout.setVisibility(View.VISIBLE);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		ArrayList<Job> arrJobs  = new ArrayList<>();

		Job job = null;
		for(int i= 0; i <40 ; i++){
			job = new Job();
			job.title = "UI Developer";
			job.company = "Rabbit Digital Branding Solutions";
			job.city = "San Jose";
			job.state = "CA";
			job.sourceName = "Juju";
			
			arrJobs.add(job);
		}

		lstJobs = (ListView)view.findViewById(R.id.lst_search);
		jobsListAdapter = new JobsListAdapter(arrJobs, context);

		lstJobs.setAdapter(jobsListAdapter);
		
		mOakFragmentInterface.showBackBtn();
		
//		mOakFragmentInterface.disableSlidingPane(false);

		return view;
		
		
	}



	public class JobsListAdapter extends BaseAdapter{

		ArrayList<Job> arrJobs = null;
		Context context;
		MyViewHolder mViewHolder;

		public JobsListAdapter(ArrayList<Job> arrJobs, Context context) {
			// TODO Auto-generated constructor stub
			this.arrJobs = arrJobs;
			this.context = context;
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return arrJobs.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return arrJobs.get(position);
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			

			if (convertView == null) {
				convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.searchlistdetail, parent, false);
				mViewHolder = new MyViewHolder(convertView);
				convertView.setTag(mViewHolder);
			} else {
				mViewHolder = (MyViewHolder) convertView.getTag();
			}

			Job currentJobData = (Job) getItem(position);

			mViewHolder.tvTitle.setText(currentJobData.title);
			mViewHolder.tvCompany.setText(currentJobData.company + "-" + currentJobData.city + ", " + currentJobData.state);
			
			mViewHolder.tvSource.setText(currentJobData.sourceName);
			mViewHolder.tvDate.setText("28 days ago");
			
			mViewHolder.btnApply.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					
//					Toast.makeText(getActivity(), "Hi", 0).show();
					
//					mOakFragmentInterface.oakFragmentListner(new JobDetailFragment(), Utility.TAG_DETAIL,true);
					
					mOakFragmentInterface.initiateNewActivity(JobDetailActivity.class, Utility.TAG_DETAIL);
				}
			});
			
			convertView.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
//					mOakFragmentInterface.oakFragmentListner(new JobDetailFragment(), Utility.TAG_DETAIL,true);
					mOakFragmentInterface.initiateNewActivity(JobDetailActivity.class, Utility.TAG_DETAIL);
				
				}
			});
			

			return convertView;
		}

	}


	private class MyViewHolder {
		TextView tvTitle, tvCompany, tvSource, tvDate;
		ImageView imFav, imShare;
		Button btnApply; 

		public MyViewHolder(View item) {
			tvTitle = (TextView) item.findViewById(R.id.tv_title);
			tvCompany = (TextView) item.findViewById(R.id.tv_company);
			tvSource = (TextView) item.findViewById(R.id.tv_source);
			tvDate = (TextView) item.findViewById(R.id.tv_date);
			imFav = (ImageView) item.findViewById(R.id.im_fav);
			imShare= (ImageView) item.findViewById(R.id.im_share);

			btnApply = (Button)item.findViewById(R.id.btn_apply);
		}
	}
	


}
*/