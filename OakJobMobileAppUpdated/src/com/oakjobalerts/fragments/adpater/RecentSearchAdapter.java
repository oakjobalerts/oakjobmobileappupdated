package com.oakjobalerts.fragments.adpater;

import java.util.List;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.oakjobalerts.R;
import com.oakjobalerts.SearchListActivity;
import com.oakjobalerts.db.OakjobDb;
import com.oakjobalerts.db.ServerSideDb;
import com.oakjobalerts.fragments.MyAlertsFragment;
import com.oakjobalerts.modeldata.JobLocationData;
import com.oakjobalerts.oakinterface.OakFragmentInterface;
import com.oakjobalerts.utilities.Utility;

public class RecentSearchAdapter extends BaseAdapter{

	Context context=null;
	OakFragmentInterface mOakFragmentInterface;
	List<JobLocationData> recentDataList;
	public RecentSearchAdapter(OakFragmentInterface mOakFragmentInterface,List<JobLocationData> recentDataList,Context context) {
		this.mOakFragmentInterface=mOakFragmentInterface;
		this.recentDataList=recentDataList;
		this.context=context;
				
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return recentDataList.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return recentDataList.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		MyViewHolder mViewHolder;

		if (convertView == null) {
			convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.recentsearchlistdetail, parent, false);
			mViewHolder = new MyViewHolder(convertView);
			convertView.setTag(mViewHolder);
		} else {
			mViewHolder = (MyViewHolder) convertView.getTag();
		}

	    mViewHolder.tvTitle.setText(recentDataList.get(position).getKeyword());
		mViewHolder.tvLocation.setText(recentDataList.get(position).getStateCity());
		
		if(MyAlertsFragment.edit_mode)
		{
			
			mViewHolder.rightArrow.setVisibility(View.GONE);
			mViewHolder.im_recent_delete.setVisibility(View.VISIBLE);
			convertView.setClickable(false);
		}
		else
		{
			mViewHolder.rightArrow.setVisibility(View.VISIBLE);
			mViewHolder.im_recent_delete.setVisibility(View.GONE);
			convertView.setClickable(true);
		}
		
		mViewHolder.im_recent_delete.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				confirmAlert(position);		
//				if(Utility.isNetworkAvailable(context))
//				{
//					confirmAlert(position);		
//				}
//				else
//				{
//					Utility.checkInternerConnection(context);
//				}
//				
			}
		});
		
		convertView.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// open job detail
				if(!MyAlertsFragment.edit_mode)
				{
				mOakFragmentInterface.initiateSearchResultActivity(recentDataList.get(position).getKeyword(), recentDataList.get(position).getStateCity(), SearchListActivity.class, Utility.TAG_SEARCH_LIST);
				}
			}
		});
		

		return convertView;
	}



	private class MyViewHolder {
		TextView tvTitle, tvLocation;
		ImageView rightArrow,im_recent_delete;


		public MyViewHolder(View item) {
			tvTitle = (TextView) item.findViewById(R.id.tv_title);
			tvLocation = (TextView) item.findViewById(R.id.tv_location);
			rightArrow= (ImageView) item.findViewById(R.id.next_btn);
			im_recent_delete=(ImageView) item.findViewById(R.id.im_recent_delete);
		}
	}
	
	
	public void confirmAlert(final int position)
	{
		    AlertDialog alertDialog = new AlertDialog.Builder(context,R.style.DialogTheme).create();
	        alertDialog.setTitle("Delete Alert");
	        alertDialog.setMessage("Do you want to delete this Alert?");
	        
	        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Yes", new DialogInterface.OnClickListener() {
			
				@Override
				public void onClick(DialogInterface dialog, int which) {
					ServerSideDb.getSeverDbInstance().deleteAlertFromServer( position,recentDataList,RecentSearchAdapter.this,recentDataList.get(position).getKeyword(),recentDataList.get(position).getStateCity(), context);
					//OakjobDb.getDBInstance(context).deleteMyAlert(recentDataList.get(position).getKeyword(),recentDataList.get(position).getStateCity());
					
//					if(recentDataList.size()>0)
//					{
//					RecentSearchAdapter.this.notifyDataSetChanged();
//					}
//					else{
//						MyAlertsFragment.getInstance().emptyList();
//					}
					//Toast.makeText(context, "Alert Deleted", Toast.LENGTH_LONG).show();
				}
			});
	        
	        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "No", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					
					
				}
			});
	        alertDialog.show();
	       
	}
}