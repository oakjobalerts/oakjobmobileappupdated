package com.oakjobalerts.fragments;

import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Selection;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.oakjobalerts.AwsCloudSearch;
import com.oakjobalerts.MainActivity;
import com.oakjobalerts.R;
import com.oakjobalerts.db.ServerSideDb;
import com.oakjobalerts.modeldata.SignInResponse;
import com.oakjobalerts.modeldata.UserRegister;
import com.oakjobalerts.oakinterface.OakFragmentInterface;
import com.oakjobalerts.preferences.Preferences;
import com.oakjobalerts.utilities.Utility;

public class SettingsFragment extends Fragment implements OnClickListener, OnFocusChangeListener {

	String typeList[];
	String frequencyList[];

	private com.google.android.gms.analytics.Tracker ta;
	TextView txtSignUp = null;
	RelativeLayout sett_edit_layout = null;
	// Toast.makeText(getActivity(),"Updation Saved Succe
	RelativeLayout main_layout = null;
	TextView txtMenu = null;
	ImageView im_name = null;
	ImageView im_last_name = null;
	// ImageView im_email=null;
	ImageView im_password = null;
	ImageView im_zipcode = null;
	ImageView im_phone_no = null;
	ImageView im_refresh = null;
	
	TextView tv_name = null;
	TextView tv_last_name = null;
	TextView tv_email = null;
	TextView tv_password = null;
	TextView tv_phone_no = null;
	TextView tv_zipcode = null;

	EditText sett_edit_text = null;
	Button btn_save = null;
	Button btn_logout = null;
	// UserRegister user = null;

	OakFragmentInterface mOakFragmentInterface;

	RelativeLayout email = null;
	RelativeLayout phone_no;
	RelativeLayout zipcode;
	// Toast.makeText(getActivity(),"Updation Saved Succe
	RelativeLayout name_layout;
	RelativeLayout last_name_layout;
	RelativeLayout password_layout;
	LinearLayout layout_userdetail;
	private EditText edit_name_new;
	private EditText edit_last_name;
	// private EditText edit_email;
	private EditText edit_phone_no;
	private EditText edit_zipcode;
	private Spinner alert_type;
	private Spinner alert_frequency;
	View view = null;
	public ProgressBar progress_sett;
	public ScrollView scroll;
	private static SettingsFragment fragment = null;

	private SettingsFragment() {

	}

	public static SettingsFragment getInstance() {
		if (fragment == null)
			fragment = new SettingsFragment();

		return fragment;
	}

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);

		mOakFragmentInterface = (OakFragmentInterface) activity;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		
		try {
			ta = Utility.initialiseTracker(getActivity(), "Update Profile View", Utility.TRACKING_ID);
		} catch (Exception e) {
			e.printStackTrace();
		}

		view = inflater.inflate(R.layout.settings, null);
		st_init(view);
		getUserData();
		if (!MainActivity.isLogin(getActivity()))
			layout_userdetail.setVisibility(View.GONE);

		

		im_name.setOnClickListener(this);
		im_last_name.setOnClickListener(this);
		// im_email.setOnClickListener(this);
		im_password.setOnClickListener(this);
		im_phone_no.setOnClickListener(this);
		im_zipcode.setOnClickListener(this);

		btn_save.setOnClickListener(this);

		edit_name_new.setOnFocusChangeListener(this);
		edit_last_name.setOnFocusChangeListener(this);
		// edit_email.setOnFocusChangeListener(this);
		edit_phone_no.setOnFocusChangeListener(this);
		edit_zipcode.setOnFocusChangeListener(this);

		btn_logout.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				mOakFragmentInterface.hideKeyboard();
				if (MainActivity.isLogin(getActivity())){
					
					logout();}
				else
					login();
				// Preferences.getPreferenceInstance().logoutSession(getActivity());
				// Utility.replaceFragment(getActivity(),
				// SearchFragment.getInstance(),Utility.TAG_HOME,false);
				// txtMenu.setText(Utility.TAG_HOME);
			}
		});
		
		im_refresh.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				progress_sett.setVisibility(View.VISIBLE);
				scroll.setVisibility(View.GONE);
				getUserData();
				
			}
		});

		btn_save.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				mOakFragmentInterface.hideKeyboard();
				try {
					Utility.sendClickEvent(ta, "Update Profile View", "Click", "Update Profile");
				} catch (Exception e) {
					e.printStackTrace();
				}

				// Log.e("email=", edit_email.getText().toString());
				// Toast.makeText(getActivity(),"Updation Saved Successfully",
				// Toast.LENGTH_LONG).show();

				if (MainActivity.isLogin(getActivity())) {

					if (tv_password.getText().equals("")) {
						Toast.makeText(getActivity(), "Password cannot be empty", 0).show();
					} else if (!Utility.isValidMobile(edit_phone_no.getText().toString())) {
						Log.e("email=", tv_phone_no.getText().toString());
						Toast.makeText(getActivity(), "Invalid Phone No", 0).show();

					} else {
						progress_sett.setVisibility(View.VISIBLE);
						scroll.setVisibility(View.GONE);
						
						Log.e("tv_name.getText() 123", tv_name.getText().toString());
						Log.e("edit_name.getText() 123", edit_name_new.getText().toString());

						tv_name.setText(edit_name_new.getText());

						Log.e("tv_name.getText() 123", tv_name.getText().toString());

						tv_last_name.setText(edit_last_name.getText());
						// tv_email.setText(edit_email.getText());
						tv_zipcode.setText(edit_zipcode.getText());
						tv_phone_no.setText(edit_phone_no.getText());

						tv_name.setTextColor(Color.parseColor("#000000"));
						tv_last_name.setTextColor(Color.parseColor("#000000"));
						// tv_email.setTextColor(Color.parseColor("#000000"));
						tv_password.setTextColor(Color.parseColor("#000000"));
						tv_zipcode.setTextColor(Color.parseColor("#000000"));
						tv_phone_no.setTextColor(Color.parseColor("#000000"));

						UserRegister userData = new UserRegister();
						userData.setFirstName(tv_name.getText().toString());
						userData.setLastName(tv_last_name.getText().toString());
						userData.setPhone(tv_phone_no.getText().toString());
						userData.setPass(tv_password.getText().toString());
						userData.setUserEmail(tv_email.getText().toString());
						userData.setZipcode(tv_zipcode.getText().toString());

						String alert_val = "0", fre_val = "1";
						String alert = alert_type.getSelectedItem().toString();
						for (int i = 0; i < typeList.length; i++) {
							if (alert.equalsIgnoreCase(typeList[i])) {
								alert_val = String.valueOf(i);

							}
						}
						String fre_alert = alert_frequency.getSelectedItem().toString();
						for (int i = 0; i < frequencyList.length; i++) {
							if (fre_alert.equalsIgnoreCase(frequencyList[i])) {
								fre_val = String.valueOf(i + 1);

							}
						}
						userData.setAlertActive(alert_val);
						userData.setFrequency(fre_val);
						//Preferences.getPreferenceInstance().setAlerts(getActivity(), alert_val, fre_val);
						//
						// if(alert.equalsIgnoreCase("Notification"))
						// alert_val="2";
						// else
						// if(alert.equalsIgnoreCase("Email and Notification"))
						// alert_val="1";
						// Log.e("alert","alert="+alert_val);
						// user.setAlertActive(alert_val);
						//
						// String
						// String fre_alert=
						// alert_frequency.getSelectedItem().toString();
						// if(fre_alert.equalsIgnoreCase("Daily")) fre_val="1";
						// else if(fre_alert.equalsIgnoreCase("Twice a week"))
						// fre_val="2";
						// else if(fre_alert.equalsIgnoreCase("Weekly"))
						// fre_val="3";
						// else if(fre_alert.equalsIgnoreCase("Monthly"))
						// fre_val="4";
						//
						// Log.e("alert","alert="+fre_val);
						// user.setFrequency(fre_val);

						Log.e("object", userData.getFirstName());
						updateData(userData);

						edit_name_new.setVisibility(View.GONE);
						edit_last_name.setVisibility(View.GONE);
						// edit_email.setVisibility(View.GONE);
						edit_zipcode.setVisibility(View.GONE);
						edit_phone_no.setVisibility(View.GONE);

						tv_name.setVisibility(View.VISIBLE);
						tv_last_name.setVisibility(View.VISIBLE);
						tv_email.setVisibility(View.VISIBLE);
						tv_zipcode.setVisibility(View.VISIBLE);
						tv_phone_no.setVisibility(View.VISIBLE);
					}
				} else {
//					UserRegister userwithoutlogin = new UserRegister();
//					String fre_val = "1";
//					String fre_alert = alert_frequency.getSelectedItem().toString();
//					if (fre_alert.equalsIgnoreCase("Daily"))
//						fre_val = "1";
//					else if (fre_alert.equalsIgnoreCase("Twice a week"))
//						fre_val = "2";
//					else if (fre_alert.equalsIgnoreCase("Weekly"))
//						fre_val = "3";
//					else if (fre_alert.equalsIgnoreCase("Monthly"))
//						fre_val = "4";
//
//					userwithoutlogin.setFrequency(fre_val);
//					Preferences.getPreferenceInstance().setAlerts(getActivity(), "", fre_val);
//					List<String> device = Preferences.getPreferenceInstance().getDevicedata(getActivity());
//					userwithoutlogin.setDeviceId(device.get(0));
//					userwithoutlogin.setDeviceToken(device.get(1));
//					updateData(userwithoutlogin);
				}

				// Utility.replaceFragment(getActivity(),
				// SearchFragment.getInstance(), Utility.TAG_HOME, true);
			}

		});

		mOakFragmentInterface.showRefreshBtn();
		return view;
	}

	@Override
	public void onResume() {
		super.onResume();
		setUserdata();
		setDataInSpinner();
	}

	void getUserData()
	{
		if(Utility.isNetworkAvailable(getActivity()))
		ServerSideDb.getSeverDbInstance().getUserDetails(getActivity());
		else
		{
		progress_sett.setVisibility(View.GONE);
		scroll.setVisibility(View.VISIBLE);
		}
	}
	
	void st_init(View view) {
		typeList = new String[] { "No Alert", "Email and Notification", "Notification" };
		frequencyList = new String[] { "Daily", "Twice a week", "Weekly", "Monthly" };

		im_refresh=(ImageView) getActivity().findViewById(R.id.im_refresh);
		progress_sett=(ProgressBar)view.findViewById(R.id.progress_sett);
		scroll=(ScrollView)view.findViewById(R.id.scroll);
		// ImageView Intialization
		im_name = (ImageView) view.findViewById(R.id.im_name);
		im_last_name = (ImageView) view.findViewById(R.id.im_last_name);
		// im_email=(ImageView)view.findViewById(R.id.im_email);
		im_password = (ImageView) view.findViewById(R.id.im_password);
		im_phone_no = (ImageView) view.findViewById(R.id.im_phone_no);
		im_zipcode = (ImageView) view.findViewById(R.id.im_zipcode);

		// TextView Intialization
		tv_name = (TextView) view.findViewById(R.id.tv_name);
		tv_last_name = (TextView) view.findViewById(R.id.tv_last_name);
		tv_email = (TextView) view.findViewById(R.id.tv_email);
		tv_password = (TextView) view.findViewById(R.id.tv_password);
		tv_zipcode = (TextView) view.findViewById(R.id.tv_zipcode);
		tv_phone_no = (TextView) view.findViewById(R.id.tv_phone_no);
		txtMenu = (TextView) getActivity().findViewById(R.id.txt_menu);

		// //Set Data in TextView
		// tv_name=(TextView)view.findViewById(R.id.tv_name);
		// tv_last_name=(TextView)view.findViewById(R.id.tv_last_name);
		// tv_email=(TextView)view.findViewById(R.id.tv_email);
		// tv_password=(TextView)view.findViewById(R.id.tv_password);
		// tv_zipcode=(TextView)view.findViewById(R.id.tv_zipcode);
		// tv_phone_no=(TextView)view.findViewById(R.id.tv_phone_no);

		// Spinner Intialization
		alert_type = (Spinner) view.findViewById(R.id.alert_type);
		alert_frequency = (Spinner) view.findViewById(R.id.alert_frequency);

		// Button Intialization
		btn_logout = (Button) view.findViewById(R.id.btn_logout);
		btn_save = (Button) view.findViewById(R.id.btn_save);

		// Layout Intialization
		name_layout = (RelativeLayout) view.findViewById(R.id.name_layout);
		last_name_layout = (RelativeLayout) view.findViewById(R.id.last_name_layout);
		email = (RelativeLayout) view.findViewById(R.id.email);
		password_layout = (RelativeLayout) view.findViewById(R.id.password_layout);
		phone_no = (RelativeLayout) view.findViewById(R.id.phone_no);
		zipcode = (RelativeLayout) view.findViewById(R.id.zipcode);
		layout_userdetail = (LinearLayout) view.findViewById(R.id.layout_userdetail);

		// EditText
		edit_name_new = (EditText) view.findViewById(R.id.edit_name);
		edit_last_name = (EditText) view.findViewById(R.id.edit_last_name);
		// edit_email = (EditText) view.findViewById(R.id.edit_email);
		edit_phone_no = (EditText) view.findViewById(R.id.edit_phone_no);
		edit_zipcode = (EditText) view.findViewById(R.id.edit_zipcode);

		main_layout = (RelativeLayout) view.findViewById(R.id.main_layout);

		txtMenu.setText(Utility.TAG_SETTINGS);

//		if (!MainActivity.isLogin(getActivity()))
//			btn_logout.setText("Login");

	}

	void setDataInSpinner() {
		List<String> alertList = Preferences.getPreferenceInstance().getAlerts(getActivity());

		ArrayAdapter<String> typeadapter = new ArrayAdapter<String>(getActivity(), R.layout.spinner_layout, typeList);
		typeadapter.setDropDownViewResource(R.layout.spinner_list_layout);
		alert_type.setAdapter(typeadapter);

		int alert_num = 1, frequency = 1;

		// if(OakjobDb.getDBInstance(getActivity()).getJobAlerts().size()==0);
		// alert_frequency.setEnabled(false);
		//
		// if(alertList.get(0).equals("1"))
		// alert_num=0;
		// else if(alertList.get(0).equals("0"))
		// alert_num=2;
		// else if(alertList.get(0).equals("2"))
		// alert_num=1;
		int alert = 0;
		if (!alertList.get(0).equals(""))
			alert = Integer.parseInt(alertList.get(0));
		alert_type.setSelection(alert);

		ArrayAdapter<String> frequencyAdapter = new ArrayAdapter<String>(getActivity(), R.layout.spinner_layout,
				frequencyList);
		frequencyAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		alert_frequency.setAdapter(frequencyAdapter);

		if (!MainActivity.isLogin(getActivity())) {
			alert_type.setSelection(1);
			alert_type.setEnabled(false);
		}
		// if((OakjobDb.getDBInstance(getActivity()).getJobAlerts()).size()==0)
		// {
		// alert_frequency.setSelection(0);
		// alert_frequency.setEnabled(false);
		// }
		// else
		// {
		if (!alertList.get(1).equals(""))
			frequency = Integer.parseInt(alertList.get(1));
		alert_frequency.setSelection(frequency - 1);

	}

	@Override
	public void onClick(View view) {

		btn_save.requestFocus();
		showKeyboard(true, view);

		switch (view.getId()) {
		case R.id.im_name:
			edit_name_new.setText(tv_name.getText());
			edit_name_new.setVisibility(View.VISIBLE);
			edit_name_new.requestFocus();
			edit_name_new.setCursorVisible(true);
			Selection.setSelection(edit_name_new.getText(), edit_name_new.length());
			tv_name.setVisibility(View.GONE);
			break;

		case R.id.im_last_name:
			edit_last_name.setText(tv_last_name.getText());
			edit_last_name.setVisibility(View.VISIBLE);
			edit_last_name.requestFocus();
			edit_last_name.setCursorVisible(true);
			Selection.setSelection(edit_last_name.getText(), edit_last_name.length());
			tv_last_name.setVisibility(View.GONE);
			break;

		// case R.id.im_email:
		// edit_email.setText(tv_email.getText());
		// edit_email.setVisibility(View.VISIBLE);
		// edit_email.requestFocus();
		// edit_email.setCursorVisible(true);
		// Selection.setSelection(edit_email.getText(), edit_email.length());
		// tv_email.setVisibility(View.GONE);
		// break;

		case R.id.im_password:
			hideKeyboard();
			showPasswordDialog();
			break;

		case R.id.im_phone_no:
			edit_phone_no.setText(tv_phone_no.getText());
			edit_phone_no.setVisibility(View.VISIBLE);
			edit_phone_no.requestFocus();
			edit_phone_no.setCursorVisible(true);
			Selection.setSelection(edit_phone_no.getText(), edit_phone_no.length());
			tv_phone_no.setVisibility(View.GONE);
			// hideKeyboard();
			// im_phone_no.requestFocus();
			// showKeyboard(false,view);
			break;

		case R.id.im_zipcode:
			edit_zipcode.setText(tv_zipcode.getText());
			edit_zipcode.setVisibility(View.VISIBLE);
			edit_zipcode.requestFocus();
			edit_zipcode.setCursorVisible(true);
			Selection.setSelection(edit_zipcode.getText(), edit_zipcode.length());
			tv_zipcode.setVisibility(View.GONE);
			break;

		}

	}

	@Override
	public void onFocusChange(View view, boolean hasFocus) {
		showText();
		switch (view.getId()) {
		case R.id.edit_name:
			if (!hasFocus) {
				Log.e("edit_name.getText() on chnage", edit_name_new.getText().toString());
				edit_name_new.setVisibility(View.GONE);
				tv_name.setVisibility(View.VISIBLE);
				if (!edit_name_new.getText().toString().equals(tv_name.getText().toString())) {
					tv_name.setText(edit_name_new.getText());
					tv_name.setTextColor(Color.parseColor("#FF0000"));
				}
			}
			break;

		case R.id.edit_last_name:
			if (!hasFocus) {
				edit_last_name.setVisibility(View.GONE);
				tv_last_name.setVisibility(View.VISIBLE);
				if (!(edit_last_name.getText().toString()).equals(tv_last_name.getText().toString())) {
					Log.e("e", edit_last_name.getText().toString());
					Log.e("e", tv_last_name.getText().toString());
					tv_last_name.setText(edit_last_name.getText());
					tv_last_name.setTextColor(Color.parseColor("#FF0000"));
				}
			}
			break;

		// case R.id.edit_email:
		// if (!hasFocus) {
		// edit_email.setVisibility(View.GONE);
		// tv_email.setVisibility(View.VISIBLE);
		// if
		// (!edit_email.getText().toString().equals(tv_email.getText().toString()))
		// {
		// tv_email.setText(edit_email.getText());
		// tv_email.setTextColor(Color.parseColor("#FF0000"));
		// }
		// }
		// break;

		case R.id.im_password:
			showPasswordDialog();
			break;

		case R.id.edit_phone_no:
			if (!hasFocus) {
				edit_phone_no.setVisibility(View.GONE);
				tv_phone_no.setVisibility(View.VISIBLE);
				if (!edit_phone_no.getText().toString().equals(tv_phone_no.getText().toString())) {
					tv_phone_no.setText(edit_phone_no.getText());
					tv_phone_no.setTextColor(Color.parseColor("#FF0000"));
				}
			}
			break;

		case R.id.edit_zipcode:
			if (!hasFocus) {
				edit_zipcode.setVisibility(View.GONE);
				tv_zipcode.setVisibility(View.VISIBLE);
				if (!edit_zipcode.getText().toString().equals(tv_zipcode.getText().toString())) {
					tv_zipcode.setText(edit_zipcode.getText());
					tv_zipcode.setTextColor(Color.parseColor("#FF0000"));
				}
			}
			break;

		}

	}

	public void showText() {
		tv_name.setVisibility(View.VISIBLE);
		tv_last_name.setVisibility(View.VISIBLE);
		tv_email.setVisibility(View.VISIBLE);
		tv_password.setVisibility(View.VISIBLE);
		tv_zipcode.setVisibility(View.VISIBLE);
		tv_phone_no.setVisibility(View.VISIBLE);
	}

	public void showKeyboard(boolean flag, View v) {
		InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(
				Context.INPUT_METHOD_SERVICE);
		if (inputMethodManager != null) {
			if (flag == true) {
				inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
			} else {
				inputMethodManager.showSoftInputFromInputMethod(v.getApplicationWindowToken(), 1);
			}
		}
	}

	public void hideKeyboard() {
		View view = getActivity().getCurrentFocus();
		if (view != null) {
			InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
		}
	}

	public void showPasswordDialog() {
		AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity(), R.style.DialogTheme);
		LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		final View viewMessEdit = inflater.inflate(R.layout.dialog, null);
		dialog.setView(viewMessEdit);

		dialog.setPositiveButton("SAVE", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				EditText dialog_pass = (EditText) viewMessEdit.findViewById(R.id.dialog_pass);
				EditText dialog_con_pass = (EditText) viewMessEdit.findViewById(R.id.dialog_con_pass);
				if (dialog_pass.getText().toString().equals("")) {
					Toast.makeText(getActivity(), "Password cannot be empty", Toast.LENGTH_LONG).show();
				} else if (dialog_pass.getText().toString().equals(dialog_con_pass.getText().toString())) {
					tv_password.setText(dialog_pass.getText().toString());
					tv_password.setTextColor(Color.parseColor("#FF0000"));
				} else {
					Toast.makeText(getActivity(), "Password and change password does not match", Toast.LENGTH_LONG)
							.show();
				}
				dialog.dismiss();

			}
		});
		dialog.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();

			}
		});
		// dialog.show();
		AlertDialog alert = dialog.create();
		alert.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
		alert.show();
		Button bq = alert.getButton(DialogInterface.BUTTON_NEGATIVE);
		bq.setBackgroundColor(getResources().getColor(R.color.c_grey));
		bq.setTextColor(getResources().getColor(R.color.c_white));
		Button bq1 = alert.getButton(DialogInterface.BUTTON_POSITIVE);
		bq1.setBackgroundColor(getResources().getColor(R.color.c_header_color));
		bq1.setTextColor(getResources().getColor(R.color.c_white));
		hideKeyboard();

	}

	public void setUserdata() {

		UserRegister user = null;
		user = Preferences.getPreferenceInstance().getUserData(getActivity());
		Log.e("user.getFirstName()", user.getFirstName());
		tv_name.setText(user.getFirstName());
		tv_last_name.setText(user.getLastName());
		tv_email.setText(user.getUserEmail());
		tv_password.setText(user.getPass());
		tv_zipcode.setText(user.getZipcode());
		tv_phone_no.setText(user.getPhone());

		edit_name_new.setText(user.getFirstName());
		edit_last_name.setText(user.getLastName());
		// edit_email.setText(user.getUserEmail());
		edit_zipcode.setText(user.getZipcode());
		edit_phone_no.setText(user.getPhone());

		Log.e("edit_name.getText()", edit_name_new.getText().toString());
		progress_sett.setVisibility(View.GONE);
		scroll.setVisibility(View.VISIBLE);
	}

	public void updateData(final UserRegister user) {
		RestAdapter adapter = Utility.getAdapter();
		AwsCloudSearch i = adapter.create(AwsCloudSearch.class);
		i.javaAwsCloudSearchAPIUpdate(user, new Callback<SignInResponse>() {

			@Override
			public void failure(RetrofitError arg0) {
				Toast.makeText(getActivity(), "Please check your Internet Connection", Toast.LENGTH_LONG).show();
			}

			@Override
			public void success(SignInResponse arg0, Response arg1) {
				Preferences.getPreferenceInstance().loginSession(getActivity(), user);
				Toast.makeText(getActivity(), arg0.getMessage(), Toast.LENGTH_LONG).show();
				progress_sett.setVisibility(View.GONE);
				scroll.setVisibility(View.VISIBLE);
			}

		});

	}

	public void logout() {
		AlertDialog alertDialog = new AlertDialog.Builder(getActivity(), R.style.DialogTheme).create();
		alertDialog.setTitle("Confirm Dialog");
		alertDialog.setMessage("Do you want to Logout?");

		alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Yes", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				if(Utility.isNetworkAvailable(getActivity())){
				progress_sett.setVisibility(View.VISIBLE);
				scroll.setVisibility(View.GONE);
				ServerSideDb.getSeverDbInstance().logout(getActivity());
				progress_sett.setVisibility(View.GONE);
				scroll.setVisibility(View.VISIBLE);
				}
				else
				{
					dialog.dismiss();
					Utility.NoInternet(getActivity(),false);
				}
			}
		});

		alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "No", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {

			}
		});
		alertDialog.show();

	}

	public void login() {
		Utility.replaceFragment(getActivity(), LoginFragment.getInstance(), Utility.TAG_LOGIN, false);
	}

}
