package com.oakjobalerts.fragments;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebSettings.TextSize;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.oakjobalerts.AwsCloudSearch;
import com.oakjobalerts.R;
import com.oakjobalerts.SearchListActivity;
import com.oakjobalerts.modeldata.SignInResponse;
import com.oakjobalerts.modeldata.UserRegister;
import com.oakjobalerts.oakinterface.OakFragmentInterface;
import com.oakjobalerts.preferences.Preferences;
import com.oakjobalerts.utilities.Utility;

public class SignUpFragment extends Fragment {

	private com.google.android.gms.analytics.Tracker ta;
	public static final String PROJECT_NO = "810783849550";
	OakFragmentInterface mOakFragmentInterface;
	View view;

	EditText et_first_name = null;
	TextView txtMenu = null;
	EditText et_last_name = null;
	EditText et_email = null;
	EditText et_phone = null;
	EditText et_password = null;
	EditText et_c_password = null;
	EditText et_zipcode = null;

	CheckBox check_receive = null;
	CheckBox check_agree = null;

	Button butt_register = null;

	String first_name, last_name, email, phone, password, c_password, zipcode;
	private RelativeLayout signup_progress;
	private TextView agree;

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);

		mOakFragmentInterface = (OakFragmentInterface) activity;
	}

	public static SignUpFragment con=null;
	public static SignUpFragment getInstance()
	{
		if(con==null)
		{
			con=new SignUpFragment();
		}
		return con;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub

		try{ ta = Utility.initialiseTracker(getActivity(), "Registration View",Utility.TRACKING_ID);	}
		catch(Exception e)	{	e.printStackTrace(); }
		
		view = inflater.inflate(R.layout.signup, container, false);
		txtMenu = (TextView) getActivity().findViewById(R.id.txt_menu);
		agree = (TextView) view.findViewById(R.id.agree);
		init();
		getTextSpannable();
		mOakFragmentInterface.showNoBtn();
		txtMenu.setText(Utility.TAG_SIGNUP);
		butt_register.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				
				try{ Utility.sendClickEvent(ta, "Registration View", "Click","Register");}
				catch(Exception e){ e.printStackTrace();}
				hideKeyboard();

				first_name = et_first_name.getText().toString();
				last_name = et_last_name.getText().toString();
				email = et_email.getText().toString();
				phone = et_phone.getText().toString();
				password = et_password.getText().toString();
				c_password = et_c_password.getText().toString();
				zipcode = et_zipcode.getText().toString();

				if (first_name.trim().equals("") || last_name.trim().equals("") || email.trim().equals("") || password.trim().equals("")
						|| c_password.trim().equals("")) {
					Toast.makeText(getActivity(), "Please fill all fields", Toast.LENGTH_LONG).show();
				} else if (check_receive.isChecked() && zipcode.equals("")) {
					Toast.makeText(getActivity(), "Please fill zipcode if you want to get Job alerts",
							Toast.LENGTH_LONG).show();
				} else if (!check_agree.isChecked()) {
					Toast.makeText(getActivity(), "Please check I agree before Register", Toast.LENGTH_LONG).show();
				} else if (!password.equals(c_password)) {
					Toast.makeText(getActivity(), "Password and confirm password is not matched", Toast.LENGTH_LONG)
							.show();
				} else if (!Utility.checkEmail(email)) {
					Toast.makeText(getActivity(), "Invalid Email", Toast.LENGTH_LONG).show();
				} else if (!Utility.isValidMobile(phone)) {
					Toast.makeText(getActivity(), "Invalid Phone No", Toast.LENGTH_LONG).show();
				} else {
					List<String> diviceList = Preferences.getPreferenceInstance().getDevicedata(getActivity());
					signup_progress.setVisibility(View.VISIBLE);
					UserRegister user = new UserRegister(first_name, last_name, email, password, phone, zipcode,
							diviceList.get(0), diviceList.get(1), "Android", "1", "1");
					registerApi(user);
				}
			}
		});

		return view;
	}

	public void registerApi(final UserRegister user) {

		RestAdapter adapter = Utility.getAdapter();
		AwsCloudSearch i = adapter.create(AwsCloudSearch.class);
		i.javaAwsCloudSearchAPIRegister(user, new Callback<SignInResponse>() {
			@Override
			public void failure(RetrofitError arg0) {

				Log.e("a", "failure=" + arg0.getMessage());
				Toast.makeText(getActivity(), "Please check your Internet Connection", Toast.LENGTH_LONG).show();
				signup_progress.setVisibility(View.GONE);
			}

			@Override
			public void success(SignInResponse arg0, Response arg1) {
				signup_progress.setVisibility(View.GONE);
				Log.e("a", "Success=" + arg0.getMessage());
				Toast.makeText(getActivity(), arg0.getMessage(), 1).show();
				if (arg0.getSuccess()) {
					Preferences.getPreferenceInstance().loginSession(getActivity(), user);
					Utility.replaceFragment(getActivity(), SearchFragment.getInstance(), Utility.TAG_HOME, false);
				}
			}
		});

	}

	@Override
	public void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		hideKeyboard();
	}

	public void init() {

		et_first_name = (EditText) view.findViewById(R.id.et_first_name);
		et_email = (EditText) view.findViewById(R.id.et_email);
		et_phone = (EditText) view.findViewById(R.id.et_phone);
		et_password = (EditText) view.findViewById(R.id.et_password);
		et_c_password = (EditText) view.findViewById(R.id.et_c_password);
		et_zipcode = (EditText) view.findViewById(R.id.et_zipcode);
		et_last_name = (EditText) view.findViewById(R.id.et_last_name);

		check_receive = (CheckBox) view.findViewById(R.id.check_receive);
		check_agree = (CheckBox) view.findViewById(R.id.check_agree);

		butt_register = (Button) view.findViewById(R.id.butt_register);
		signup_progress = (RelativeLayout) view.findViewById(R.id.signup_progress);

	}

	public void hideKeyboard() {
		View view = getActivity().getCurrentFocus();
		if (view != null) {
			InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
		}
	}

	public Spannable getTextSpannable() {

		SpannableString wordtoSpan = new SpannableString(
				"I agree to the Terms of Service and Privacy Policy of this site");

		wordtoSpan.setSpan(new ClickableSpan() {
			@Override
			public void onClick(View textView) {
				String url = "http://oakjobalerts.com/termsandservice.php";
				Intent i = new Intent(Intent.ACTION_VIEW);
				i.setData(Uri.parse(url));
				startActivity(i);
			}

			@Override
			public void updateDrawState(TextPaint ds) {
				super.updateDrawState(ds);
				ds.setUnderlineText(false);
				ds.setColor(getActivity().getResources().getColor(R.color.c_header_color));
			}
		}, 14, 31, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

		wordtoSpan.setSpan(new ClickableSpan() {
			@Override
			public void onClick(View arg0) {
				String url = "http://oakjobalerts.com/privacy.php";
				Intent i = new Intent(Intent.ACTION_VIEW);
				i.setData(Uri.parse(url));
				startActivity(i);
			}

			@SuppressLint("ResourceAsColor")
			@Override
			public void updateDrawState(TextPaint ds) {
				super.updateDrawState(ds);
				ds.setUnderlineText(false);
				ds.setColor(getActivity().getResources().getColor(R.color.c_header_color));
			}
		}, 36, 50, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

		agree.setText(wordtoSpan, TextView.BufferType.SPANNABLE);
		agree.setMovementMethod(LinkMovementMethod.getInstance());
		return wordtoSpan;
	}

}