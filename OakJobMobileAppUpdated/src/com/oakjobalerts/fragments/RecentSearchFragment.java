package com.oakjobalerts.fragments;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.oakjobalerts.R;
import com.oakjobalerts.db.OakjobDb;
import com.oakjobalerts.fragments.adpater.RecentSearchAdapter;
import com.oakjobalerts.modeldata.JobLocationData;
import com.oakjobalerts.oakinterface.OakFragmentInterface;
import com.oakjobalerts.utilities.Utility;


public class RecentSearchFragment extends Fragment{

	private com.google.android.gms.analytics.Tracker ta;
	RecentSearchAdapter adapter;
	OakFragmentInterface mOakFragmentInterface;
	ListView lstRecentSearch;
	RelativeLayout top_rl;
	List<JobLocationData> dataList;
	
	private static RecentSearchFragment fragment = null;
	
	ImageView imMenu = null;
	TextView txtMenu = null;
	ImageView imLogout = null;
	ImageView imDelete=null;
	Context context;
	private RecentSearchFragment(){
		
	}
	
	public static RecentSearchFragment getInstance(){
		if(fragment == null)
			fragment = new RecentSearchFragment();
		
		return fragment;
	}

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		context=activity;
		mOakFragmentInterface = (OakFragmentInterface)activity;
	}
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub

		try{ ta = Utility.initialiseTracker(getActivity(), "Recent Search List View",Utility.TRACKING_ID);	}
		catch(Exception e)	{	e.printStackTrace(); }
		
		View view = inflater.inflate(R.layout.searchlist ,container, false);
		try {
			imMenu		= (ImageView)getActivity().findViewById(R.id.im_menu);
			txtMenu     = (TextView)getActivity().findViewById(R.id.txt_menu);
			imLogout    = (ImageView)getActivity().findViewById(R.id.im_logout);
			imDelete    = (ImageView)getActivity().findViewById(R.id.im_delete);
			
			txtMenu.setText(Utility.TAG_RECENT_SEARCH);
			mOakFragmentInterface.showDeleteBtn();
			
//			imMenu.setImageResource(R.drawable.menu);
//			imLogout.setImageResource(R.drawable.edit_icon);
//			imLogout.setVisibility(View.GONE);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


		lstRecentSearch = (ListView)view.findViewById(R.id.lst_search);
		top_rl= (RelativeLayout)view.findViewById(R.id.top_ll);
		top_rl.setVisibility(View.GONE);
	
		dataList=OakjobDb.getDBInstance(context).getRecentSearchData();
		if(dataList.size()>0)
		{
		adapter = new RecentSearchAdapter(mOakFragmentInterface,dataList,getActivity());
		lstRecentSearch.setAdapter(adapter);
		}
		else
		{
			imDelete.setVisibility(View.GONE);
		}
		imDelete.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				 AlertDialog alertDialog = new AlertDialog.Builder(getActivity(),R.style.DialogTheme).create();
			        alertDialog.setTitle("Clear Recent Search");
			        alertDialog.setMessage("Do you want to clear recent search?");
			        
			        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Yes", new DialogInterface.OnClickListener() {
					
						@Override
						public void onClick(DialogInterface dialog, int which) {
							deleteAllRecentJobs();
							
						}
					});
			        
			        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "No", new DialogInterface.OnClickListener() {
						
						@Override
						public void onClick(DialogInterface dialog, int which) {
							
							
						}
					});
			        alertDialog.show();
								
			}
		});
		
		return view;
	}
	
	@Override
	public void onResume() {
		
		super.onResume();
		if(dataList.size()==0)
		{
			emptyRecentList();
		}
	}
	
public void deleteAllRecentJobs()
{
	OakjobDb.getDBInstance(getActivity()).deleteAllRecentJobs();
	Toast.makeText(getActivity(), "Clear Recent Jobs", Toast.LENGTH_LONG).show();
	dataList.clear();
	adapter.notifyDataSetChanged();
	emptyRecentList();
}

public void emptyRecentList()
{
	lstRecentSearch.setVisibility(View.GONE);
	TextView text_empty_alert=(TextView)getActivity().findViewById(R.id.emptyRecentList);
	text_empty_alert.setText("No Recent Search");
	text_empty_alert.setTextColor(Color.parseColor("#585858"));
	text_empty_alert.setVisibility(View.VISIBLE);
	mOakFragmentInterface.showNoBtn();
}


}
