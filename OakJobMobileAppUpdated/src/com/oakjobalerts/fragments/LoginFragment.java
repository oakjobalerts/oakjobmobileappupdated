package com.oakjobalerts.fragments;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.oakjobalerts.ApplyJob;
import com.oakjobalerts.AwsCloudSearch;
import com.oakjobalerts.R;
import com.oakjobalerts.SearchListActivity;
import com.oakjobalerts.db.ServerSideDb;
import com.oakjobalerts.modeldata.Job_;
import com.oakjobalerts.modeldata.SignInResponse;
import com.oakjobalerts.modeldata.UserRegister;
import com.oakjobalerts.oakinterface.OakFragmentInterface;
import com.oakjobalerts.preferences.Preferences;
import com.oakjobalerts.utilities.Utility;

public class LoginFragment extends Fragment {
	private com.google.android.gms.analytics.Tracker ta;
	// public static String DEVICE_ID = "";
	// public static String DEVICE_TOKEN = "";
	public static final String PROJECT_NO = "810783849550";
	GoogleCloudMessaging mGoogleCloudMessaging;

	TextView txtSignUp = null;
	OakFragmentInterface mOakFragmentInterface;

	private static LoginFragment fragment = null;

	List<Job_> saveJobList = new ArrayList();
	ImageView imMenu = null;
	TextView txtMenu = null;
	ImageView imLogout = null;
	Button butt_sign_in = null;
	EditText et_email = null;
	EditText et_password = null;
	String userName = "";
	boolean status = false;
	private RelativeLayout login_progress;
	String email = "";
	private Button butt_forgot_password;

	private LoginFragment() {

	}

	public static LoginFragment getInstance() {
		if (fragment == null)
			fragment = new LoginFragment();

		return fragment;
	}

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);

		mOakFragmentInterface = (OakFragmentInterface) activity;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		if (Preferences.getPreferenceInstance().getDevicedata(getActivity()).get(1).equalsIgnoreCase(""))
			new RegisterGCMAsyc().execute();
			
		try{ ta = Utility.initialiseTracker(getActivity(), "Login View",Utility.TRACKING_ID);	}
		catch(Exception e)	{	e.printStackTrace(); }
		
		View view = inflater.inflate(R.layout.login, container, false);
		try {
			imMenu = (ImageView) getActivity().findViewById(R.id.im_menu);
			txtMenu = (TextView) getActivity().findViewById(R.id.txt_menu);
			imLogout = (ImageView) getActivity().findViewById(R.id.im_logout);
			butt_sign_in = (Button) view.findViewById(R.id.butt_sign_in);
			butt_forgot_password=(Button) view.findViewById(R.id.butt_forgot_password);
			et_password = (EditText) view.findViewById(R.id.et_password);
			et_email = (EditText) view.findViewById(R.id.et_email);
			login_progress = (RelativeLayout) view.findViewById(R.id.login_progress);

			txtMenu.setText(Utility.TAG_LOGIN);
			mOakFragmentInterface.showNoBtn();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		txtSignUp = (TextView) view.findViewById(R.id.txt_signup);

		txtSignUp.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				mOakFragmentInterface.oakFragmentListner(new SignUpFragment(), Utility.TAG_SIGNUP, false);
				txtMenu.setText(Utility.TAG_SIGNUP);
			}
		});

		butt_forgot_password.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				String url="http://oakjobalerts.com/forgot_password.php?platform=app";
//				Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
//				startActivity(i);
				
				Intent i = new Intent(getActivity(), ApplyJob.class);
				i.putExtra("url", url);
				startActivity(i);
				
			}
		});
		
		butt_sign_in.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				try{ Utility.sendClickEvent(ta, "Login View", "Click","Login");}
				catch(Exception e){ e.printStackTrace();}

				hideKeyboard();
				email = et_email.getText().toString();
				String password = et_password.getText().toString();
				if (email.equals("") || password.equals("")) {
					Toast.makeText(getActivity(), "Please Fill all Fields", Toast.LENGTH_LONG).show();
				} else if (!checkEmail(email)) {
					Toast.makeText(getActivity(), "Email is not Valid", Toast.LENGTH_LONG).show();
				} else {
					List<String> deviceData = Preferences.getPreferenceInstance().getDevicedata(getActivity());
					if (deviceData.size() > 0) {
						System.out.println("Registering for GCM");
						login_progress.setVisibility(View.VISIBLE);
						UserRegister loginList = new UserRegister("", "", email, password, "", "", deviceData.get(0),
								deviceData.get(1), "Android", "1", "1");
						resultApi(loginList);
					}

				}
			}
		});

		return view;
	}

	boolean checkEmail(String email) {
		String regex = "^[a-zA-Z0-9_!#$%&'*+/=?`{|}~^-]+(?:\\.[a-zA-Z0-9_!#$%&'*+/=?`{|}~^-]+)*@[a-zA-Z0-9-]+(?:\\.[a-zA-Z0-9-]+)*$";

		Pattern pattern = Pattern.compile(regex);

		Matcher matcher = pattern.matcher(email);
		System.out.println(email + " : " + matcher.matches());
		return (matcher.matches());
	}

	public void resultApi(UserRegister loginList) {

		RestAdapter adapter = Utility.getAdapter();
		AwsCloudSearch i = adapter.create(AwsCloudSearch.class);

		// i.javaAwsCloudSearchAPILogin(loginList.getUserEmail(),
		// loginList.getPass(), new Callback<SignInResponse>() {
		i.javaAwsCloudSearchAPILogin(loginList, new Callback<SignInResponse>() {
			@Override
			public void failure(RetrofitError arg0) {
				// TODO Auto-generated method stub
				login_progress.setVisibility(View.GONE);

				Utility.checkInternerConnection(getActivity());
				Log.e("a", "failure=" + arg0.getMessage());

			}

			@Override
			public void success(SignInResponse arg0, Response arg1) {
				// TODO Auto-generated method stub
				login_progress.setVisibility(View.GONE);
				Log.e("a", arg0.getMessage());
				if (arg0.getResponse() != null) {
					userName = arg0.getResponse().getFirstName() + " " + arg0.getResponse().getLastName();
				}

				if (arg0.getSuccess()) {

					Preferences.getPreferenceInstance().loginSession(getActivity(), arg0.getResponse());
					et_email.setText("");
					et_password.setText("");
					Utility.replaceFragment(getActivity(), SearchFragment.getInstance(), Utility.TAG_HOME, false);
					txtMenu.setText(Utility.TAG_HOME);
					
					getSaveJobsFromServer(email);
					getAlertsFromServer();
					
					if (Preferences.getPreferenceInstance().getDevicedata(getActivity()).get(1).equalsIgnoreCase(""))
						new RegisterGCMAsyc().execute();
					
				}
				Toast.makeText(getActivity(), arg0.getMessage(), Toast.LENGTH_LONG).show();
			}
		});

	}

	@Override
	public void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		et_email.setText("");
		et_password.setText("");
	}

	public void hideKeyboard() {
		View view = getActivity().getCurrentFocus();
		if (view != null) {
			InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
		}
	}

	public void getSaveJobsFromServer(String email) {
		ServerSideDb.getSeverDbInstance().getSaveJobsFromServer(getActivity(), email);
	}

	public void getAlertsFromServer() {
		ServerSideDb.getSeverDbInstance().getAlertFromServer(getActivity());
	}

	class RegisterGCMAsyc extends AsyncTask<Void, Void, String> {

		String DEVICE_TOKEN = "";

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			mGoogleCloudMessaging = GoogleCloudMessaging.getInstance(getActivity());

		}

		@Override
		protected String doInBackground(Void... params) {
			try {

				DEVICE_TOKEN = mGoogleCloudMessaging.register(PROJECT_NO);

			} catch (Exception e) {
				e.printStackTrace();
			}
			return DEVICE_TOKEN;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);

			if (result != null && !DEVICE_TOKEN.equalsIgnoreCase("")) {
				System.out.println(result);
				String DEVICE_ID = Settings.Secure.getString(LoginFragment.this.getActivity().getApplicationContext()
						.getContentResolver(), Settings.Secure.ANDROID_ID);
				Preferences.getPreferenceInstance().setDevicedata(getActivity(), DEVICE_ID, DEVICE_TOKEN);

				Log.e("deviceId=", DEVICE_ID);
				Log.e("deviceToken=", "token=" + DEVICE_TOKEN);

			}
		}
	}

}
