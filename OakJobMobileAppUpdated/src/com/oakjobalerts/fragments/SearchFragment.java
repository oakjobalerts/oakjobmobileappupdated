package com.oakjobalerts.fragments;



import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.oakjobalerts.BrowseActivity;
import com.oakjobalerts.MainActivity;
import com.oakjobalerts.R;
import com.oakjobalerts.SearchListActivity;
import com.oakjobalerts.db.OakjobDb;
import com.oakjobalerts.db.ServerSideDb;
import com.oakjobalerts.fragments.adpater.RecentSearchAdapter;
import com.oakjobalerts.modeldata.JobLocationData;
import com.oakjobalerts.oakinterface.OakFragmentInterface;
import com.oakjobalerts.preferences.Preferences;
import com.oakjobalerts.utilities.Utility;

public class SearchFragment extends Fragment{

	private com.google.android.gms.analytics.Tracker ta;
	Button btnSearch;
	ListView lstRecent;
	RecentSearchAdapter adapter;
	OakFragmentInterface mOakFragmentInterface;
	ImageView imMenu = null;
	TextView txtMenu = null;
	ImageView imLogout = null;
	Context context;
	TextView browseText;
	EditText what=null, where=null;
	private static SearchFragment fragment = null;

	private SearchFragment(){

	}

	public static SearchFragment getInstance(){
		if(fragment == null)
			fragment = new SearchFragment();

		return fragment;
	}

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);

		mOakFragmentInterface = (OakFragmentInterface)activity;
		context=activity;
	}
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub

		try{ta = Utility.initialiseTracker(getActivity(), "Home View",Utility.TRACKING_ID);	}
		catch(Exception e)	{ e.printStackTrace();	}
		
		View view = inflater.inflate(R.layout.home ,container, false);
		
		try {
			imMenu		= (ImageView)getActivity().findViewById(R.id.im_menu);
			txtMenu     = (TextView)getActivity().findViewById(R.id.txt_menu);
			imLogout = (ImageView)getActivity().findViewById(R.id.im_logout);
			what=(EditText)view.findViewById(R.id.et_what);
			what.requestFocus();
			where=(EditText)view.findViewById(R.id.et_where);	
			lstRecent =(ListView)view.findViewById(R.id.lst_recent);
			
			txtMenu.setText(Utility.TAG_HOME);
			checkLogin();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
//		browseText=(TextView)view.findViewById(R.id.browse_text);
//		
//		browseText.setOnClickListener(new OnClickListener() {
//			
//			@Override
//			public void onClick(View v) {
//		//		mOakFragmentInterface.initiateNewActivity(BrowseActivity.class, Utility.TAG_BROWSE);
//			}
//		}); 

		btnSearch = (Button)view.findViewById(R.id.btn_search);
		btnSearch.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				try{ Utility.sendClickEvent(ta, "Home View", "Click","Find Jobs");}
				catch(Exception e){ e.printStackTrace();}

				String keyword=what.getText().toString();
				String location=where.getText().toString();
				
				if(keyword.trim().equals("") || location.trim().equals(""))
				{
					Toast.makeText(context,"Please fill the above fields", Toast.LENGTH_LONG).show();
				}
				else{
					if(keyword.contains("'"))keyword= keyword.replaceAll("'"," ");
					if(location.contains("'"))location= location.replaceAll("'"," ");
					OakjobDb.getDBInstance(context).insertToRecentJobs(keyword, location);
					mOakFragmentInterface.hideKeyboard();
					mOakFragmentInterface.initiateSearchResultActivity(keyword,location,SearchListActivity.class, Utility.TAG_SEARCH_LIST);
				}
//			mOakFragmentInterface.oakFragmentListner(new SearchListFragment(), Utility.TAG_SEARCH_LIST,true);
				
			}
		});
		
		
		return view;
	}
	
	
	public void checkLogin()
	{
		if(isLogin())
		{
			mOakFragmentInterface.showLogoutBtn();
		}
		else
		{
			mOakFragmentInterface.showMenuBtn();
		}
	
		imLogout.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				logout();
				
			}
		});
	}
	
	public void logout()
	{
		 AlertDialog alertDialog = new AlertDialog.Builder(getActivity(),R.style.DialogTheme).create();
	        alertDialog.setTitle("Confirm Dialog");
	        alertDialog.setMessage("Do you want to Logout?");
	        
	        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Yes", new DialogInterface.OnClickListener() {
			
				@Override
				public void onClick(DialogInterface dialog, int which) {
					if(Utility.isNetworkAvailable(getActivity()))
					{
						ServerSideDb.getSeverDbInstance().logout(getActivity());
						imLogout.setVisibility(View.GONE);
					}
					else
					Utility.NoInternet(getActivity(),true);
				   
				   
				}
			});
	        
	        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "No", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
			
					
				}
			});
	        alertDialog.show();

	}
	
	public boolean isLogin()
	{
		if(Preferences.getPreferenceInstance().islogin(getActivity()))
		{
			return true;
		}
		return false;
	}
	
	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		what.setText("");
		where.setText("");
		List<JobLocationData> dataList=OakjobDb.getDBInstance(context).getRecentSearchData();
		if(dataList.size()>0)
		{
		adapter = new RecentSearchAdapter(mOakFragmentInterface,dataList,getActivity());
		lstRecent.setAdapter(adapter);
		}
	}
	
		// Read all Recent Search data from local DB
	

}

