package com.oakjobalerts.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

import com.oakjobalerts.R;
import com.oakjobalerts.oakinterface.OakFragmentInterface;
import com.oakjobalerts.utilities.Utility;

public class AboutAppFragment extends Fragment{
	
	private com.google.android.gms.analytics.Tracker ta;
	TextView txtVersion = null;
	OakFragmentInterface mOakFragmentInterface;
	WebView web=null;
	String url="";
	TextView txtMenu = null;



	private static AboutAppFragment fragment = null;

	private AboutAppFragment(){
		
	

	}

	public static AboutAppFragment getInstance(String url){
		if(fragment == null)
			fragment = new AboutAppFragment();
		fragment.url=url;
		return fragment;
	}
	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);

		mOakFragmentInterface = (OakFragmentInterface) activity;
	}



	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub

		try{ ta = Utility.initialiseTracker(getActivity(), "About Us View",Utility.TRACKING_ID);	}
		catch(Exception e)	{	e.printStackTrace(); }
		
		View view = inflater.inflate(R.layout.aboutapp ,container, false);
		mOakFragmentInterface.showMenuBtn();
		txtMenu     = (TextView)getActivity().findViewById(R.id.txt_menu);
	//	txtVersion  = (TextView)view.findViewById(R.id.version);
		txtMenu.setText(Utility.TAG_ABOUT);
		web=(WebView)view.findViewById(R.id.web);
		 String text = "<html><body>"
				 + " <div style='vertical-align: center;height:40px; line-height: 100%; color:#75391E;font-size:20px;'>"
				 + "<div style='float:left'><img src='http://oakjobalerts.com/img/logo.png' height='40px' width='40px' align='middle'></div>"

				 + " <div style='float:left;display:table; height:100%; line-height:100%'>"
				 + "<span style='margin-top:20px; display:table-cell;vertical-align: middle; '>&nbsp;&nbsp;OAKJOBALERTS.COM</span></div></div>"
				
				 + "<p style='color:#424242;' align=\"justify\">" 
				 + getString(R.string.About1)+"<br><br>"
				 + getString(R.string.About2)+"<br><br>"
				 + getString(R.string.About3)+"<br><br>"
				 + getString(R.string.About4)+"<br>"
				 + "</p> "
				 +"<div style='width:100%'><a style='margin-left:auto; text-align: center; margin-right:auto; display:block; color:#75391E' href='https://jobalerts.freshdesk.com/support/tickets/new'>Contact Us</a><div><br>"
				 + "</body></html>";
// +"<div style='vertical-align: center; line-height: 100%; color:#75391E;font-size:20px;'><img src='http://oakjobalerts.com/img/logo.png' height='40px' width='40px' align='middle'>&nbsp;&nbsp;&nbsp;<span style='margin-top:10px;'>OAKJOBALERTS.COM</span></div>"
//		web.getSettings().setBuiltInZoomControls(true);
		web.setScrollBarStyle(View.SCROLLBARS_OUTSIDE_INSET);
		web.loadData(text, "text/html", "utf-8");
		web.setWebViewClient(new WebViewClient(){

		    @Override
		    public boolean shouldOverrideUrlLoading(WebView view, String url){
		      view.loadUrl(url);
		      return true;
		    }
		});
		return view;
	}



}
