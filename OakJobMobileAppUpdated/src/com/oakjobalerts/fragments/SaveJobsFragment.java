package com.oakjobalerts.fragments;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.oakjobalerts.AwsCloudSearch;
import com.oakjobalerts.JobDetailActivity;
import com.oakjobalerts.MainActivity;
import com.oakjobalerts.R;
import com.oakjobalerts.SearchListActivity;
import com.oakjobalerts.db.OakjobDb;
import com.oakjobalerts.db.ServerSideDb;
import com.oakjobalerts.fragments.adpater.RecentSearchAdapter;
import com.oakjobalerts.modeldata.Job_;
import com.oakjobalerts.modeldata.SaveJobResponse;
import com.oakjobalerts.modeldata.SignInResponse;
import com.oakjobalerts.oakinterface.OakFragmentInterface;
import com.oakjobalerts.utilities.Utility;

public class SaveJobsFragment extends Fragment{

	private com.google.android.gms.analytics.Tracker ta;
	OakFragmentInterface mOakFragmentInterface;
	Context context;
	ListView savedJobsList;
	SavedJobsListAdapter jobsListAdapter = null;
	ImageView imMenu = null;
	TextView txtMenu = null;
	ImageView im_edit = null;
	RelativeLayout 	top_rl;
	boolean edit_mode=false;
	int deleteJob=0;
	View convertView=null;
	List<Job_> arrJobs = null;
	private LinearLayout empty_list;


	private static SaveJobsFragment fragment = null;

	private SaveJobsFragment(){

	}

	public static SaveJobsFragment getInstance(){
		if(fragment == null)
			fragment = new SaveJobsFragment();

		return fragment;
	}

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);

		mOakFragmentInterface = (OakFragmentInterface) activity;
	}

	@Override
	public void onPause()
	{
		super.onPause();
		edit_mode=false;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub

		View view = inflater.inflate(R.layout.searchlist ,container, false);
		
		//checkLogin();
		
		try{ta = Utility.initialiseTracker(getActivity(), "Saved Job List View",Utility.TRACKING_ID);	}
		catch(Exception e)	{ e.printStackTrace();	}
		
		context = getActivity();
		try {
			imMenu		= (ImageView)getActivity().findViewById(R.id.im_menu);
			txtMenu     = (TextView)getActivity().findViewById(R.id.txt_menu);
			im_edit = (ImageView)getActivity().findViewById(R.id.im_edit);
			empty_list  = (LinearLayout)view.findViewById(R.id.empty_list);
			mOakFragmentInterface.showEditBtn();
			
			txtMenu.setText(Utility.TAG_SAVE_JOBS);
//			imMenu.setImageResource(R.drawable.menu);
//			imLogout.setImageResource(R.drawable.forward);
//			imLogout.setVisibility(View.VISIBLE );
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		top_rl= (RelativeLayout)view.findViewById(R.id.top_ll);
		top_rl.setVisibility(View.GONE);

		
		savedJobsList = (ListView)view.findViewById(R.id.lst_search);

		im_edit.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
			
				if(!edit_mode)
				{
					edit_mode=true;
					jobsListAdapter.notifyDataSetChanged();
				}
				else
				{
					edit_mode=false;
					jobsListAdapter.notifyDataSetChanged();
				}
				
			}
		});
		return view;


	}
	
	
	public class SavedJobsListAdapter extends BaseAdapter{

		
		Context context;


		public SavedJobsListAdapter(List<Job_> arrJobs, Context context) {
			// TODO Auto-generated constructor stub
			SaveJobsFragment.this.arrJobs = arrJobs;
			this.context = context;
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return arrJobs.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return arrJobs.get(position);
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			MyViewHolder mViewHolder;

			if (convertView == null) {
				convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.savejobslistdetail, parent, false);
				mViewHolder = new MyViewHolder(convertView);
				convertView.setTag(mViewHolder);
			} else {
				mViewHolder = (MyViewHolder) convertView.getTag();
			}
			if(edit_mode)
			{
				mViewHolder.im_sv_star.setVisibility(View.GONE);
				mViewHolder.im_sv_delete.setVisibility(View.VISIBLE);
			}
			else
			{
				mViewHolder.im_sv_star.setVisibility(View.VISIBLE);
				mViewHolder.im_sv_delete.setVisibility(View.GONE);
			}
			mViewHolder.tvTitle.setText((arrJobs.get(position).getTitle()).replace("&amp;", "&"));
			mViewHolder.tvCompany.setText(arrJobs.get(position).getEmployer());
			String d[]=arrJobs.get(position).getPostingdate().split("T");
			//mViewHolder.tvDate.setText(d[0]);
		//	mViewHolder.tvSource.setText(arrJobs.get(position).getSourcename());
			
			
			mViewHolder.im_sv_delete.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
				confirmAlert(position);
				}
			});
			
			
			convertView.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					Log.e("position=",""+position);

					Intent intent=new Intent(getActivity(), JobDetailActivity.class);
					intent.putExtra("bundle",arrJobs.get(position));
					startActivity(intent);

				
				}
			});


			return convertView;
		}

	}
	
	@Override
	public void onResume() {
		super.onResume();
		List<Job_> arrJobs  =  OakjobDb.getDBInstance(context).getSaveJobs();
				
		if(arrJobs.size()>0)
		{
			jobsListAdapter = new SavedJobsListAdapter(arrJobs, context);
			savedJobsList.setAdapter(jobsListAdapter);
			im_edit.setVisibility(View.VISIBLE);
		}
		else
		{
			emptyList();
		}
		
	};


	private class MyViewHolder {
		TextView tvTitle, tvCompany,tvDesc;
		ImageView imStar;
		ImageView im_sv_star=null;
		ImageView im_sv_delete=null;
		public MyViewHolder(View item) {
			tvTitle = (TextView) item.findViewById(R.id.tv_sv_title);
			tvCompany = (TextView) item.findViewById(R.id.tv_sv_company);
			tvDesc= (TextView) item.findViewById(R.id.tv_sv_desc);
		//	tvSource = (TextView) item.findViewById(R.id.tv_sv_source);
			//tvDate = (TextView) item.findViewById(R.id.tv_sv_date);
			im_sv_star = (ImageView) item.findViewById(R.id.im_sv_star);
			im_sv_delete=(ImageView) item.findViewById(R.id.im_sv_delete);
		}
	}


	public void confirmAlert(final int position)
	{
		boolean status=false, flag=false;
		 AlertDialog alertDialog = new AlertDialog.Builder(getActivity(),R.style.DialogTheme).create();
	        alertDialog.setTitle("Delete Save Job");
	        alertDialog.setMessage("Do you want to delete the saved job?");
	        
	        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Yes", new DialogInterface.OnClickListener() {
			
				@Override
				public void onClick(DialogInterface dialog, int which) {
					Job_ deleteJob=arrJobs.get(position);		
					if(ServerSideDb.getSeverDbInstance().deleteSaveJobDB(position,getActivity(), deleteJob))
					{
						refreshList(position);
					}
					//OakjobDb.getDBInstance(getActivity()).deleteSavedJob(deleteJob);
					
					if(arrJobs.size()==0)
					{
						emptyList();
					}
				//	Toast.makeText(getActivity(), "Job Deleted", Toast.LENGTH_LONG).show();
					
				}
			});
	        
	        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "No", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					
					
				}
			});
	        alertDialog.show();
	       
	}
	
	public void checkLogin()
	{
		if(!MainActivity.isLogin(getActivity()))
		{
			String msg="Please login to Sync your saved jobs";
			Utility.loginAlert(msg, getActivity());
		}
	}
	
	public void emptyList()
	{
		fragment.savedJobsList.setVisibility(View.GONE);
		TextView text_empty_alert=(TextView)getActivity().findViewById(R.id.text_empty_alert);
		text_empty_alert.setText(R.string.saveJob_text_empty_alerts);
		text_empty_alert.setTextColor(Color.parseColor("#585858"));
		TextView top_text_empty_alert=(TextView)getActivity().findViewById(R.id.top_text_empty_alert);
		top_text_empty_alert.setText(R.string.saveJob_top_text_empty_alerts);
		text_empty_alert.setTextColor(Color.parseColor("#585858"));
		ImageView image=(ImageView)getActivity().findViewById(R.id.image);
		image.setImageResource(R.drawable.savejob);
		fragment.empty_list.setVisibility(View.VISIBLE);
		im_edit.setVisibility(View.GONE);
	}
	
	public void refreshList(int position)
	{
		if(Utility.isNetworkAvailable(getActivity())) 
		{
			arrJobs.remove(position);
			jobsListAdapter.notifyDataSetChanged();
		}
	}

}
