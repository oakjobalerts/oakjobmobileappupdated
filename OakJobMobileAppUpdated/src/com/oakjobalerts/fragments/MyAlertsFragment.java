package com.oakjobalerts.fragments;

import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.oakjobalerts.MainActivity;
import com.oakjobalerts.R;
import com.oakjobalerts.db.OakjobDb;
import com.oakjobalerts.fragments.adpater.RecentSearchAdapter;
import com.oakjobalerts.modeldata.JobLocationData;
import com.oakjobalerts.oakinterface.OakFragmentInterface;
import com.oakjobalerts.utilities.Utility;


public class MyAlertsFragment extends Fragment{

	private com.google.android.gms.analytics.Tracker ta;
	RecentSearchAdapter adapter;
	OakFragmentInterface mOakFragmentInterface;
	ListView lstRecentSearch;
	RelativeLayout top_rl;
	Context context;
	public static boolean edit_mode=false;
	List<JobLocationData> myAlertList=null;
	
	
	private static MyAlertsFragment fragment = null;
	
	ImageView imMenu = null;
	TextView txtMenu = null;
	ImageView imLogout = null;
	ImageView im_edit=null;
	private LinearLayout empty_list=null;
	
	private MyAlertsFragment(){
		
	}
	
	public static MyAlertsFragment getInstance(){
		if(fragment == null)
			fragment = new MyAlertsFragment();
		
		return fragment;
	}

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);

		mOakFragmentInterface = (OakFragmentInterface)activity;
		context=activity;
	}
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		
		try{ta = Utility.initialiseTracker(getActivity(), "Alert list View",Utility.TRACKING_ID);	}
		catch(Exception e){	e.printStackTrace(); }

		View view = inflater.inflate(R.layout.searchlist ,container, false);
		try {
			imMenu		= (ImageView)getActivity().findViewById(R.id.im_menu);
			txtMenu     = (TextView)getActivity().findViewById(R.id.txt_menu);
			imLogout    = (ImageView)getActivity().findViewById(R.id.im_logout);
			im_edit     = (ImageView)getActivity().findViewById(R.id.im_edit);
			empty_list  = (LinearLayout)view.findViewById(R.id.empty_list);
			
			mOakFragmentInterface.showEditBtn();
			txtMenu.setText(Utility.TAG_MY_ALERTS);
			myAlertList=OakjobDb.getDBInstance(context).getJobAlerts();
			
			 //checkLogin();
			 
			 im_edit.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
					
						if(!edit_mode)
						{
							edit_mode=true;
							adapter.notifyDataSetChanged();
						}
						else
						{
							edit_mode=false;
							adapter.notifyDataSetChanged();
						}
					}
				});
			 
//			imMenu.setImageResource(R.drawable.menu);
//			imLogout.setImageResource(R.drawable.edit_icon);
//			imLogout.setVisibility(View.GONE);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


		lstRecentSearch = (ListView)view.findViewById(R.id.lst_search);
		top_rl= (RelativeLayout)view.findViewById(R.id.top_ll);
		top_rl.setVisibility(View.GONE);
	

		return view;
	}
	
	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		if(myAlertList.size()>0)
		{
			im_edit.setVisibility(View.VISIBLE);
			adapter = new RecentSearchAdapter(mOakFragmentInterface,myAlertList,getActivity());
			lstRecentSearch.setAdapter(adapter);
		}
		else
		{
			emptyList();
		}
	}

	@Override
	public void onPause()
	{
		super.onPause();
		edit_mode=false;
	}

	public void emptyList()
	{
		fragment.lstRecentSearch.setVisibility(View.GONE);
		TextView text_empty_alert=(TextView)getActivity().findViewById(R.id.text_empty_alert);
		text_empty_alert.setText(R.string.text_empty_alerts);
		text_empty_alert.setTextColor(Color.parseColor("#585858"));
		TextView top_text_empty_alert=(TextView)getActivity().findViewById(R.id.top_text_empty_alert);
		top_text_empty_alert.setText(R.string.top_text_empty_alerts);
		top_text_empty_alert.setTextColor(Color.parseColor("#585858"));
		ImageView image=(ImageView)getActivity().findViewById(R.id.image);
		image.setImageResource(R.drawable.alert_bar);
		fragment.empty_list.setVisibility(View.VISIBLE);
		im_edit.setVisibility(View.GONE);
	}
	

	public void checkLogin()
	{
		if(!MainActivity.isLogin(getActivity()))
		{
			String msg="Please login to Sync your saved Alerts";
			Utility.loginAlert(msg, getActivity());
		}
	}
	

}
