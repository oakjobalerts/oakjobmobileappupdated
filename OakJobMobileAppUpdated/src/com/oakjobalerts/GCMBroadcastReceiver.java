package com.oakjobalerts;

import java.util.Random;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.oakjobalerts.preferences.Preferences;

public class GCMBroadcastReceiver extends BroadcastReceiver {
	static final String TAG = "GCMDemo";
	public static final int NOTIFICATION_ID = 1;
	private NotificationManager mNotificationManager;
	NotificationCompat.Builder builder;
	Context ctx;
	Random random;

	@Override
	public void onReceive(Context context, Intent intent) {
		Log.e("onReceive", "onReceive");
		random = new Random();

		GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(context);
		ctx = context;
		String buildingId = "";
		String messageType = gcm.getMessageType(intent);
		System.out.println("reciever is" + messageType);
		if (GoogleCloudMessaging.MESSAGE_TYPE_SEND_ERROR.equals(messageType)) {
			// generateNotification(context, "Send error: " +
			// intent.getExtras().toString());
		} else if (GoogleCloudMessaging.MESSAGE_TYPE_DELETED.equals(messageType)) {
			// generateNotification(context, "Deleted messages on server: "
			// + intent.getExtras().toString());
		} else {
			String message = "";

			JSONObject object = null;
			try {
				message = intent.getExtras().getString("default");

				object = new JSONObject(message);
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			// check App is closed or not
			// if (ApplicationClass.isActivityVisible()) {
			// if
			// (Preferences.getInstance(context).getBuildingID().equalsIgnoreCase(buildingId))
			// {
			// Intent in = new Intent(ctx, DialogActivity.class);
			// in.putExtra("message", message);
			// in.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			// ctx.startActivity(in);
			// }
			//
			// } else {

			try {
				String token=Preferences.getPreferenceInstance().getDevicedata(ctx).get(1);
				if (object.getBoolean("isBroadcast")) {
					JSONObject broadcastMessage = object.getJSONObject("broadcastMessage");
//					generateNotification(context, broadcastMessage, true);
					sendLocalNotification(context, broadcastMessage, true);

				} else {
					JSONObject notification = object.getJSONObject("notification");
					System.out.println("aws::::"+notification.getString("deviceToken"));
					System.out.println("aws::::"+token);
					if(notification.getString("deviceToken").equalsIgnoreCase(token))
					{
					if (notification.getString("emailAddress").equalsIgnoreCase(
							Preferences.getPreferenceInstance().getEmail(ctx)) || notification.getString("emailAddress").equalsIgnoreCase("")
							)
//						generateNotification(context, notification, false);
						sendLocalNotification(context, notification, false);
				}
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			// }
		}

	}

	private void generateNotification(Context context, JSONObject message, boolean isBroadCast) {

		int icon = R.drawable.ic_launcher;

		String title = "OakJobAlerts.com";
		
		// notificationIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
		// Intent.FLAG_ACTIVITY_SINGLE_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK);

		
		try {
			NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(ctx)
					.setAutoCancel(true)
					.setContentTitle(title)
					.setSmallIcon(icon)
					.setContentText((String) message.getString("message"))
					.setLargeIcon(BitmapFactory.decodeResource(ctx.getResources(), R.drawable.ic_launcher))
					.setStyle(new NotificationCompat.BigTextStyle().bigText((String) message.getString("message")))
					.setPriority(NotificationCompat.PRIORITY_MAX)
					.setDefaults(
							Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE | Notification.DEFAULT_LIGHTS);
			
			
			Intent notificationIntent = null;

			if (isBroadCast) {
				notificationIntent = new Intent(ctx, ApplyJob.class);
				try {
					notificationIntent.putExtra("url", message.getString("url"));
				} catch (JSONException e) {
					e.printStackTrace();
				}
			} else {
				notificationIntent = new Intent(ctx, SearchListActivity.class);
				try {
					notificationIntent.putExtra("what", message.getString("keyword"));
					notificationIntent.putExtra("where", message.getString("location"));
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			notificationIntent.putExtra("notification", true);
			PendingIntent intent = PendingIntent.getActivity(context, 0, notificationIntent,
					PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_ONE_SHOT);
			
			mBuilder.setContentIntent(intent).setSound(null, Notification.DEFAULT_SOUND);

			NotificationManager mNotificationManager = (NotificationManager) ctx
					.getSystemService(Context.NOTIFICATION_SERVICE);
			mNotificationManager.notify(random.nextInt(1000), mBuilder.build());

		} catch (Exception e) {
			e.printStackTrace();

		}

	}

	public void sendLocalNotification(Context context, JSONObject message, boolean isBroadCast) {

		try {

			Intent notificationIntent = null;
			String mJsonString = null;

			Random random = new Random();
			int no = random.nextInt(1000);

			int icon = R.drawable.ic_launcher;

			NotificationManager notificationManager = (NotificationManager) context
					.getSystemService(Context.NOTIFICATION_SERVICE);
			String contentTitle = "";
			NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context).setSmallIcon(icon)
					.setContentTitle("OakJobAlerts.com")
					.setContentText((String) message.getString("message")).setAutoCancel(true)
					.setDefaults(Notification.DEFAULT_ALL).setPriority(NotificationCompat.PRIORITY_HIGH)
					.setStyle(new NotificationCompat.BigTextStyle().bigText((String) message.getString("message")));

//			NotificationCompat.InboxStyle inboxStyle = new NotificationCompat.InboxStyle();
//			String[] events = new String[6];
//			// Sets a title for the Inbox in expanded layout
//			inboxStyle.setBigContentTitle("Chat Notification");
//
//			// ...
//			// Moves events into the expanded layout
//			for (int i = 0; i < events.length; i++) {
//				inboxStyle.addLine(events[i]);
//			}
			// Moves the expanded layout object into the notification object.
			// mBuilder.setStyle(inboxStyle);

			notificationIntent = new Intent(context, SearchListActivity.class);
			notificationIntent.setAction(Long.toString(System.currentTimeMillis()));

			try {
				notificationIntent.putExtra("what", message.getString("keyword"));
				notificationIntent.putExtra("where", message.getString("location"));
				notificationIntent.putExtra("notification", true);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
			PendingIntent intent = PendingIntent.getActivity(context, 0, notificationIntent,
					PendingIntent.FLAG_UPDATE_CURRENT);
			mBuilder.setContentIntent(intent);
			notificationManager.notify(no, mBuilder.build());
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
}
