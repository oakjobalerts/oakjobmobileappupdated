package com.oakjobalerts;


import java.util.ArrayList;

import com.oakjobalerts.db.OakjobDb;
import com.oakjobalerts.db.ServerSideDb;
import com.oakjobalerts.fragments.LoginFragment;
import com.oakjobalerts.modeldata.*;
import com.oakjobalerts.oakinterface.OakFragmentInterface;
import com.oakjobalerts.preferences.Preferences;
import com.oakjobalerts.utilities.Utility;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.codec.binary.Base64;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.RestAdapter.LogLevel;
import retrofit.client.Response;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.webkit.JavascriptInterface;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.Toast;

public class SearchListActivity extends Activity{

	private com.google.android.gms.analytics.Tracker ta;
	//OakFragmentInterface mOakFragmentInterface;
	List<Job_> list;
//	List<Job_> listJobItems=new ArrayList();
	RestAdapter adapter;
	final int NUM_LIST_ITEM=15;
	int increment=0,start=0,offset,stop=0;
	ListView lstJobs;
	JobsListAdapter jobsListAdapter = null;
	Context context;
	ImageView imBack = null;
	TextView txtMenu = null;
	TextView searchItemText=null;
	TextView resultNoText=null;
	ImageView imEdit = null;
	ImageView imSearch=null;
	ImageView imgFav=null;
	String searchWhat="",searchWhere="";
	MyViewHolder mViewHolder;
	int position;
	Button butt_save_as_alert=null;
	Button butt_sort_by=null;
	Button sort_by_relevance=null;
	Button sort_by_date=null;
	LinearLayout sort_by_layout=null;
	String sort_by="relevance";
	ProgressBar mProgressBar;
	Boolean isJobAlertsaved;
	List<String> savedJobIds;
	public static boolean saveAlertStatus=true;
	int totalItems;
	boolean paginationStatus=true;
	private ProgressBar progress_pagination;
	
	boolean firstTimeStatusFlag=true;
//	public static SearchListActivity mSearchListActivity;
//	
//	public SearchListActivity() {
//		mSearchListActivity=this;
//	}
	public static SearchListActivity con=null;
	public static SearchListActivity getInstance()
	{
		if(con==null)
		{
			con=new SearchListActivity();
		}
		return con;
	}

	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		totalItems=0;
		setContentView(R.layout.searchlist_act);
		
		Intent searchIntent=getIntent();
		searchWhat=searchIntent.getStringExtra("what").trim();
		searchWhere=searchIntent.getStringExtra("where").trim();
		
		boolean flag=false;
		try{ flag=searchIntent.getBooleanExtra("notification",false);}
		catch(Exception e){ e.printStackTrace();}
		
		try{ if(flag) ta = Utility.initialiseTracker(this, "Notification View",Utility.TRACKING_ID); 
			ta = Utility.initialiseTracker(this, "Search List View",Utility.TRACKING_ID); }
		catch(Exception e)	{	e.printStackTrace(); }

			list=new ArrayList<Job_>();
			imBack		= (ImageView)findViewById(R.id.im_back);
			txtMenu     = (TextView)findViewById(R.id.txt_menu);
			imEdit = (ImageView)findViewById(R.id.im_edit);
			imSearch= (ImageView)findViewById(R.id.im_search);
			context=SearchListActivity.this;
			lstJobs = (ListView)findViewById(R.id.lst_search);
			
			progress_pagination=new ProgressBar(SearchListActivity.this);
			butt_save_as_alert=(Button)findViewById(R.id.butt_save_as_alert);
			searchItemText=(TextView)findViewById(R.id.seach_item_txt);
			searchItemText.setText(searchWhat+" - "+searchWhere);
			resultNoText=(TextView)findViewById(R.id.results_no_txt);
			butt_sort_by=(Button)findViewById(R.id.butt_sort_by);
			sort_by_layout=(LinearLayout)findViewById(R.id.sort_by_layout);
			sort_by_relevance=(Button)findViewById(R.id.sort_by_relevance);
			sort_by_date=(Button)findViewById(R.id.sort_by_date);
			mProgressBar=(ProgressBar)findViewById(R.id.progress);
			
			
			setDrawable();
			
			
			
			if(checkJobAlert())
			{
				Drawable img = getResources().getDrawable( R.drawable.alerts);
				img.setBounds( 0, 0, 51, 51 );
				butt_save_as_alert.setCompoundDrawables( null, img, null, null );
				butt_save_as_alert.setText("Saved Alert");
				butt_save_as_alert.setTextColor(Color.parseColor("#7CC243"));
			}
			
			sort_by_relevance.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					savedJobIds=OakjobDb.getDBInstance(context).getSavedJobId(); 
					mProgressBar.setVisibility(View.VISIBLE);
					sort_by_relevance.setBackgroundResource(R.color.c_header_color);
					sort_by_date.setBackgroundResource(R.color.c_grey);
					sort_by_layout.setVisibility(v.GONE);
					Drawable img = getResources().getDrawable( R.drawable.active_shot);
					img.setBounds( 0, 0, 61, 38 );
					butt_sort_by.setCompoundDrawables( null, img, null, null );
					butt_sort_by.setTextColor(Color.parseColor("#979797"));
					list.clear();
					lstJobs.removeFooterView(progress_pagination);
					lstJobs.addFooterView(progress_pagination);
					stop=0;
					getList(0, "relevance");
				}
			});

			sort_by_date.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					savedJobIds=OakjobDb.getDBInstance(context).getSavedJobId(); 
					mProgressBar.setVisibility(View.VISIBLE);
					sort_by_relevance.setBackgroundResource(R.color.c_grey);
					sort_by_date.setBackgroundResource(R.color.c_header_color);
					sort_by_layout.setVisibility(v.GONE);
					Drawable img = getResources().getDrawable( R.drawable.active_shot);
					img.setBounds( 0, 0, 61, 38 );
					butt_sort_by.setCompoundDrawables( null, img, null, null );
					butt_sort_by.setTextColor(Color.parseColor("#979797"));
					list.clear();
					lstJobs.removeFooterView(progress_pagination);
					lstJobs.addFooterView(progress_pagination);
					stop=0;
					getList(0, "date");
					
				}
			});
			
			butt_sort_by.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					
					if(sort_by_layout.getVisibility()==v.GONE)
					{
					sort_by_layout.setVisibility(v.VISIBLE);
					Drawable img = getResources().getDrawable( R.drawable.shot);
					img.setBounds( 0, 0, 61, 38 );
					butt_sort_by.setCompoundDrawables( null, img, null, null );
					butt_sort_by.setTextColor(Color.parseColor("#7CC243"));
					}
					else{
						sort_by_layout.setVisibility(v.GONE);
						Drawable img = getResources().getDrawable( R.drawable.active_shot);
						img.setBounds( 0, 0, 61, 38 );
						butt_sort_by.setCompoundDrawables( null, img, null, null );
						butt_sort_by.setTextColor(Color.parseColor("#979797"));
						
					}
				}
			});
			
			butt_save_as_alert.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					
					try{ Utility.sendClickEvent(ta, "Search List View", "Click","Save Alerts");}
					catch(Exception e){ e.printStackTrace();}
					
					if(checkJobAlert())
					{
						Toast.makeText(SearchListActivity.this,"Alert Already Saved" , Toast.LENGTH_SHORT).show();
					}
//					else if(!MainActivity.isLogin(SearchListActivity.this) )
//					{
//						if(!Preferences.getPreferenceInstance().getLoginAlertStatus(SearchListActivity.this))
//						{
//							Job_ job=new Job_();
//							job.setCity(searchWhere);
//							job.setTitle(searchWhat);
//							loginAlertOnSearch(job,0,"Please login to save alerts, otherwise it will save locally. You will not receive any notification", SearchListActivity.this);
//						}
//						else
//						{
//							OakjobDb.getDBInstance(con).insertToJobAlerts(searchWhat, searchWhere);
//							changeAlertIcon();
//						}
//						//Toast.makeText(SearchListActivity.this,"Please Login to save alert" , Toast.LENGTH_SHORT).show();
//					}
					else if(Utility.isNetworkAvailable(SearchListActivity.this))
					{
						if(saveAlertStatus)
						ServerSideDb.getSeverDbInstance().saveMyAlert(searchWhat, searchWhere, SearchListActivity.this);
					}
					else
					{
						 openDialog();
					//	Utility.NoInternet(SearchListActivity.this);
						//Toast.makeText(SearchListActivity.this,"Please Check Your Internet Connection" , Toast.LENGTH_SHORT).show();
					}
					
				}
			});
			
			imEdit.setVisibility(View.GONE);
			imSearch.setVisibility(View.VISIBLE);
			
			txtMenu.setText(Utility.TAG_SEARCH_LIST);
		
			
			imSearch.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					MainActivity.search_flag=1;
					finish();
				}
			});
			
			imBack.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					
					finish();
				}
			});
			
			 adapter=new RestAdapter.Builder()
				.setEndpoint("http://s2.oakjobalerts.com:8080").build();
			 mProgressBar.setVisibility(View.VISIBLE);
				
			 if(Utility.isNetworkAvailable(SearchListActivity.this))
			 {
				 getList(0,sort_by);					  
			 }
			 else{
				// Utility.NoInternet(this);
				 openDialog();
			 }
			 
				lstJobs.addFooterView(progress_pagination);
				 lstJobs.setOnScrollListener(new OnScrollListener() {
					int totalVisibleCount,firstVisibleItem=0,totalItemCount;
					@Override
					public void onScroll(AbsListView arg0, int firstVisibleItem, int totalVisibleCount, int totalItemCount) {
						this.firstVisibleItem=firstVisibleItem;
						this.totalVisibleCount=totalVisibleCount;
						this.totalItemCount=totalItemCount;
					}

					@Override
					public void onScrollStateChanged(AbsListView arg0, int scrollState) {
						int lastItem = firstVisibleItem + totalVisibleCount;
						if(lastItem == totalItemCount && stop==0 ){
							
							if(paginationStatus==true)
							{
								increment++;
								//lstJobs.addFooterView(progress_pagination);
								paginationStatus=false;
								getList(increment,sort_by);
							}
						}
						else if(stop==1){
							lstJobs.removeFooterView(progress_pagination);
						}


					}
			 
								
				});
				 				 
			 
	}
	
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		savedJobIds=OakjobDb.getDBInstance(context).getSavedJobId(); 		

		
		if(jobsListAdapter!=null){
			jobsListAdapter.notifyDataSetChanged(); }
		
	}
	
	public String calculateDays(String postingDate)
	{
		if(!postingDate.equals("")){
		String[] date=postingDate.split("T");
		String ad[]=date[0].split("-");
		Calendar c=Calendar.getInstance();
		c.set(Integer.parseInt(ad[0]),Integer.parseInt(ad[1])-1,Integer.parseInt(ad[2]));
		Date d=c.getTime();
		Date d1=new Date(System.currentTimeMillis());
		String days=String.valueOf((d1.getTime()-d.getTime())/1000/60/60/24);
		if(days.equalsIgnoreCase("0"))
		{
			return("Today");
		}
		return(days+" days ago");
		}
		return("");
	}
	
	
	
	public void getList(final int num,String sort_by)
	{
		if(this.sort_by!=sort_by)
		{
			increment=0;
			this.sort_by=sort_by;
			list.clear();
		}
		if(num==0)
		start=0;
		AwsCloudSearch i=adapter.create(AwsCloudSearch.class);
		adapter.setLogLevel(LogLevel.FULL );
		i.SearcApi(searchWhat,searchWhere,String.valueOf(start),String.valueOf(NUM_LIST_ITEM),sort_by,new Callback<Job>(){
		
			public void failure(RetrofitError arg0) {
				// TODO Auto-generated method stub
				Toast.makeText(SearchListActivity.this,"No matching job found", 1).show();
				mProgressBar.setVisibility(View.GONE);
			}

			@Override
			public void success(Job arg0, Response arg1) {
				try{
				if(arg0!=null)
				{
					if(totalItems==0 || totalItems<15)
					{
						totalItems=arg0.getTotal_found();					
						if(totalItems<15)
							lstJobs.removeFooterView(progress_pagination);
					}
					if(totalItems<=start)
					{
						stop=1;
						
					}
					else
					{
						
						list.addAll(arg0.getJobs());
						
						if(num==0)
						{
							resultNoText.setText(String.valueOf(totalItems)+" results");
							jobsListAdapter=new JobsListAdapter(list, context);
							lstJobs.setAdapter(jobsListAdapter);
						}
						else
						{
							jobsListAdapter.notifyDataSetChanged();
						}
						start=start+NUM_LIST_ITEM;
					}
					if(list.size()==0)
					{
						Toast.makeText(SearchListActivity.this,"No matching job found", 1).show();
						stop=1;
					}
					
				}
				
				paginationStatus=true;
				mProgressBar.setVisibility(View.GONE);
				}catch(Exception e){e.printStackTrace();}
				if(firstTimeStatusFlag){
					if(totalItems>0)
						showSavealertDialog(totalItems);
					firstTimeStatusFlag=false;
					}
			}		
				});
	}

	void showSavealertDialog(int totalJobs)
	{
		
		 if(Utility.isNetworkAvailable(SearchListActivity.this))
		 {
			 if(!checkJobAlert()){
				 String status=Preferences.getPreferenceInstance().getSaveAlertStatus(this);
				 if(!status.equals("1") && !status.equals("2"))
				 saveAlertOnSearch(this,totalJobs);
				 else if(status.equals("2")){
				 	 ServerSideDb.getSeverDbInstance().saveMyAlert(searchWhat, searchWhere, SearchListActivity.this);	 
					}
			 }
				  
		 }
	}
	
	public class JobsListAdapter extends BaseAdapter{

		List<Job_> arrJobs = null;
		Context context;


		public JobsListAdapter(List<Job_> arrJobs, Context context) {
			// TODO Auto-generated constructor stub
			this.arrJobs = arrJobs;
			this.context = context;
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return arrJobs.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return arrJobs.get(position);
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {
			MyViewHolder mViewHolder;
			SearchListActivity.this.position=position;
			
			if (convertView == null) {
				convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.searchlistdetail, parent, false);
				mViewHolder = new MyViewHolder(convertView);
				convertView.setTag(mViewHolder);
			} else {
				mViewHolder = (MyViewHolder) convertView.getTag();
			}
			
			final Job_ currentJobData = (Job_) getItem(position);

			String title= Html.fromHtml(currentJobData.getTitle()).toString();
			String employer=Html.fromHtml(currentJobData.getEmployer()).toString();
			String city=Html.fromHtml(currentJobData.getCity()).toString();
			String state=Html.fromHtml(currentJobData.getState()).toString();
					
			mViewHolder.tvTitle.setText(title.replace("&amp;", "&"));
			mViewHolder.tvCompany.setText( employer.replace("&amp;", "&")+ "-" +city.replace("&amp;", "&")  + ", " + state.replace("&amp;", "&"));

	//		mViewHolder.tvSource.setText(currentJobData.getDisplaysource());
		//    mViewHolder.tvDate.setText(calculateDays(currentJobData.getPostingdate()));
		    
		    final boolean isFav;
		    
		    if(currentJobData.isFav() || savedJobIds.contains(currentJobData.getTitle()+"####"+currentJobData.getEmployer()))
		    {
		    	isFav = true;
		    	mViewHolder.imFav.setImageResource(R.drawable.save_star);
		    }else{
		    	isFav = false;
		    	mViewHolder.imFav.setImageResource(R.drawable.star);
		    }
		    
	

		    mViewHolder.imShare.setOnClickListener( new OnClickListener(){
		   
				@Override
				public void onClick(View v) {
					mProgressBar.setVisibility(View.VISIBLE);
					String url="http://www.oakjobalerts.com/RedirectWEB.php?q="+encodeUrl(list.get(position))+"&token="+currentJobData.getJobId();
					shareurl(url,currentJobData.getTitle(),currentJobData.getEmployer());
					
//					Intent shareIntent= new Intent(Intent.ACTION_SEND);
//					String shareUrl="Check out this job on Oak Job Alerts,<br><br>I thought you might be interested in this job offer on Oak Job Alerts.<br><br><a href="+ currentJobData.getJoburl()+">"+currentJobData.getTitle()+"<br>"+currentJobData.getEmployer()+"</a>";
//					shareIntent.putExtra(android.content.Intent.EXTRA_SUBJECT,"Job For You");
//					shareIntent.putExtra(android.content.Intent.EXTRA_TEXT,Html.fromHtml(shareUrl));
//					shareIntent.setType("text/plain");
//					startActivity(Intent.createChooser(shareIntent, "Share with"));
				}
		    	
		    });
		    	
		    mViewHolder.imFav.setOnClickListener( new OnClickListener(){

				@Override
				public void onClick(View v) {
					
					if(MainActivity.isLogin(context))
					{
					
						if(isFav){
							Toast.makeText(context,"Job already saved" , Toast.LENGTH_LONG).show();
						}
						else if(Utility.isNetworkAvailable(context))
						{			
							currentJobData.setEmail(Preferences.getPreferenceInstance().getEmail(SearchListActivity.this));
							ServerSideDb.getSeverDbInstance().saveJobsAtServer(currentJobData,position,SearchListActivity.this);						
						}
						else
						{
							Toast.makeText(context,"Please check your internet connection" , Toast.LENGTH_LONG).show();
						}
					}
					else{
						if(isFav){
							Toast.makeText(context,"Job already saved" , Toast.LENGTH_LONG).show();
						}
						else
						{
							if(!Preferences.getPreferenceInstance().getLoginAlertStatus(SearchListActivity.this))
							{
									String msg="Jobs will only save locally. Please login to get the benefit of viewing your saved jobs on all devices. ";
									loginAlertOnSearch(currentJobData,position,msg, SearchListActivity.this);
							}
							else{
								saveJobsAtServer(position,currentJobData);
							}
						}
						//Toast.makeText(context,"Please Login to save Jobs" , Toast.LENGTH_LONG).show();
					}
				}
		    	
		    });
		    
			mViewHolder.btnApply.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					String jobId = list.get(position).getId(); 
					String url="http://www.oakjobalerts.com/RedirectWEB.php?q="+encodeUrl(list.get(position))+"&token="+jobId;			
					Intent i = new Intent(SearchListActivity.this, ApplyJob.class);
					i.putExtra("url", url);
					startActivity(i);
//					Intent i = new Intent(Intent.ACTION_VIEW,Uri.parse(url));
//					startActivity(i);
				}
			});

			
			convertView.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
//					Intent intent=new Intent(SearchListActivity.this, JobDetailActivity.class);
//					intent.putExtra("bundle",currentJobData);
//					startActivity(intent);
					String jobId = list.get(position).getId(); 
					String url="http://www.oakjobalerts.com/RedirectWEB.php?q="+encodeUrl(list.get(position))+"&token="+jobId;			
					Intent i = new Intent(SearchListActivity.this, ApplyJob.class);
					i.putExtra("url", url);
					startActivity(i);
				}
			});

			return convertView;
		}

	}
	
	void shareurl(String url,final String title, final String employer)
	{
		 if(Utility.isNetworkAvailable(this))
		 {
		 RestAdapter adapter=Utility.getAdapter();
		 AwsCloudSearch inter=adapter.create(AwsCloudSearch.class);
		 inter.javaAwsCloudSearchAPIredirecturl(url, new Callback<String>() {

			@Override
			public void failure(RetrofitError arg0) {
				
				
			}

			@Override
			public void success(String arg0, Response arg1) {
				
				 Intent shareIntent= new Intent(Intent.ACTION_SEND);
				// String shareUrl="Check out this job on Oak Job Alerts,<br><br>I thought you might be interested in this job offer on Oak Job Alerts.<br><br><a href="+ url+"> Sales Job</a>";
				 String shareUrl="Check out this job on Oak Job Alerts,\n\n"
				 		+ title.replace("&amp;", "&")+" - "+employer+"\n"
				 		+ "http://oakjobalerts.com/Redirect.php?id="+arg0;
				  shareIntent.putExtra(android.content.Intent.EXTRA_TEXT,shareUrl);
					shareIntent.setType("text/plain");
					startActivity(Intent.createChooser(shareIntent, "Share with"));
					mProgressBar.setVisibility(View.GONE);
			}
			 
		});
		 
		 }
		 else
		 {
			 Utility.NoInternet(this,false);
		 }
	}
	
	public boolean checkJobAlert()
	{
		Boolean isJobalertSaved=OakjobDb.getDBInstance(context).isJobAlertSaved(searchWhat,searchWhere);
		if(isJobalertSaved){ return true; }
		return false;
	}
	
	private class MyViewHolder {
		TextView tvTitle, tvCompany; //,tvDate,tvSource;
		ImageView imFav, imShare;
		Button btnApply; 

		public MyViewHolder(View item) {
			tvTitle = (TextView) item.findViewById(R.id.tv_title);
			tvCompany = (TextView) item.findViewById(R.id.tv_company);
	//		tvSource = (TextView) item.findViewById(R.id.tv_source);
//			tvDate = (TextView) item.findViewById(R.id.tv_date);
			imFav = (ImageView) item.findViewById(R.id.im_fav);
			imShare= (ImageView) item.findViewById(R.id.im_share);

			btnApply = (Button)item.findViewById(R.id.btn_apply);
		}
		public boolean isStarActive(int flag)
		{
			if(flag==0)
			{
				imFav.setImageResource(R.drawable.icon_save);
				
			}
			else if(flag==1)
			{
				imFav.setImageResource(R.drawable.star);
				return(true);
			}
			return(false);
		}
	}
	
	public void setDrawable()
	{
		Drawable img = getResources().getDrawable( R.drawable.active_shot);
		img.setBounds( 0, 0, 61, 38 );
		butt_sort_by.setCompoundDrawables( null, img, null, null );

		img = getResources().getDrawable( R.drawable.active_alert);
		img.setBounds( 0, 0, 48, 48 );
		butt_save_as_alert.setCompoundDrawables( null, img, null, null );
	
	}
	
	public String encodeUrl(Job_ job) {
		String JOB_SOURCE = job.getSource();
		String JOB_URL =job.getJoburl();
		String SOURCE_NAME = job.getSourcename();
		String STATE = job.getState();
		String CITY = job.getCity();
		String JOB_TITLE = job.getTitle();
		String EMPLOYER = job.getEmployer();
		String EMAIl=Preferences.getPreferenceInstance().getEmail(SearchListActivity.this);
		Log.d("position",""+position);
		JOB_URL=JOB_URL.replace("&", "##");
		//https://www.ziprecruiter.com/clk/ZKVyubFim_jMoUgb3zUrU0FKdZIA6Dlf6-xWo3gXDM_ShEDt0B_gtcet7GZofVZjqT-Ytusmam6fYeWsLE0JwDXJYqOgQPLDlIxSQMd-qbIxx2tibf2mw7RgcuP5WlSlUnlMmhma-kidcpZhffw770GMq5OeSmpciHsxjUcFAZfiaBNKloIyg8_krq8aAw7qy222VdtBeZeKq1Jy0a7Ym9q5GD8aa9fY-_JbBQYXW8jRvF8Mcp9bOaCpNUNIiiVlhWDRUmi40gqRP5WiQoSvJro31vOpjmMxUDfCiPyQyMbrVClzAaQTBJg6t8k9YAC7ab52v7PWn_7nCJGs9UQj8TjbCVHNQ7x_KXazG-5ClzlyiYAiOnzv_I59-xAcFTCeOYQvshiqqRJMKcXAYe8L8w.511fa34e07d137e48ddda091118ab9cc
		
		String redirectUrl = "url="+JOB_URL+"&sourcename=" + SOURCE_NAME + "&source=" + JOB_SOURCE + "&city=" + CITY + "&state=" + STATE + "&employer=" + EMPLOYER + "&title=" + JOB_TITLE+"&provider=oakjob-app&email="+EMAIl;
		byte[] encoded = Base64.encodeBase64(redirectUrl.getBytes());
		return (new String(encoded));

	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		sort_by="relevance";
	}
	

public void saveJobsAtServer(int position,Job_ job)
	{
		
		list.get(position).setFav(true);
		jobsListAdapter.notifyDataSetChanged();
	    OakjobDb.getDBInstance(context).insertToSaveJob(job);
	    Toast.makeText(context, "Job Saved Successfully", Toast.LENGTH_LONG).show();
		
	}	

public void changeAlertIcon()
{
	Drawable img = getResources().getDrawable( R.drawable.alerts);
	img.setBounds( 0, 0, 45, 45 );
	butt_save_as_alert.setCompoundDrawables( null, img, null, null );
	butt_save_as_alert.setTextColor(Color.parseColor("#7CC243"));
	butt_save_as_alert.setText("Saved Alert");
}

public void openDialog()
{
		 AlertDialog alertDialog = new AlertDialog.Builder(this,R.style.DialogTheme).create();
	        	//.setTitle("Delete Save Job");
	        
		 alertDialog.setMessage(" No Internet Connection!");

			
		 alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Ok", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					SearchListActivity.this.finish();
				}
			});
	        
	     
	        alertDialog.show();
	       
}

public void loginAlertOnSearch(final Job_ job,final int position,String msg,final Context con)
{
	
	 AlertDialog alertDialog = new AlertDialog.Builder(con,R.style.DialogTheme).create();

	 alertDialog.setMessage(msg);
	 LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	 View viewMessEdit = inflater.inflate(R.layout.custom_checkbox,null);
	 alertDialog.setView(viewMessEdit);
	 final CheckBox check= (CheckBox)viewMessEdit.findViewById(R.id.customcheck);
	 check.setOnClickListener(new OnClickListener() {
		@Override
		public void onClick(View arg0) {
			
			if(check.isChecked()){
				check.setButtonDrawable(R.drawable.abc_btn_check_to_on_mtrl_015);
			}else{
				check.setButtonDrawable(R.drawable.abc_btn_check_to_on_mtrl_000);
			}
		}
	});
 alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Login", new DialogInterface.OnClickListener() {
	
		@Override
		public void onClick(DialogInterface dialog, int which) {
			SearchListActivity.this.finish();
			MainActivity.search_flag=2;
			
		}
	});
 
 alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Skip", new DialogInterface.OnClickListener() {
		
		@Override
		public void onClick(DialogInterface dialog, int which) {
			if(check.isChecked())
			{
				Preferences.getPreferenceInstance().loginAlertStatus(SearchListActivity.this,Preferences.ALERT_ACTIVE);
			}
			saveJobsAtServer(position,job);
			
		}
	});
 alertDialog.show();
}

public void saveAlertOnSearch(final Context con, int totalJobs)
{
	
	 AlertDialog alertDialog = new AlertDialog.Builder(con,R.style.DialogTheme).create();

	 alertDialog.setMessage("We found "+totalJobs+" '"+searchWhat+"' jobs within 30 miles of "+searchWhere);
	
	 
	 
	 LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	 View viewMessEdit = inflater.inflate(R.layout.custom_checkbox,null);
	 
	 alertDialog.setView(viewMessEdit);
	 final CheckBox check= (CheckBox)viewMessEdit.findViewById(R.id.customcheck);
	 check.setOnClickListener(new OnClickListener() {
		@Override
		public void onClick(View arg0) {
			
			if(check.isChecked()){
				check.setButtonDrawable(R.drawable.abc_btn_check_to_on_mtrl_015);
			}else{
				check.setButtonDrawable(R.drawable.abc_btn_check_to_on_mtrl_000);
			}
		}
	});
// final CheckBox check= new CheckBox(SearchListActivity.this,null,R.style.checkboxstyle);	 
	 
//	 if(android.os.Build.VERSION.SDK_INT < 21){
//	
//		 int id = Resources.getSystem().getIdentifier("check_box", "drawable" ,"android");
//		 check.setButtonDrawable(id);
//		 }
	// check.setText("Remember Choice");
	// check.setTextColor(Color.parseColor("#000000"));
	// alertDialog.setView(check);
	
 alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Create Alert", new DialogInterface.OnClickListener() {
	
		@Override
		public void onClick(DialogInterface dialog, int which) {
			if(check.isChecked())
			{
				Preferences.getPreferenceInstance().saveAlertStatus(SearchListActivity.this,"2");
			}
			if(!checkJobAlert())
			ServerSideDb.getSeverDbInstance().saveMyAlert(searchWhat, searchWhere, SearchListActivity.this);
						
		}
	});
 
 alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Skip", new DialogInterface.OnClickListener() {
		
		@Override
		public void onClick(DialogInterface dialog, int which) {
			if(check.isChecked())
			{
				Preferences.getPreferenceInstance().saveAlertStatus(SearchListActivity.this,Preferences.ALERT_ACTIVE);
			}
					
		}
	});
 alertDialog.show();

}

}
