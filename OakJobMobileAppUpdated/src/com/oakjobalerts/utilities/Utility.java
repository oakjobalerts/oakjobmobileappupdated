package com.oakjobalerts.utilities;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit.RestAdapter;
import retrofit.RestAdapter.LogLevel;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.Gravity;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.oakjobalerts.MainActivity;
import com.oakjobalerts.Model;
import com.oakjobalerts.R;
import com.oakjobalerts.SearchListActivity;
import com.oakjobalerts.fragments.LoginFragment;
import com.oakjobalerts.fragments.SearchFragment;
import com.oakjobalerts.preferences.Preferences;

public class Utility {

	public final static String TAG_LOGIN = "Login";
	public final static String TAG_SIGNUP = "Sign Up";
	public final static String TAG_DETAIL = "Detail";
	public final static String TAG_JOB_DETAIL = "Job Detail";
	public final static String TAG_SEARCH_LIST = "Results";
	public final static String TAG_SETTINGS = "Settings";
	public final static String TAG_RECENT_SEARCH = "Recent Search";
	public final static String TAG_MY_ALERTS = "My Alerts";
	public final static String TAG_SAVE_JOBS = "Saved Jobs";
	public final static String TAG_ABOUT = "About Us";
	public final static String TAG_HOME = "Oak Job Alerts";
	public final static String TAG_BROWSE = "Browse";
	
	public final static String TRACKING_ID = "UA-31800346-3";

	// public static ArrayList<String> f_backStack = new ArrayList<String>();

	// public static void replaceFragment(FragmentActivity fa, Fragment
	// fragment, String TAG,boolean addtoStack){

	// ReplaceFragment obj=new ReplaceFragment(fa, fragment);

	// FragmentManager fragmentManager = fa.getSupportFragmentManager();
	// FragmentManager.BackStackEntry backEntry=null;
	//
	// int count=fragmentManager.getBackStackEntryCount();
	// Log.e("t",""+count);
	//
	// if(count>0){
	// backEntry=fragmentManager.getBackStackEntryAt(count-1);
	// }
	//
	// if(count>0 && backEntry.getName().equals(TAG))
	// {
	// Log.e("t","if="+count);
	// }
	// else{
	//
	//
	// }

	// }

	public static void replaceFragment(FragmentActivity fa, Fragment fragment, String TAG, boolean addtoStack) {
		FragmentManager fragmentManager = fa.getSupportFragmentManager();

		int count = fragmentManager.getBackStackEntryCount();
		
		String name = "";
//		if (count > 0)
//			name = fragmentManager.getBackStackEntryAt(count - 1).getName().toString();
//		if (!name.contains(fragment.getClass().getName())) {

			FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
			fragmentTransaction.replace(R.id.frag_main, fragment);

			// if(addtoStack){
			fragmentTransaction.addToBackStack(fragment.getClass().getName());

			// }
			fragmentTransaction.commit();
		//}
	}

	//
	// public void checkFragment(FragmentManager fragmentManager,Fragment
	// fragment)
	// {
	// int count=fragmentManager.getBackStackEntryCount();
	//
	// }

	// public static void replaceFragmentWithStack(FragmentActivity fa, Fragment
	// fragment, String TAG){
	//
	// FragmentManager fragmentManager = fa.getSupportFragmentManager();
	// FragmentTransaction fragmentTransaction =
	// fragmentManager.beginTransaction();
	// fragmentTransaction.add(R.id.frag_main, fragment,TAG);
	// fragmentTransaction.addToBackStack(TAG);
	// fragmentTransaction.commit();
	// }

	// public static void backBtn(FragmentActivity fa){
	// FragmentManager fragmentManager = fa.getSupportFragmentManager();
	// fragmentManager.popBackStack();
	// }

	public static RestAdapter getAdapter() {
		//Preferences.getPreferenceInstance().getUpdatedata();
		RestAdapter adapter = new RestAdapter.Builder().setLogLevel(LogLevel.FULL).setEndpoint("http://s2.oakjobalerts.com:8080/OakJobMobileApi").build();
		return (adapter);
	}
	
	public static boolean isValidMobile(String phone) 
	{
	    boolean check=false;
	    try{
	    	 	long ph=Long.parseLong(phone);
	    	 	Log.e("a","not exception");
	    	check=true;
	    }
	    catch(NumberFormatException n)
	    {
	    	Log.e("a","exception");
	    	check=false;
	    }
	    return check;
		//return android.util.Patterns.PHONE.matcher(phone).matches();
	}
	public static boolean checkEmail(String email)
	{
		Log.e("email in utility=",email);
		String regex = "^[a-zA-Z0-9_!#$%&'*+/=?`{|}~^-]+(?:\\.[a-zA-Z0-9_!#$%&'*+/=?`{|}~^-]+)*@[a-zA-Z0-9-]+(?:\\.[a-zA-Z0-9-]+)*$";
		 
		Pattern pattern = Pattern.compile(regex);
		 
		    Matcher matcher = pattern.matcher(email);
		    System.out.println(email +" : "+ matcher.matches());
		  return(matcher.matches());
	}
	
	public static boolean isNetworkAvailable(Context con) {
	    ConnectivityManager connectivityManager 
	          = (ConnectivityManager) con.getSystemService(Context.CONNECTIVITY_SERVICE);
	    NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
	    
	    return activeNetworkInfo != null && activeNetworkInfo.isConnected();
	}
	
	public static void checkInternerConnection(Context context)
	{
		Toast.makeText(context, "Please Check Your Internet Connection", Toast.LENGTH_SHORT).show();
	}
	
	
	
	public static void loginAlert(String msg,final FragmentActivity con)
	{
		
		 AlertDialog alertDialog = new AlertDialog.Builder(con,R.style.DialogTheme).create();

		 alertDialog.setMessage(msg);

		
     alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Login", new DialogInterface.OnClickListener() {
		
			@Override
			public void onClick(DialogInterface dialog, int which) {
				Utility.replaceFragment(con, LoginFragment.getInstance(), Utility.TAG_LOGIN, false);
			}
		});
     
     alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Skip", new DialogInterface.OnClickListener() {
 		
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}
		});
     alertDialog.show();
	
	}
	public static void loginAlertOnSearch(String msg,final Context con)
	{
		
		 AlertDialog alertDialog = new AlertDialog.Builder(con,R.style.DialogTheme).create();

		 alertDialog.setMessage(msg);

		
     alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Login", new DialogInterface.OnClickListener() {
		
			@Override
			public void onClick(DialogInterface dialog, int which) {
			//	SearchListActivity.mSearchListActivity.finish();
				SearchListActivity.getInstance().finish();
				//Model.getInstance().loadLoginPage();
				
				
				MainActivity.search_flag=2;
//				con.finish();
			}
		});
     
     alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Skip", new DialogInterface.OnClickListener() {
 		
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}
		});
     alertDialog.show();
	
	}
	
	public static void NoInternet(final Activity con,final boolean finish)
	{
		String msg="  No Internet Connection!  ";
		 AlertDialog alertDialog = new AlertDialog.Builder(con,R.style.DialogTheme).create();

		 alertDialog.setMessage(msg);

		
		 alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Ok", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					if(finish)
					con.finish();
				}
			});
		 alertDialog.show();
	}

	
	public static Tracker initialiseTracker(Context context, String screenName,
            String key) {
        // myPref = Utility.this.getSharedPreferences("myPrefs", 0);
        // String gaIdKey=myPref.getString("gakey", null);
        GoogleAnalytics analytics = GoogleAnalytics.getInstance(context);
        Tracker t = analytics.newTracker(key);
        t.setScreenName(screenName);
        t.send(new HitBuilders.AppViewBuilder().build());
 
        return t;
    }
 
    public static void sendClickEvent(Tracker t, String category,
            String action, String label) {
 
        // Build and send an Event.
        t.send(new HitBuilders.EventBuilder().setCategory(category)
                .setAction(action).setLabel(label).build());
 
    }
	
}

