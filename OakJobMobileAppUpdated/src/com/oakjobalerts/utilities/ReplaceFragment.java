package com.oakjobalerts.utilities;

import com.oakjobalerts.R;
import com.oakjobalerts.oakinterface.OakFragmentInterface;

import android.support.v4.app.FragmentManager.OnBackStackChangedListener;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;

public class ReplaceFragment implements OnBackStackChangedListener{

	FragmentManager fragmentManager;
	Fragment fragment;
	FragmentActivity activity;
	FragmentTransaction tran;
	
	
	public ReplaceFragment(){
		
	}
	
	ReplaceFragment(FragmentActivity activity,Fragment fragment)
	{
		fragmentManager = activity.getSupportFragmentManager();
		fragmentManager.addOnBackStackChangedListener(this);
		this.activity=activity;
		this.fragment=fragment;
		tran=fragmentManager.beginTransaction();
	}
	
	@Override
	public void onBackStackChanged() {
		Log.e("l","listener changed");
		FragmentManager.BackStackEntry backEntry=null;
		int count=fragmentManager.getBackStackEntryCount();
		for(int i=0;i<count;i++)
		{
			backEntry=fragmentManager.getBackStackEntryAt(i);
			if((fragment.getClass().getName()).equals(backEntry))
					{
						tran.remove(fragment);
					}
		}
	}
	
	public  void replaceFragment(boolean isAddTobackstack)
	{
		
		Log.e("l","replace");
		
        
         tran.replace(R.id.frag_main, fragment);
         tran.addToBackStack(fragment.getClass().getName());
         tran.commit();
	}

}
