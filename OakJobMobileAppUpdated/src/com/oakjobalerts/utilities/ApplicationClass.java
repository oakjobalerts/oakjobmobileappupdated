package com.oakjobalerts.utilities;

import android.app.Application;

public class ApplicationClass extends Application
{

    @Override
    public void onCreate()
    {
        super.onCreate();

    }

    public static boolean isActivityVisible()
    {
        return activityVisible;
    }

    public static void activityResumed()
    {
        activityVisible = true;
    }

    public static void activityPaused()
    {
        activityVisible = false;
    }

    private static boolean activityVisible = false;

}
