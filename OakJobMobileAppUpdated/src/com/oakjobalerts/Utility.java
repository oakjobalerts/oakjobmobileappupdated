package com.oakjobalerts;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

public class Utility {
	
	public static void replaceFragment(FragmentActivity fa, Fragment fragment){
		
		 FragmentManager fragmentManager = fa.getSupportFragmentManager();
         FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
         fragmentTransaction.replace(R.id.frag_main, fragment);
         fragmentTransaction.commit();
	}
	
}
