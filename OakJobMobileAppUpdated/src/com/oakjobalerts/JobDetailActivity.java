package com.oakjobalerts;

import java.util.List;

import org.apache.commons.codec.binary.Base64;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.oakjobalerts.db.OakjobDb;
import com.oakjobalerts.db.ServerSideDb;
import com.oakjobalerts.modeldata.Job_;
import com.oakjobalerts.oakinterface.OakFragmentInterface;
import com.oakjobalerts.preferences.Preferences;
import com.oakjobalerts.utilities.Utility;

public class JobDetailActivity extends Activity {

	private com.google.android.gms.analytics.Tracker ta;
	TextView txtJobTitle = null;
	OakFragmentInterface mOakFragmentInterface;
	ImageView imBack = null;
	TextView txtMenu = null;
	ImageView imEdit = null;
	LinearLayout shareLayout = null;
	LinearLayout favLayout = null;
	Button apply_now_btn = null;
	String url = null;
	TextView tv_job_description = null;
	ImageView im_logout = null;
	ImageView im_search = null;
	Button favourite_btn = null;
	TextView tv_favourite;
	Job_ data=null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		try{ ta = Utility.initialiseTracker(this, "Job Detail View",Utility.TRACKING_ID); }
		catch(Exception e)	{	e.printStackTrace(); }
		
		setContentView(R.layout.layout_job_detail_activity);

		imBack = (ImageView) findViewById(R.id.im_back);
		txtMenu = (TextView) findViewById(R.id.txt_menu);
		tv_job_description = (TextView) findViewById(R.id.tv_job_description);

		im_search = (ImageView) findViewById(R.id.im_search);
		im_logout = (ImageView) findViewById(R.id.im_logout);
		imEdit = (ImageView) findViewById(R.id.im_edit);
		shareLayout = (LinearLayout) findViewById(R.id.ll_share);
		favLayout = (LinearLayout) findViewById(R.id.ll_favorite);
		apply_now_btn = (Button) findViewById(R.id.apply_now_btn);
		favourite_btn = (Button) findViewById(R.id.favourite_btn);
		tv_favourite = (TextView) findViewById(R.id.tv_favourite);
		
		
		
		Intent intent = getIntent();
		data=(Job_)intent.getSerializableExtra("bundle");
		
			String JOB_TITLE = data.getTitle();
			String EMPLOYER = data.getEmployer();
			String DESCRIPTION = data.getDescription();
		if (DESCRIPTION.equals("")) {
			tv_job_description.setText("No Description Found");
		} else {
			tv_job_description.setText(Html.fromHtml(DESCRIPTION));
		}
	
		if (isFavIconActive(JOB_TITLE + "####" + EMPLOYER)) {
			favourite_btn.setBackgroundResource(R.drawable.heart);
			tv_favourite.setTextColor(Color.parseColor("#7CC243"));
		}

		apply_now_btn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) { // TODO Auto-generated method stub

				String jobId = data.getId();
				url = "http://www.oakjobalerts.com/RedirectWEB.php?q=" + encodeUrl(data) + "&token=" + jobId;
				Log.e("URL=", url);
				Intent i = new Intent(JobDetailActivity.this, ApplyJob.class);
				i.putExtra("url", url);
				startActivity(i);
//				Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
//				startActivity(i);
			}
		});
		imBack.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				finish();
			}
		});

		favLayout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				String JOB_TITLE = data.getTitle();
				String EMPLOYER = data.getEmployer();
				
				if (isFavIconActive(JOB_TITLE + "####" + EMPLOYER)) {
					Toast.makeText(JobDetailActivity.this, "Job already Saved", 0).show();
				}
				else if(!MainActivity.isLogin(JobDetailActivity.this))
				{
					if(!Preferences.getPreferenceInstance().getLoginAlertStatus(JobDetailActivity.this))
					loginAlertOnSearch(data,0,"Job will save locally, please login to get benefit of saved jobs on other devices. ", JobDetailActivity.this);
					else
					{
						OakjobDb.getDBInstance(JobDetailActivity.this).insertToSaveJob(data);
						favourite_btn.setBackgroundResource(R.drawable.heart);
						tv_favourite.setTextColor(Color.parseColor("#7CC243"));
					}
				}
				else {
				
					favourite_btn.setBackgroundResource(R.drawable.heart);
					tv_favourite.setTextColor(Color.parseColor("#7CC243"));
				ServerSideDb.getSeverDbInstance().saveFavJob(data,JobDetailActivity.this);
					//OakjobDb.getDBInstance(JobDetailActivity.this).insertToSaveJob(data);//JOB_ID, JOB_TITLE, JOB_SOURCE, JOB_URL, SOURCE_NAME, STATE, EMPLOYER, POSTING_DATE, CITY,DESCRIPTION,DISPLAYSOURCE);
					//Toast.makeText(JobDetailActivity.this, "Job Successfully saved", 0).show();
				}
			}
		});

		im_logout.setVisibility(View.GONE);
		imEdit.setVisibility(View.GONE);
		im_search.setVisibility(View.GONE);
		txtMenu.setText("Detail");

		txtJobTitle = (TextView) findViewById(R.id.tv_job_title);

		shareLayout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String url="http://www.oakjobalerts.com/RedirectWEB.php?q="+encodeUrl(data)+"&token="+data.getJobId();
				shareurl(url, data.getTitle(), data.getEmployer());
//				Intent shareIntent = new Intent(Intent.ACTION_SEND);
//				String shareUrl="Check out this job on oakJob Alerts,<br><br>I thought you might be interested in this job offer on OakJobAlerts.<br><br><a href="+ data.getJoburl()+">"+data.getTitle()+"<br>"+data.getEmployer()+"</a>";
//				shareIntent.setType("text/html");
//				shareIntent.putExtra(shareIntent.EXTRA_TEXT,Html.fromHtml(shareUrl));
//				startActivity(Intent.createChooser(shareIntent, "Share with"));
			}
		});
	}

	public boolean isFavIconActive(String jobId) {
		List<String> saveJob = OakjobDb.getDBInstance(this).getSavedJobId();
		for (String a : saveJob) {
			Log.e("positionDb", a);
		}
		Log.e("position", jobId);
		if (saveJob.contains(jobId)) {
			return true;
		}
		return false;
	}

	void shareurl(String url,final String title, final String employer)
	{
		 if(Utility.isNetworkAvailable(this))
		 {
		 RestAdapter adapter=Utility.getAdapter();
		 AwsCloudSearch inter=adapter.create(AwsCloudSearch.class);
		 inter.javaAwsCloudSearchAPIredirecturl(url, new Callback<String>() {

			@Override
			public void failure(RetrofitError arg0) {
				
				
			}

			@Override
			public void success(String arg0, Response arg1) {
				
				 Intent shareIntent= new Intent(Intent.ACTION_SEND);
				// String shareUrl="Check out this job on Oak Job Alerts,<br><br>I thought you might be interested in this job offer on Oak Job Alerts.<br><br><a href="+ url+"> Sales Job</a>";
				 String shareUrl="Check out this job on Oak Job Alerts,\n\n"
				 		+ title.replace("&amp;", "&")+" - "+employer+"\n"
				 		+ "http://oakjobalerts.com/Redirect.php?id="+arg0;
				  shareIntent.putExtra(android.content.Intent.EXTRA_TEXT,shareUrl);
					shareIntent.setType("text/plain");
					startActivity(Intent.createChooser(shareIntent, "Share with"));
					
			}
			 
		});
		 
		 }
		 else
		 {
			 Utility.NoInternet(this,false);
		 }
	}
	
	public String encodeUrl(Job_ job) {
		String JOB_SOURCE = job.getSource();
		String JOB_URL =job.getJoburl();
		String SOURCE_NAME = job.getSourcename();
		String STATE = job.getState();
		String CITY = job.getCity();
		String JOB_TITLE = job.getTitle();
		String EMPLOYER = job.getEmployer();
		String EMAIl=Preferences.getPreferenceInstance().getEmail(this);
		
		JOB_URL=JOB_URL.replace("&", "##");
		//https://www.ziprecruiter.com/clk/ZKVyubFim_jMoUgb3zUrU0FKdZIA6Dlf6-xWo3gXDM_ShEDt0B_gtcet7GZofVZjqT-Ytusmam6fYeWsLE0JwDXJYqOgQPLDlIxSQMd-qbIxx2tibf2mw7RgcuP5WlSlUnlMmhma-kidcpZhffw770GMq5OeSmpciHsxjUcFAZfiaBNKloIyg8_krq8aAw7qy222VdtBeZeKq1Jy0a7Ym9q5GD8aa9fY-_JbBQYXW8jRvF8Mcp9bOaCpNUNIiiVlhWDRUmi40gqRP5WiQoSvJro31vOpjmMxUDfCiPyQyMbrVClzAaQTBJg6t8k9YAC7ab52v7PWn_7nCJGs9UQj8TjbCVHNQ7x_KXazG-5ClzlyiYAiOnzv_I59-xAcFTCeOYQvshiqqRJMKcXAYe8L8w.511fa34e07d137e48ddda091118ab9cc
		
		String redirectUrl = "url="+JOB_URL+"&sourcename=" + SOURCE_NAME + "&source=" + JOB_SOURCE + "&city=" + CITY + "&state=" + STATE + "&employer=" + EMPLOYER + "&title=" + JOB_TITLE+"&provider=oakjob-app&email="+EMAIl;
		byte[] encoded = Base64.encodeBase64(redirectUrl.getBytes());
		return (new String(encoded));

	}

	
	
	public void loginAlertOnSearch(final Job_ job,final int position,String msg,final Context con)
	{
		
		 AlertDialog alertDialog = new AlertDialog.Builder(con,R.style.DialogTheme).create();

		 alertDialog.setMessage(msg);
		 final CheckBox check=new CheckBox(JobDetailActivity.this);
		 check.setText("Remember Choice");
		 check.setTextColor(Color.parseColor("#000000"));
		 alertDialog.setView(check);
		
	 alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Login", new DialogInterface.OnClickListener() {
		
			@Override
			public void onClick(DialogInterface dialog, int which) {
				JobDetailActivity.this.finish();
				MainActivity.search_flag=2;
				
			}
		});
	 
	 alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Skip", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				if(check.isChecked())
				{
					Preferences.getPreferenceInstance().loginAlertStatus(JobDetailActivity.this,Preferences.ALERT_ACTIVE);
				}
				OakjobDb.getDBInstance(con).insertToSaveJob(job);
				favourite_btn.setBackgroundResource(R.drawable.heart);
				tv_favourite.setTextColor(Color.parseColor("#7CC243"));
				
				
				//dialog.dismiss();
			}
		});
	 alertDialog.show();

	}
	public void setDrawable()
	{
		Drawable img = getResources().getDrawable( R.drawable.active_shot);
		img.setBounds( 0, 0, 61, 38 );
		//butt_sort_by.setCompoundDrawables( null, img, null, null );

		img = getResources().getDrawable( R.drawable.active_alert);
		img.setBounds( 0, 0, 48, 48 );
	//	butt_save_as_alert.setCompoundDrawables( null, img, null, null );
	
	}
}
