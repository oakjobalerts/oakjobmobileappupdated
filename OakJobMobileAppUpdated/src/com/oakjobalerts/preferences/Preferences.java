package com.oakjobalerts.preferences;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.widget.Toast;

import com.oakjobalerts.modeldata.UpdateVersion;
import com.oakjobalerts.modeldata.UserRegister;

public class Preferences {

	static SharedPreferences prefrences;
	static Editor editor;

	private static Preferences INSTANCE = null;

	public static final String SHARED_PREFERENCES = "userdetails";
	public static final String ALERTSTATUS = "alertStatus";
	public static final String SAVEALERTFLAG= "saveAlertFlag";
	public static final String FIRSTNAME = "firstName";
	public static final String STATUS = "status";
	public static final String EMAIL = "email";
	public static final String LASTNAME = "lastName";
	public static final String PHONENO = "phonNo";
	public static final String ZIPCODE = "zipcode";
	public static final String PASSWORD = "password";
	public static final String DEVICEID = "deviceId";
	public static final String DEVICETOKEN = "deviceToken";
	public static final String PLATFORM = "platform";
	public static final String USERREGISTERID = "userRegisterId";
	public static final String ISALERTACTIVE = "isAlertActive";
	public static final String FREQUENCY = "frequency";
	

	public static final String ALERT_ACTIVE = "1";
	public static final String ALERT_NOT_ACTIVE = "0";

	public UserRegister getUserData(Context context) {
		
		SharedPreferences pref = getPrefrences(context);
		UserRegister user = new UserRegister();
		
		user.setuserRegisterId(Long.parseLong(pref.getString(USERREGISTERID, "0")));
		
		user.setFirstName(pref.getString(FIRSTNAME, ""));
		user.setLastName(pref.getString(LASTNAME, ""));
		user.setUserEmail(pref.getString(EMAIL, ""));
		user.setPass(pref.getString(PASSWORD, ""));
		user.setPhone(pref.getString(PHONENO, ""));
		user.setZipcode(pref.getString(ZIPCODE, ""));
		user.setDeviceId(pref.getString(DEVICEID, ""));
		user.setDeviceToken(pref.getString(DEVICETOKEN, ""));
		user.setPlatform(pref.getString(PLATFORM, ""));
		user.setAlertActive(pref.getString(ISALERTACTIVE, ""));
		return user;
	}

	public boolean getLoginAlertStatus(Context context) {
		SharedPreferences prefs = getPrefrences(context);
		String status = prefs.getString(ALERTSTATUS, "");
		if (status.equals("1"))
			return true;
		return false;
	}
	public String getSaveAlertStatus(Context context) {
		SharedPreferences prefs = getPrefrences(context);
		return  prefs.getString(SAVEALERTFLAG, "");
	}
	public void saveAlertStatus(Context context, String status) {
		SharedPreferences pref = getPrefrences(context);
		editor = pref.edit();
		editor.putString(SAVEALERTFLAG, status);
		editor.commit();
	}
	public void loginAlertStatus(Context context, String status) {
		SharedPreferences pref = getPrefrences(context);
		editor = pref.edit();
		editor.putString(ALERTSTATUS, status);
		editor.commit();
	}

	public void loginSession(Context context, UserRegister user) {
		SharedPreferences pref = getPrefrences(context);
		editor = pref.edit();
		editor.putString(STATUS, "true");
		editor.putString(USERREGISTERID, String.valueOf(user.getuserRegisterId()));
		editor.putString(FIRSTNAME, user.getFirstName());
		
		editor.putString(LASTNAME, user.getLastName());
		editor.putString(EMAIL, user.getUserEmail());
		if(user.getPass()!=null)
		editor.putString(PASSWORD, user.getPass());
		editor.putString(PHONENO, user.getPhone());
		editor.putString(ZIPCODE, user.getZipcode());
		if(user.getPlatform()!=null)
		editor.putString(PLATFORM, user.getPlatform());
		editor.putString(ALERTSTATUS, ALERT_NOT_ACTIVE);
		editor.putString(SAVEALERTFLAG, ALERT_NOT_ACTIVE);
		editor.putString(ISALERTACTIVE, String.valueOf(user.getAlertActive()));
		editor.putString(FREQUENCY, String.valueOf(user.getFrequency()));
		editor.commit();
	}

	public String getUserName(Context context) {
		SharedPreferences prefs = getPrefrences(context);
		String userName = prefs.getString(FIRSTNAME, "") + " " + prefs.getString(LASTNAME, "");

		return userName;
	}
	
	public List<String> getAlerts(Context context) {
		List<String> alertList=new ArrayList();
		SharedPreferences prefs = getPrefrences(context);
		alertList.add(prefs.getString(ISALERTACTIVE, ""));
		alertList.add(prefs.getString(FREQUENCY, ""));

		return alertList;
	}
	
	public void setAlerts(Context context,String alert,String frequency) {
		SharedPreferences pref = getPrefrences(context);
		editor = pref.edit();
		editor.putString(ISALERTACTIVE, String.valueOf(alert));
		editor.putString(FREQUENCY, String.valueOf(frequency));
		editor.commit();
	}

	public void logoutSession(Context context) {
		if (islogin(context)) {
			SharedPreferences pref = getPrefrences(context);
			editor = pref.edit();
			editor.putString(STATUS, "false");
			editor.putString(USERREGISTERID,"0");
//			editor.putString(FIRSTNAME, "");
			editor.remove(FIRSTNAME);
//			editor.putString(LASTNAME, "");
			editor.remove(LASTNAME);
			editor.putString(EMAIL, "");
			editor.putString(PASSWORD, "");
			editor.putString(PHONENO, "");
			editor.putString(ZIPCODE, "");
			editor.putString(SAVEALERTFLAG, "0");
			editor.commit();
			Toast.makeText(context, "Logout Successfully", Toast.LENGTH_LONG).show();
		}
	}

	public String getEmail(Context context) {
		SharedPreferences prefs = getPrefrences(context);
		String email = prefs.getString(EMAIL, "");

		return email;
	}

	public boolean islogin(Context context) {
		SharedPreferences prefs = getPrefrences(context);
		String value = prefs.getString(EMAIL, "");
		if (!value.equals("")) {
			return true;
		}
		return false;
	}

	public static SharedPreferences getPrefrences(Context context) {

		prefrences = context.getSharedPreferences(SHARED_PREFERENCES, Context.MODE_PRIVATE);

		return prefrences;
	}

	public static Preferences getPreferenceInstance() {
		if (INSTANCE == null) {
			INSTANCE = new Preferences();
		}
		return (INSTANCE);
	}

	private Preferences() {

	}

	public void setDevicedata(FragmentActivity context, String device_id, String device_token) {
		SharedPreferences pref = getPrefrences(context);
		editor = pref.edit();
		editor.putString(DEVICEID, device_id);
		editor.putString(DEVICETOKEN, device_token);
		editor.commit();
	}

	public void setDevicedata(Context context, String device_id, String device_token) {
		SharedPreferences pref = getPrefrences(context);
		editor = pref.edit();
		editor.putString(DEVICEID, device_id);
		editor.putString(DEVICETOKEN, device_token);
		editor.commit();
	}
	
	public List<String> getDevicedata(Context context) {
		ArrayList<String> list = new ArrayList<String>();
		SharedPreferences pref = getPrefrences(context);
		list.add(pref.getString(DEVICEID, ""));
		list.add(pref.getString(DEVICETOKEN, ""));
		return list;
	}

	public static String UPADTEVERSION="updateVersion";
	public static String UPDATEDATA="updatedata";
	public void versionUpdate(UpdateVersion data, Context context)
	{
		SharedPreferences pref = getPrefrences(context);
		editor = pref.edit();
		editor.putString(UPADTEVERSION, String.valueOf(data.getVersion()));
		editor.putString(UPDATEDATA, data.toString());
		editor.commit();
	}
	public String getUpdatedata(Context context)
	{
		SharedPreferences prefs = getPrefrences(context);
		return prefs.getString(UPDATEDATA, "");
		
	}
	public String getUpdateVersion(Context context)
	{
		SharedPreferences prefs = getPrefrences(context);
		return prefs.getString(UPADTEVERSION, "");
	}
	
	
}
