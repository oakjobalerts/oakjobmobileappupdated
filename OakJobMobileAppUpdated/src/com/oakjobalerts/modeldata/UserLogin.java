package com.oakjobalerts.modeldata;



public class UserLogin {


	private String userEmail;
	private String pass;
	private String deviceId;
	private String deviceToken;
	private String platform;
	private boolean isActive;

	public String getUserEmail() {
		return userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public String getDeviceToken() {
		return deviceToken;
	}

	public void setDeviceToken(String deviceToken) {
		this.deviceToken = deviceToken;
	}

	public String getPlatform() {
		return platform;
	}

	public void setPlatform(String platform) {
		this.platform = platform;
	}

	public UserLogin(String userEmail, String pass, String deviceId,
			String deviceToken, String platform, boolean isActive) {
		super();
		this.userEmail = userEmail;
		this.pass = pass;
		this.deviceId = deviceId;
		this.deviceToken = deviceToken;
		this.platform = platform;
		this.isActive = isActive;
	}

	public boolean isActive() {
		return isActive;
	}

	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}

	public UserLogin() {
		super();
	}

}
