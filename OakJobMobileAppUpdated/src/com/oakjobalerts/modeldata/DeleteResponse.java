package com.oakjobalerts.modeldata;

public class DeleteResponse {

	
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public boolean isStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	
	
	
	private String message="";
	private boolean status=false;
	
	
}
