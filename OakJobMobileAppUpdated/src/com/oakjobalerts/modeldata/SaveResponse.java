package com.oakjobalerts.modeldata;

import com.google.gson.annotations.SerializedName;

public class SaveResponse {


			private boolean success=false;
		    private String message="";
		    private Job_ response;
		    
		    public SaveResponse(boolean success, String message, Job_ response) {
		        super();
		        this.success = success;
		        this.message = message;
		        this.response=response;
		    }

		    public SaveResponse() {
		        super();
		    }

		    public Job_ getResponse() {
				return response;
			}

			public void setResponse(Job_ response) {
				this.response = response;
			}

			public boolean isSuccess() {
		        return success;
		    }

		    public void setSuccess(boolean success) {
		        this.success = success;
		    }

		    public String getMessage() {
		        return message;
		    }

		    public void setMessage(String message) {
		        this.message = message;
		    }

		}

