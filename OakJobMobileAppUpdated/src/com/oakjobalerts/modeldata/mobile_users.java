package com.oakjobalerts.modeldata;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class mobile_users {

	@SerializedName("date")
	@Expose
	private String date;
	
	@SerializedName("email_id")
	@Expose
	private String email_id;
	
	@SerializedName("password")
	@Expose
	private String password;
	
	@SerializedName("device_id")
	@Expose
	private String device_id;
	
	@SerializedName("device_token")
	@Expose
	private String device_token;
	
	@SerializedName("platform")
	@Expose
	private String platform;
	
	@SerializedName("first_name")
	@Expose
	private String first_name;
	
	@SerializedName("phone_no")
	@Expose
	private String phone_no;
	
	@SerializedName("zipcode")
	@Expose
	private String zipcode;
	
	@SerializedName("is_alert_active")
	@Expose
	private boolean is_alert_active;
	
	
	public String getDate() {
		return date;
	}
	
	public void setDate(String date) {
		this.date = date;
	}
	
	public String getEmail_id() {
		return email_id;
	}
	
	public void setEmail_id(String email_id) {
		this.email_id = email_id;
	}
	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getDevice_id() {
		return device_id;
	}
	
	public void setDevice_id(String device_id) {
		this.device_id = device_id;
	}
	
	public String getDevice_token() {
		return device_token;
	}
	public void setDevice_token(String device_token) {
		this.device_token = device_token;
	}
	
	public String getPlatform() {
		return platform;
	}
	
	public void setPlatform(String platform) {
		this.platform = platform;
	}
	
	public String getFirst_name() {
		return first_name;
	}
	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}
	
	public String getPhone_no() {
		return phone_no;
	}
	
	public void setPhone_no(String phone_no) {
		this.phone_no = phone_no;
	}
	public String getZipcode() {
		return zipcode;
	}
	
	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}
	
	public boolean isIs_alert_active() {
		return is_alert_active;
	}
	
	public void setIs_alert_active(boolean is_alert_active) {
		this.is_alert_active = is_alert_active;
	}
	

}
