package com.oakjobalerts.modeldata;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SignInResponse {

@SerializedName("success")
@Expose
private Boolean success;
@SerializedName("message")
@Expose
private String message;
@SerializedName("response")
@Expose
private UserRegister response;

/**
* No args constructor for use in serialization
*
*/
public SignInResponse() {
}

/**
*
* @param response
* @param message
* @param success
*/
public SignInResponse(Boolean success, String message, UserRegister response) {
this.success = success;
this.message = message;
this.response = response;
}

/**
*
* @return
* The success
*/
public Boolean getSuccess() {
return success;
}

/**
*
* @param success
* The success
*/
public void setSuccess(Boolean success) {
this.success = success;
}

/**
*
* @return
* The message
*/
public String getMessage() {
return message;
}

/**
*
* @param message
* The message
*/
public void setMessage(String message) {
this.message = message;
}

/**
*
* @return
* The response
*/
public UserRegister getResponse() {
return response;
}

/**
*
* @param response
* The response
*/
public void setResponse(UserRegister response) {
this.response = response;
}

}