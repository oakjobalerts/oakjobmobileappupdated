package com.oakjobalerts.modeldata;


import java.util.List;

public class SaveJobResponse {

		private boolean success=false;
	    private String message="";
	    private List<Job_> response;
	    
	    public SaveJobResponse(boolean success, String message, List<Job_> response) {
	        super();
	        this.success = success;
	        this.message = message;
	        this.response=response;
	    }

	    public SaveJobResponse() {
	        super();
	    }

	    public List<Job_> getResponse() {
			return response;
		}

		public void setResponse(List<Job_> response) {
			this.response = response;
		}

		public boolean isSuccess() {
	        return success;
	    }

	    public void setSuccess(boolean success) {
	        this.success = success;
	    }

	    public String getMessage() {
	        return message;
	    }

	    public void setMessage(String message) {
	        this.message = message;
	    }

	}

