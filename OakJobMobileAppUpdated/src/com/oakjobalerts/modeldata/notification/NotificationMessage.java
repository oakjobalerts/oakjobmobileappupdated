package com.oakjobalerts.modeldata.notification;

public class NotificationMessage {

	boolean isBroadcast=false;
	BroadCastMessage broadcastMessage=null;
	NotificationData notification=null;
	
	
	public boolean isBroadcast() {
		return isBroadcast;
	}
	public void setBroadcast(boolean isBroadcast) {
		this.isBroadcast = isBroadcast;
	}
	public BroadCastMessage getBroadcastMessage() {
		return broadcastMessage;
	}
	public void setBroadcastMessage(BroadCastMessage broadcastMessage) {
		this.broadcastMessage = broadcastMessage;
	}
		
	public NotificationData getNotification() {
		return notification;
	}
	public void setNotification(NotificationData notification) {
		this.notification = notification;
	}
	
	
}
