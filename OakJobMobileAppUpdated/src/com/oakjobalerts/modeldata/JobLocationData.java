package com.oakjobalerts.modeldata;

public class JobLocationData {
	public int getAlertId() {
		return alertId;
	}
	public void setAlertId(int alertId) {
		this.alertId = alertId;
	}
	public JobLocationData(int alertid,String keyword, String stateCity)
	{
		this.alertId=alertid;
		this.keyword=keyword;
		this.stateCity=stateCity;
	}
	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public String getStateCity() {
		return stateCity;
	}

	public void setStateCity(String stateCity) {
		this.stateCity = stateCity;
	}

	String keyword,stateCity;
	int alertId=0;
}
