package com.oakjobalerts.modeldata;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserRegister {

public String getFrequency() {
		return frequency;
	}

	public void setFrequency(String frequency) {
		this.frequency = frequency;
	}

public String getAlertActive() {
		return alertActive;
	}

	public void setAlertActive(String alertActive) {
		this.alertActive = alertActive;
	}


private String frequency="";
	
private long userRegisterId=0;
@SerializedName("firstName")
@Expose
private String firstName="";
@SerializedName("lastName")
@Expose
private String lastName="";
@SerializedName("userEmail")
@Expose
private String userEmail="";
@SerializedName("pass")
@Expose
private String pass="";
@SerializedName("phone")
@Expose
private String phone="";
@SerializedName("zipcode")
@Expose
private String zipcode="";
@SerializedName("deviceId")
@Expose
private String deviceId="";
@SerializedName("deviceToken")
@Expose
private String deviceToken="";
@SerializedName("platform")
@Expose
private String platform="";
@SerializedName("alertActive")
@Expose
private String alertActive;

/**
* No args constructor for use in serialization
*
*/
public UserRegister() {
}

/**
*
* @param platform
* @param lastName
* @param phone
* @param alertActive
* @param deviceToken
* @param zipcode
* @param userEmail
* @param firstName
* @param deviceId
* @param pass
*/
public UserRegister(String firstName, String lastName, String userEmail, String pass, String phone, String zipcode, String deviceId, String deviceToken, String platform, String alertActive,String frequency) {
this.firstName = firstName;
this.lastName = lastName;
this.userEmail = userEmail;
this.pass = pass;
this.phone = phone;
this.zipcode = zipcode;
this.deviceId = deviceId;
this.deviceToken = deviceToken;
this.platform = platform;
this.alertActive = alertActive;
this.frequency=frequency;
}


public long getuserRegisterId() {
	return userRegisterId;
}
public void setuserRegisterId(long userRegisterId) {
	this.userRegisterId = userRegisterId;
}

/**
*
* @return
* The firstName
*/
public String getFirstName() {
return firstName;
}

/**
*
* @param firstName
* The firstName
*/
public void setFirstName(String firstName) {
this.firstName = firstName;
}

/**
*
* @return
* The lastName
*/
public String getLastName() {
return lastName;
}

/**
*
* @param lastName
* The lastName
*/
public void setLastName(String lastName) {
this.lastName = lastName;
}

/**
*
* @return
* The userEmail
*/
public String getUserEmail() {
return userEmail;
}

/**
*
* @param userEmail
* The userEmail
*/
public void setUserEmail(String userEmail) {
this.userEmail = userEmail;
}

/**
*
* @return
* The pass
*/
public String getPass() {
return pass;
}

/**
*
* @param pass
* The pass
*/
public void setPass(String pass) {
this.pass = pass;
}

/**
*
* @return
* The phone
*/
public String getPhone() {
return phone;
}

/**
*
* @param phone
* The phone
*/
public void setPhone(String phone) {
this.phone = phone;
}

/**
*
* @return
* The zipcode
*/
public String getZipcode() {
return zipcode;
}

/**
*
* @param zipcode
* The zipcode
*/
public void setZipcode(String zipcode) {
this.zipcode = zipcode;
}

/**
*
* @return
* The deviceId
*/
public String getDeviceId() {
return deviceId;
}

/**
*
* @param deviceId
* The deviceId
*/
public void setDeviceId(String deviceId) {
this.deviceId = deviceId;
}

/**
*
* @return
* The deviceToken
*/
public String getDeviceToken() {
return deviceToken;
}

/**
*
* @param deviceToken
* The deviceToken
*/
public void setDeviceToken(String deviceToken) {
this.deviceToken = deviceToken;
}

/**
*
* @return
* The platform
*/
public String getPlatform() {
return platform;
}

/**
*
* @param platform
* The platform
*/
public void setPlatform(String platform) {
this.platform = platform;
}




}
