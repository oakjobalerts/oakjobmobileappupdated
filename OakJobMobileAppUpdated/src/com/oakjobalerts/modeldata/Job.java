package com.oakjobalerts.modeldata;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Job {
    private List<Job_> jobs=new ArrayList();
    private String end_point;
    private int total_found;
    private String reading_from;

    public List<Job_> getJobs() {
        return jobs;
    }

    public void setJobs(List<Job_> jobs) {
        this.jobs = jobs;
    }

    public String getEnd_point() {
        return end_point;
    }

    public void setEnd_point(String end_point) {
        this.end_point = end_point;
    }

    public int getTotal_found() {
        return total_found;
    }

    public void setTotal_found(int i) {
        this.total_found = i;
    }
    

    public String getReading_from() {
        return reading_from;
    }

    public void setReading_from(String reading_from) {
        this.reading_from = reading_from;
    }

  

    }