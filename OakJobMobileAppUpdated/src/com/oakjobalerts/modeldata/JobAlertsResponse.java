package com.oakjobalerts.modeldata;

import java.util.List;

public class JobAlertsResponse {

	private String message="";
	private boolean status=false;
	private List<JobAlerts> alert;
	
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public boolean isStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	public List<JobAlerts> getAlert() {
		return alert;
	}
	public void setAlert(List<JobAlerts> alert) {
		this.alert = alert;
	}
	
	
}
