package com.oakjobalerts.modeldata;

import java.util.List;

public class UpdateVersion {

	public int getVersion() {
		return version;
	}
	public void setVersion(int version) {
		this.version = version;
	}
	public String getBaseUrl() {
		return baseUrl;
	}
	public void setBaseUrl(String baseUrl) {
		this.baseUrl = baseUrl;
	}
	public String[] getAlertList() {
		return alertList;
	}
	public void setAlertList(String[] alertList) {
		this.alertList = alertList;
	}
	public String[] getFrequencyList() {
		return frequencyList;
	}
	public void setFrequencyList(String[] frequencyList) {
		this.frequencyList = frequencyList;
	}
	int version=0;
	String baseUrl="";
	String alertList[]={"No Alert","Email and Notification","Notification"};
	String frequencyList[]={"Daily","Twice a week","Weekly","Monthly"};
	
}
