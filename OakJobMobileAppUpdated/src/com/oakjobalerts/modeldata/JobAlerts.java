package com.oakjobalerts.modeldata;

public class JobAlerts {

	public String getDeviceToken() {
		return deviceToken;
	}
	public void setDeviceToken(String deviceToken) {
		this.deviceToken = deviceToken;
	}
	public String getDeviceId() {
		return deviceId;
	}
	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}
	public String getPlatform() {
		return platform;
	}
	public void setPlatform(String platform) {
		this.platform = platform;
	}
		long id=0;
		String keyword="";
		String radius="";
		String emailAddress="";
		String location="";
		String zip="";
		String dateTime="";
		String provider="";
		String frequency="";
		String deviceToken="";
		String deviceId="";
		String platform="";
		

		public long getId() {
			return id;
		}
		public void setId(long id) {
			this.id = id;
		}
		public String getKeyword() {
			return keyword;
		}
		public void setKeyword(String keyword) {
			this.keyword = keyword;
		}
		public String getRadius() {
			return radius;
		}
		public void setRadius(String radius) {
			this.radius = radius;
		}
		public String getEmailAdrress() {
			return emailAddress;
		}
		public void setEmailAdrress(String emailAddress) {
			this.emailAddress = emailAddress;
		}
		public String getLocation() {
			return location;
		}
		public void setLocation(String location) {
			this.location = location;
		}
		public String getZip() {
			return zip;
		}
		public void setZip(String zip) {
			this.zip = zip;
		}
		public String getDateTime() {
			return dateTime;
		}
		public void setDateTime(String dateTime) {
			this.dateTime = dateTime;
		}
		public String getProvider() {
			return provider;
		}
		public void setProvider(String provider) {
			this.provider = provider;
		}
		public String getFrequency() {
			return frequency;
		}
		public void setFrequency(String frequency) {
			this.frequency = frequency;
		}
		
	}

	