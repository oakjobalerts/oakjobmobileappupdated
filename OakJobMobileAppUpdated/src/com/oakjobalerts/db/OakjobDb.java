package com.oakjobalerts.db;

import java.util.ArrayList;
import java.util.List;

import com.oakjobalerts.modeldata.JobLocationData;
import com.oakjobalerts.modeldata.Job_;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class OakjobDb extends SQLiteOpenHelper  {


	public static final String DATABASE_NAME = "OakDb.db";
	private static final int DATABASE_VERSION = 1;
	public static final String SAVE_JOBS = "SaveJobs";
	public static final String RECENT_JOBS = "RecentJobs";
	public static final String JOB_ALERTS = "JobAlerts";
	public static OakjobDb INSTANCE = null;
	
	// for SaveJobs table
	public static final String JOB_ID = "jobId";
	public static final String JOB_TITLE = "jobTitle";
	public static final String JOB_SOURCE = "jobSource";
	public static final String JOB_URL = "jobUrl";
	public static final String SOURCE_NAME = "sourcename";
	public static final String STATE = "state";
	public static final String EMPLOYER = "employer";
	public static final String DISPLAYSOURCE = "displaySource";
	public static final String POSTING_DATE = "postingDate";
	public static final String CITY = "city";
	public static final String DESCRIPTION = "description";
	
	// for RecentJobs and JobAlerts table
	public static final String ID = "id";
	public static final String ALERTID = "alertId";
	public static final String KEYWORD = "keyword";
	public static final String LOCATION = "location";
	
	//creating queries
	public static final String createSaveJobs = "create table "+ SAVE_JOBS+"("+ID+" integer primary key,"+JOB_ID+" integer,"+
				 JOB_TITLE+" text,"+ JOB_SOURCE+" text,"+ JOB_URL+" text,"+ SOURCE_NAME+" text,"+
				 STATE+" text,"+ EMPLOYER+" text,"+ POSTING_DATE+" text,"+ CITY+" text,"+DESCRIPTION+" text,"+DISPLAYSOURCE+" text)";
	
	public static final String createRecentJobs = "CREATE TABLE "+ RECENT_JOBS+" ("+ID+" integer primary key ,"+KEYWORD+" text,"+LOCATION+" text)";

	public static final String createJobAlerts = "CREATE TABLE "+ JOB_ALERTS+" ("+ID+" integer primary key , "+ALERTID+" integer , "+KEYWORD+" text, "+LOCATION+" text)";
	
	// Constructor definition
	private OakjobDb(Context con)
	{
		super(con, DATABASE_NAME , null , DATABASE_VERSION); 
	}
	
	// methods Definition
	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(createSaveJobs);
		db.execSQL(createRecentJobs);
		db.execSQL(createJobAlerts);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		
	}
	
	public void deleteMyAlert(String keyword,String location)
	{
		SQLiteDatabase db =getDBObject(1);

		db.execSQL("DELETE FROM "+JOB_ALERTS+" WHERE "+KEYWORD+"='"+keyword+"' AND "+LOCATION+"='"+location+"'");
	}
	
	public void deleteSavedJob(int jobId)
	{
		SQLiteDatabase db =getDBObject(1);
//		db.delete(SAVE_JOBS, JOB_ID, jobId);
		db.execSQL("DELETE FROM "+SAVE_JOBS+" WHERE "+ID+"="+jobId);
	}
	
	public void insertToRecentJobs(String arg0,String arg1){
		SQLiteDatabase database= getDBObject(1);
		ContentValues values = new ContentValues();
		values.put(KEYWORD, arg0);
		values.put(LOCATION, arg1);
		database.insert(RECENT_JOBS, null, values);
	}
	
	public void insertToJobAlerts(int alertId,String arg0,String arg1){
		SQLiteDatabase database= getDBObject(1);
		ContentValues values = new ContentValues();
		values.put(ALERTID, alertId);
		values.put(KEYWORD, arg0);
		values.put(LOCATION, arg1);
		database.insert(JOB_ALERTS, null, values);
	}
	
	public void insertToSaveJob(Job_ jobs)  //String arg1,String arg2,String arg3,String arg4,String arg5,String arg6,String arg7,String arg9,String arg10)
	{
	    SQLiteDatabase database=getDBObject(1);
		ContentValues values = new ContentValues();
		values.put(JOB_ID,jobs.getJobId());
		values.put(JOB_TITLE, jobs.getTitle());
		values.put(JOB_SOURCE, jobs.getSource());
		values.put(JOB_URL, jobs.getJoburl());
		values.put(SOURCE_NAME, jobs.getSourcename());
		values.put(STATE, jobs.getState());
		values.put(EMPLOYER, jobs.getEmployer());
		values.put(POSTING_DATE, jobs.getPostingdate());
		values.put(CITY, jobs.getCity());
		values.put(DESCRIPTION, jobs.getDescription());
		values.put(DISPLAYSOURCE, jobs.getDisplaysource());
		database.insert(SAVE_JOBS, null, values);
	}

	public SQLiteDatabase getDBObject(int isWrtitable) {
		return (isWrtitable == 1) ? this.getWritableDatabase() : this.getReadableDatabase();
	}
	
	public List<JobLocationData> getRecentSearchData()
	{
		List<JobLocationData> list=new ArrayList<JobLocationData>();
		SQLiteDatabase db =getDBObject(0);
		
		String selectQuery = "SELECT  * FROM "+OakjobDb.RECENT_JOBS +" ORDER BY "+ID+" DESC";
		Cursor cursor = db.rawQuery(selectQuery, null);
		 if(cursor!=null){
		    // looping through all rows and adding to list
			 while (cursor.moveToNext())
			 { 
				 list.add(new JobLocationData(0,cursor.getString(1),cursor.getString(2)));
			 }
		 }    
		return list;
	}
	
	public List<JobLocationData> getJobAlerts()
	{
		
		List<JobLocationData> list=new ArrayList<JobLocationData>();
		try{
		SQLiteDatabase db =getDBObject(0);
		
		String selectQuery = "SELECT  * FROM "+OakjobDb.JOB_ALERTS +" ORDER BY "+ID+" DESC";
		Cursor cursor = db.rawQuery(selectQuery, null);
		 if(cursor!=null){
		    // looping through all rows and adding to list
			 while (cursor.moveToNext())
			 { 
				 list.add(new JobLocationData(cursor.getInt(1),cursor.getString(2),cursor.getString(3)));
			 }
		 }  
		}catch(Exception e){System.out.println("getAlerts error=:"+e);}
		return list;
	}
	
	public List<Job_> getSaveJobs()
	{
		List<Job_> list=new ArrayList<Job_>();
		SQLiteDatabase db =getDBObject(0);
		
		String selectQuery = "SELECT  * FROM "+OakjobDb.SAVE_JOBS; //+" ORDER BY "+ID+" DESC";
		Cursor cursor = db.rawQuery(selectQuery, null);
		 if(cursor!=null){
		    // looping through all rows and adding to list
			 while (cursor.moveToNext())
			 { 
				 Job_ jobObj=new Job_();
				 jobObj.setJobId(cursor.getInt(0));
				 jobObj.setId(String.valueOf(cursor.getInt(1)));
				 jobObj.setTitle(cursor.getString(2));
				 jobObj.setSource(cursor.getString(3));
				 jobObj.setJoburl(cursor.getString(4));
				 jobObj.setSourcename(cursor.getString(5));
				 jobObj.setState(cursor.getString(6));
				 jobObj.setEmployer(cursor.getString(7));
				 jobObj.setPostingdate(cursor.getString(8));
				 jobObj.setCity(cursor.getString(9));
				 jobObj.setDescription(cursor.getString(10));
				 jobObj.setDisplaysource(cursor.getString(11));
				 list.add(jobObj);
				
			 }
		 }    
		return list;
	}
	
	public List<String> getSavedJobId()
	{
		List<String> list=new ArrayList<String>();
		SQLiteDatabase db =getDBObject(0);
	try{
		String selectQuery = "SELECT  * FROM "+OakjobDb.SAVE_JOBS;
		Cursor cursor = db.rawQuery(selectQuery, null);
		 if(cursor!=null){
		    // looping through all rows and adding to list
			 while (cursor.moveToNext())
			 { 
				 list.add(String.valueOf(cursor.getString(2))+"####"+(cursor.getString(7)));
			 }
		 }
	}
	catch(Exception e)
	{
		e.printStackTrace();
		
	}
		return(list);
	}
	
	public boolean isJobAlertSaved(String keyword,String location)
	{
		SQLiteDatabase db =getDBObject(0);
		
		String selectQuery = "SELECT  * FROM "+OakjobDb.JOB_ALERTS+" WHERE "+KEYWORD+"='"+keyword+"' AND "+LOCATION+"='"+location+"' ";
		Cursor cursor = db.rawQuery(selectQuery, null);
		 if(cursor!=null){
			 if(cursor.moveToNext())
			 {
		  return(true);}
		 }
		return(false);
	}
	
	public void deleteAllRecentJobs()
	{
		SQLiteDatabase db =getDBObject(1);
		db.delete(RECENT_JOBS, null, null);
	}
	
	public static OakjobDb getDBInstance(Context con)
	{
		if(INSTANCE==null)
		{
		return	INSTANCE=new OakjobDb(con);
		}else {
			return(INSTANCE);	
		}
		
	}
	
	public void truncateSaveJobdb()
	{
		SQLiteDatabase db =getDBObject(1);
		db.delete(SAVE_JOBS, null, null);
	}
	
	public void truncateMyAlertsdb()
	{
		SQLiteDatabase db =getDBObject(1);
		db.delete(JOB_ALERTS, null, null);
	}
	
	
}
