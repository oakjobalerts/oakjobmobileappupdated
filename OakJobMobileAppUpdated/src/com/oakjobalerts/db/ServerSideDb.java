package com.oakjobalerts.db;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.http.Query;
import android.content.Context;
import android.location.SettingInjectorService;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.widget.Toast;

import com.oakjobalerts.AwsCloudSearch;
import com.oakjobalerts.MainActivity;
import com.oakjobalerts.SearchListActivity;
import com.oakjobalerts.fragments.LoginFragment;
import com.oakjobalerts.fragments.MyAlertsFragment;
import com.oakjobalerts.fragments.SettingsFragment;
import com.oakjobalerts.fragments.adpater.RecentSearchAdapter;
import com.oakjobalerts.modeldata.DeleteResponse;
import com.oakjobalerts.modeldata.JobAlerts;
import com.oakjobalerts.modeldata.JobAlertsResponse;
import com.oakjobalerts.modeldata.JobLocationData;
import com.oakjobalerts.modeldata.Job_;
import com.oakjobalerts.modeldata.SaveAlertResponse;
import com.oakjobalerts.modeldata.SaveJobResponse;
import com.oakjobalerts.modeldata.SaveResponse;
import com.oakjobalerts.modeldata.SignInResponse;
import com.oakjobalerts.preferences.Preferences;
import com.oakjobalerts.utilities.Utility;

public class ServerSideDb {
	
	AwsCloudSearch inter=null;
	public static ServerSideDb instance=null;
	 List<Job_> saveJobList=new ArrayList<Job_>();
	
	private ServerSideDb(){
		RestAdapter adapter=Utility.getAdapter();
		inter=adapter.create(AwsCloudSearch.class);
	}
	
	public static ServerSideDb getSeverDbInstance()
	{
		if(instance==null)
		{
			instance=new ServerSideDb();
		}
		return instance;
	}
	
	//*************************************************
	//**************Save Jobs At Server******************
	//*************************************************
	
	
	public void saveFavJob(final Job_ job,final Context con)
	{
		job.setEmail(Preferences.getPreferenceInstance().getEmail(con));
		inter.javaAwsCloudSearchAPISaveJob(job.getId(),job.getTitle(),job.getSource(),job.getLocation(),job.getJoburl(),job.getState(),job.getZipcode(),job.getEmployer(),job.getSourcename(),job.getPostingdate() ,job.getCity(),job.getDescription(),job.getEmail(), new Callback<SaveResponse>() {

			@Override
			public void failure(RetrofitError arg0) {
				Toast.makeText(con,"Please Check your Internet Connection", Toast.LENGTH_SHORT).show();
			}

			@Override
			public void success(SaveResponse arg0, Response arg1) {
				if(arg0.isSuccess())
				{
					Toast.makeText(con,arg0.getMessage(), Toast.LENGTH_SHORT).show();
					 OakjobDb.getDBInstance(con).insertToSaveJob(job);
				
				}
				else
				{
					Toast.makeText(con,arg0.getMessage(), Toast.LENGTH_SHORT).show();
				}
				
			}
			
		});
	}
	
	public void saveJobsAtServer(final Job_ job, final int position,final SearchListActivity con)
	{
	
		inter.javaAwsCloudSearchAPISaveJob(job.getId(),job.getTitle(),job.getSource(),job.getLocation(),job.getJoburl(),job.getState(),job.getZipcode(),job.getEmployer(),job.getSourcename(),job.getPostingdate() ,job.getCity(),job.getDescription(),job.getEmail(), new Callback<SaveResponse>() {

			@Override
			public void failure(RetrofitError arg0) {
				Toast.makeText(con,"There is some problem, please try after some time.", Toast.LENGTH_SHORT).show();
			}

			@Override
			public void success(SaveResponse arg0, Response arg1) {
				if(arg0.isSuccess())
				{
					//Toast.makeText(con,arg0.getMessage(), Toast.LENGTH_SHORT).show();
					con.saveJobsAtServer(position,job);
				
				}
				else
				{
					Toast.makeText(con,arg0.getMessage(), Toast.LENGTH_SHORT).show();
				}
				
			}
			
		});
	
			
	}
	
	public void deleteJobFromServer(final Context con,Job_ job)
	{
		String email=Preferences.getPreferenceInstance().getEmail(con);
		inter.javaAwsCloudSearchAPIDeleteJob(job.getEmployer(),job.getTitle(),email, new Callback<DeleteResponse>() {

			@Override
			public void failure(RetrofitError arg0) {
				Toast.makeText(con,"There is some problem, please try after some time.", Toast.LENGTH_SHORT).show();
			}

			@Override
			public void success(DeleteResponse arg0, Response arg1) {
				if(arg0.isStatus())
				{
					Toast.makeText(con,arg0.getMessage(), Toast.LENGTH_SHORT).show();
								
				}
				else
				{
					Toast.makeText(con,arg0.getMessage(), Toast.LENGTH_SHORT).show();
				}
				
			}
			
		});
	
	}

	
	public void getSaveJobsFromServer(final Context con,String email)
	{
		RestAdapter adapter=Utility.getAdapter();
		AwsCloudSearch inter=adapter.create(AwsCloudSearch.class);
		inter.javaAwsCloudSearchAPIgetsavedjobs(email, new Callback<SaveJobResponse>() {
			
			@Override
			public void success(SaveJobResponse arg0, Response arg1) {
				saveJobList=arg0.getResponse();
				reloadSaveJobDB(con,saveJobList);
			}
			
			@Override
			public void failure(RetrofitError arg0) {
				
				
			}
		});
	}
	
	
	//*************************************************
	//************** Alerts At Server******************
	//*************************************************
	
	
	public void saveMyAlert(final String keyword,final String location,final SearchListActivity con)
	{
		SearchListActivity.saveAlertStatus=false;
		con.changeAlertIcon();
		JobAlerts alert=getAlertObject(0,keyword,location,con);
		Log.e("Email address=",alert.getEmailAdrress());
		List<String> dataList=Preferences.getPreferenceInstance().getDevicedata(con);
		alert.setDeviceId(dataList.get(0));
		alert.setDeviceToken(dataList.get(1));
		alert.setPlatform("Android");
		inter.javaAwsCloudSearchAPISaveAlert(alert,new Callback<SaveAlertResponse>(){

			@Override
			public void failure(RetrofitError arg0) {
				Toast.makeText(con,arg0.getMessage(), Toast.LENGTH_SHORT).show();
			}

			@Override
			public void success(SaveAlertResponse arg0, Response arg1) {
				Toast.makeText(con,arg0.getMessage(), Toast.LENGTH_SHORT).show();
				OakjobDb.getDBInstance(con).insertToJobAlerts(arg0.getAlertId(),keyword, location);
				SearchListActivity.saveAlertStatus=true;
			}
			
		});
	}
	
	
	public void getAlertFromServer(final Context con)
	{
		String email=Preferences.getPreferenceInstance().getEmail(con);
		if(email.equals(""))
		{
		String deviceToken=Preferences.getPreferenceInstance().getDevicedata(con).get(0);
		
		inter.javaAwsCloudSearchAPIgetAlert("",deviceToken, new Callback<JobAlertsResponse>() {

			@Override
			public void failure(RetrofitError arg0) {
				Log.d("Failure=",arg0.getMessage());
				//Toast.makeText(con,"There is some problem, please try after some time.", Toast.LENGTH_SHORT).show();
			}

			@Override
			public void success(JobAlertsResponse arg0, Response arg1) {
				if(arg0.isStatus())
				{
					//Toast.makeText(con,arg0.getMessage(), Toast.LENGTH_SHORT).show();
					reloadAlertsDb(con, arg0.getAlert());
								
				}
				else
				{
					//Toast.makeText(con,arg0.getMessage(), Toast.LENGTH_SHORT).show();
				}
				
			}

		
			
		});
		}
		else{
			
		inter.javaAwsCloudSearchAPIgetAlert(email,"", new Callback<JobAlertsResponse>() {

			@Override
			public void failure(RetrofitError arg0) {
				//Toast.makeText(con,"There is some problem, please try after some time.", Toast.LENGTH_SHORT).show();
			}

			@Override
			public void success(JobAlertsResponse arg0, Response arg1) {
				if(arg0.isStatus())
				{
					//Toast.makeText(con,arg0.getMessage(), Toast.LENGTH_SHORT).show();
					reloadAlertsDb(con, arg0.getAlert());
								
				}
				else
				{
					//Toast.makeText(con,arg0.getMessage(), Toast.LENGTH_SHORT).show();
				}
				
			}

		
			
		});
		}
	}
	
	
	
	public void deleteAlertFromServer(final int position,final List<JobLocationData> recentDataList,final RecentSearchAdapter adapter,final String keyword,final String location,final Context con)
	{
//		if(!MainActivity.isLogin(con))
//		{
//			OakjobDb.getDBInstance(con).deleteMyAlert(keyword,location);
//			Toast.makeText(con, "Alert Deleted", Toast.LENGTH_LONG).show();
//			if(recentDataList.size()>0)
//			{
//				adapter.notifyDataSetChanged();
//			}
//			else{
//				MyAlertsFragment.getInstance().emptyList();
//			}
//		}
		if(Utility.isNetworkAvailable(con))
		{
		//JobAlerts alert=getAlertObject(recentDataList.get(position).getAlertId(),keyword, location, con);
			int alert_id=recentDataList.get(position).getAlertId();
		inter.javaAwsCloudSearchAPIdeleteAlert(alert_id, new Callback<DeleteResponse>() {

			@Override
			public void failure(RetrofitError arg0) {
				
			}

			@Override
			public void success(DeleteResponse arg0, Response arg1) {
				if(arg0.isStatus())
				{
					Toast.makeText(con, arg0.getMessage(), Toast.LENGTH_LONG).show();
					OakjobDb.getDBInstance(con).deleteMyAlert(keyword,location);
					recentDataList.remove(position);
					if(recentDataList.size()>0)
					{
						adapter.notifyDataSetChanged();
					}
					else{
						MyAlertsFragment.getInstance().emptyList();
					}
				}
			}
		});
		}
		else {
			Toast.makeText(con,"Please Check your internet Connection.", Toast.LENGTH_SHORT).show();
		}
	}
	
	
	    //*************************************************
		//************** Syc Local DB ******************
		//*************************************************
	
	
	public void reloadSaveJobDB(Context con, List<Job_> saveJobList)
	{
		OakjobDb.getDBInstance(con).truncateSaveJobdb();
		Iterator itr=saveJobList.iterator();
		while(itr.hasNext())
		{
			OakjobDb.getDBInstance(con).insertToSaveJob((Job_)itr.next());
		}
	}
	
	public void reloadAlertsDb(Context con,List<JobAlerts> alertsList)
	{
		OakjobDb.getDBInstance(con).truncateMyAlertsdb();
		Iterator itr=alertsList.iterator();
		while(itr.hasNext())
		{
			JobAlerts alert=(JobAlerts)itr.next();
			OakjobDb.getDBInstance(con).insertToJobAlerts((int)alert.getId(),alert.getKeyword(),alert.getLocation());
		}
	}
	
	public boolean deleteSaveJobDB(int position,Context con, Job_ job)
	{
		boolean flag=false;
		if(!MainActivity.isLogin(con))
		{
			OakjobDb.getDBInstance(con).deleteSavedJob(job.getJobId());
			flag=true;
			Toast.makeText(con,"Job Deleted Successfully", Toast.LENGTH_SHORT).show();
		}
		else if(Utility.isNetworkAvailable(con))
		{
			deleteJobFromServer(con,job);
			flag=true;
			OakjobDb.getDBInstance(con).deleteSavedJob(job.getJobId());
		}
		else 
		{
			Toast.makeText(con,"Please Check your internet Connection", Toast.LENGTH_SHORT).show();
		}
		return flag;
	}
	
	public void cleanAtLogout(Context con)
	{
		OakjobDb.getDBInstance(con).truncateMyAlertsdb();
		OakjobDb.getDBInstance(con).truncateSaveJobdb();
	}
	
	
	// ************ Get Object of Alert to save at Server*****************
	

	
	public JobAlerts getAlertObject(int alertId,String keyword,String location,Context con)
	{
		SimpleDateFormat d=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String date=d.format(new Date());
		
		
		String zip=Preferences.getPreferenceInstance().getUserData(con).getZipcode();
		String email=Preferences.getPreferenceInstance().getEmail(con);
		
		JobAlerts alert=new JobAlerts();
		alert.setId(alertId);
		alert.setKeyword(keyword);
		alert.setLocation(location);
		alert.setDateTime(date);
		alert.setZip(zip);
		alert.setProvider("web");
		alert.setFrequency("1");
		alert.setEmailAdrress(email);
		
		return alert;
	}
	
	  //*************************************************
	  //************** Get user Detail for Settings page ******************
	  //*************************************************
		
	
	public void getUserDetails(final Context con)
	{
		if(Utility.isNetworkAvailable(con)){
		inter.javaAwsgetUserDetail(Preferences.getPreferenceInstance().getEmail(con),Preferences.getPreferenceInstance().getUserData(con).getPass(), new Callback<SignInResponse>() {

			@Override
			public void failure(RetrofitError arg0) {
			
			}

			@Override
			public void success(SignInResponse arg0, Response arg1) {
				if(arg0!=null && arg0.getSuccess()==true)
				{
					Preferences.getPreferenceInstance().loginSession(con, arg0.getResponse());
					SettingsFragment.getInstance().setUserdata();
				}
			}
		} );}
	}
	
	
	  //*************************************************
			//************** Logout from Server ******************
			//*************************************************
		
	
	public void logout(final FragmentActivity frag)
	{
		inter.javaAwslogout(Preferences.getPreferenceInstance().getEmail(frag),Preferences.getPreferenceInstance().getDevicedata(frag).get(1), new Callback<DeleteResponse>() {

			@Override
			public void failure(RetrofitError arg0) {
			
			}

			@Override
			public void success(DeleteResponse arg0, Response arg1) {
				if(arg0!=null && arg0.isStatus()==true)
				{
					Preferences.getPreferenceInstance().logoutSession(frag);
					ServerSideDb.getSeverDbInstance().cleanAtLogout(frag);
					Utility.replaceFragment(frag, LoginFragment.getInstance(), Utility.TAG_LOGIN, false);
				}
			}
		} );
	}
	
	
}
