package com.oakjobalerts.oakinterface;

import com.oakjobalerts.SearchListActivity;
import com.oakjobalerts.utilities.Utility;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.MotionEvent;

public interface OakFragmentInterface{
	
	public void oakFragmentListner(Fragment fragment, String tag,boolean addToStack);
	
	public void initiateNewActivity(Class className,String tag);
	
	public void backBtnSimulate(Fragment fragment, String tag);
	
	public void showBackBtn();
	
	public void showMenuBtn();
	
	public void showEditBtn();
	
	public void showDeleteBtn();
	
	public void showNoBtn();
	
	public void showLogoutBtn();
	
	public void showRefreshBtn();
	
	public void hideKeyboard();
	
	public void initiateSearchResultActivity(String what,String where,Class className,String tag);
//	public boolean disableSlidingPane(boolean onOff);

//	boolean onInterceptTouchEvent(MotionEvent ev);
	
}
